﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using DichVuMotCua.Content;
using DichVuMotCua.Models;
using Microsoft.Owin.Security.VanLang;
namespace DichVuMotCua.Controllers
{
    public class VanLangLoginController : BaseLoginController
    {
        private DBmodels db = new DBmodels();
        // GET: VanLangLogin
        public ActionResult Index()
        {
            var result = BaseLogin(null, true);
            return result;
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "username,password")] taikhoan taikhoan, string url)
        {
            if("null".Equals(taikhoan.username))
            {
                Session.Add(CommonConstants.USER_SESSION, null);
            }
            else
            {
                
                // nếu đăng nhập thành công thì lưu thông tin user vào database
                var isTaiKhoan = db.taikhoans.Where(i => i.email == taikhoan.username).ToList();
                if (isTaiKhoan == null || isTaiKhoan.Count == 0 )
                {
                    taikhoan taikhoan1 = new taikhoan();
                    taikhoan1.email = taikhoan.username;
                    taikhoan1.role = 2;
                    db.taikhoans.Add(taikhoan1);
                    db.SaveChanges();
                }
                var TaiKhoan = db.taikhoans.Where(i => i.email == taikhoan.username).First();
                Session.Add(CommonConstants.USER_SESSION, TaiKhoan);


            }
            if(!url.IsEmpty())
                return Redirect(url);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult sendMail()
        {
            SmtpClient smtpClient = new SmtpClient("smtp-mail.outlook.com", 587); //587
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential("t15ct03@vanlanguni.vn", "VLUt15ct03");

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("t15ct03@vanlanguni.vn", "CTMIS-no-reply");
            mail.To.Add(new MailAddress("katothang@gmail.com"));
            mail.Subject = "test mail";
            mail.Body = "thang that tha";
            smtpClient.Send(mail);
            return View();
        }
    }
}