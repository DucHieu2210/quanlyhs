﻿using DichVuMotCua.Content;
using DichVuMotCua.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DichVuMotCua.Controllers
{
    public class BaseLoginController : Controller
    {

        // GET: BaseLogin
        public ActionResult BaseLogin(object data, Boolean isLoginPage = false)
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if(taikhoan == null)
            {
                if(isLoginPage)
                {
                    return View();
                }
                Response.Redirect("/VanLangLogin/Index");
                return null;
            }
            else
            {
                if (data == null) return View();
                return View(data);
            }
            
            
        }
    }
}