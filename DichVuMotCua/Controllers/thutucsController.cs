﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DichVuMotCua.Areas.Admin.Controllers;
using DichVuMotCua.Content;
using DichVuMotCua.Models;
using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json;
namespace DichVuMotCua.Controllers
{
    public class thutucsController : BaseLoginController
    {
        private DBmodels db = new DBmodels();
        public thutucsController()
        {
            var khoas = db.khoas.ToList();
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var item in khoas)
            {
                listItems.Add(new SelectListItem
                {
                    Text = item.name.ToString(),
                    Value = item.name.ToString(),
                    Selected = true
                });
            }
            ViewBag.khoa = listItems;

        }

        
        

        // GET: thutucs
        public ActionResult Index()
        {
            var result = BaseLogin(db.thutucs.ToList());
            return result;
        }

        // GET: thutucs/Details/5
        public ActionResult Details(int? id, int idForm)
        {
            string data = "";
            if (idForm == Constants.XINTHOIHOC)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var hocphan = db.hocphans.Where(i => i.thutuc_id == thutuc.id).ToList();
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "MAUdangkytamdung.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAHOVATEN", ("Họ Và Tên").Equals(item.noidung) ? item.value : "DATAHOVATEN");
                    data = data.Replace("DATAMSV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMSV");
                    data = data.Replace("DATANGAYSINH", ("Ngày Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATALOP", ("Lớp").Equals(item.noidung) ? item.value : "DATALOP");
                    data = data.Replace("DATAKHOA", ("Khoa").Equals(item.noidung) ? item.value : "DATAKHOA");
                    data = data.Replace("DATAHOCKI", ("Học Kì Đã Học").Equals(item.noidung) ? item.value : "DATAHOCKI");
                    data = data.Replace("DATADVHTTIEUDE", ("ĐVHT").Equals(item.noidung) ? item.value : "DATADVHTTIEUDE");

                    data = data.Replace("DATANGAYNGHI", ("Ngày Nghỉ").Equals(item.noidung) ? item.value : "DATANGAYNGHI");
                    data = data.Replace("DATANGAYDIHOC", ("Ngày Đi Học").Equals(item.noidung) ? item.value : "DATANGAYDIHOC");
                    data = data.Replace("DATALYDO", ("Lý Do").Equals(item.noidung) ? item.value : "DATALYDO");
                }

                int stt = 0;
                data = data.Replace("DATAHP10", "");
                data = data.Replace("DATATHP10", "");
                data = data.Replace("DATADVHT10", "");
                data = data.Replace("DATANGAYHOCLAI10", "");
                foreach (hocphan item in hocphan)
                {
                    stt++;
                    data = data.Replace("DATAHP" + stt, item.mahocphan);
                    data = data.Replace("DATATHP" + stt, item.tenhocphan);
                    data = data.Replace("DATADVHT" + stt, item.soDVHT);
                    data = data.Replace("DATANGAYHOCLAI" + stt, item.thoigianhoclai.Year < 2000 ? "":item.thoigianhoclai.ToString("yyyy-MM-dd"));
                }

                for (int i = 0; i <= 10; i++)
                {
                    data = data.Replace("DATAHP" + i, "");
                    data = data.Replace("DATATHP" + i, "");
                    data = data.Replace("DATADVHT" + i, "");
                    data = data.Replace("DATANGAYHOCLAI" + i, "");
                }
            }
            else if (idForm == Constants.BAOLUUKETQUAHOCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "baoluuketquahoctap.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAKHOA", ("Khoa").Equals(item.noidung) ? item.value : "DATAKHOA");
                    data = data.Replace("DATAHOTEN", ("Họ Tên").Equals(item.noidung) ? item.value : "DATAHOTEN");
                    data = data.Replace("DATAMSV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMSV");
                    data = data.Replace("DATANGAYSINH", ("Ngày Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATANOISINH", ("Nơi Sinh").Equals(item.noidung) ? item.value : "DATANOISINH");
                    data = data.Replace("DATALOP", ("Lớp").Equals(item.noidung) ? item.value : "DATALOP");
                    data = data.Replace("DATATHUONGTRU", ("Thường Trú").Equals(item.noidung) ? item.value : "DATATHUONGTRU");

                    data = data.Replace("DATASDT", ("SĐT").Equals(item.noidung) ? item.value : "DATASDT");
                    data = data.Replace("DATAHK1", ("TBC Học Kỳ 1").Equals(item.noidung) ? item.value : "DATAHK1");
                    data = data.Replace("DATAHK2", ("TBC Học Kỳ 2").Equals(item.noidung) ? item.value : "DATAHK2");
                    data = data.Replace("DATAHK3", ("TBC Học Kỳ 3").Equals(item.noidung) ? item.value : "DATAHK3");
                    data = data.Replace("DATAHK4", ("TBC Học Kỳ 4").Equals(item.noidung) ? item.value : "DATAHK4");
                    data = data.Replace("DATAHK5", ("TBC Học Kỳ 5").Equals(item.noidung) ? item.value : "DATAHK5");
                    data = data.Replace("DATAHK6", ("TBC Học Kỳ 6").Equals(item.noidung) ? item.value : "DATAHK6");
                    data = data.Replace("DATABAOLUUTU", ("Bảo Lưu Từ").Equals(item.noidung) ? item.value : "DATABAOLUUTU");
                    data = data.Replace("DATABAOLUUDEN", ("Ngày Học Lại").Equals(item.noidung) ? item.value : "DATABAOLUUDEN");
                    data = data.Replace("DATALYDO", ("Lý Do").Equals(item.noidung) ? item.value : "DATALYDO");

                }
            }

            else if (idForm == Constants.BAOLUUKETQUATUYENSINH)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "baoluuketquatuyensinh.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAHOTEN", ("Họ Tên").Equals(item.noidung) ? item.value : "DATAHOTEN");
                    data = data.Replace("DATAMASV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMASV");
                    data = data.Replace("DATANGAYSINH", ("Năm Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATANOISINH", ("Nơi Sinh").Equals(item.noidung) ? item.value : "DATANOISINH");
                    data = data.Replace("DATASBD", ("Số Báo Danh").Equals(item.noidung) ? item.value : "DATASBD");
                    data = data.Replace("DATANAMTUYENSINH", ("Năm Thi Tuyển").Equals(item.noidung) ? item.value : "DATANAMTUYENSINH");
                    data = data.Replace("DATANGANH", ("Ngành Trúng Tuyển").Equals(item.noidung) ? item.value : "DATANGANH");
                    data = data.Replace("DATAMANGANH", ("Mã Ngành").Equals(item.noidung) ? item.value : "DATAMANGANH");

                    data = data.Replace("DATATONGDIEM", ("Tổng Điểm").Equals(item.noidung) ? item.value : "DATATONGDIEM");
                    data = data.Replace("DATABAOLUUDENNAM", ("Bảo Lưu Đến").Equals(item.noidung) ? item.value : "DATABAOLUUDENNAM");
                    data = data.Replace("DATALYDO", ("Lý Do").Equals(item.noidung) ? item.value : "DATALYDO");

                }
            }

            // chuyển điểm 

            else if (idForm == Constants.CHUYENDIEM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "chuyen_diem.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAHOTEN", ("Họ Tên").Equals(item.noidung) ? item.value : "DATAHOTEN");
                    data = data.Replace("DATAMSV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMSV");
                    data = data.Replace("DATANGAYSINH", ("Năm Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATALOP", ("Lớp").Equals(item.noidung) ? item.value : "DATALOP");
                    data = data.Replace("DATAKHOA", ("Khoa").Equals(item.noidung) ? item.value : "DATAKHOA");
                    data = data.Replace("DATAHK", ("Học Kỳ").Equals(item.noidung) ? item.value : "DATAHK");
                    data = data.Replace("DATAKHÓA", ("Khóa").Equals(item.noidung) ? item.value : "DATAKHÓA");
                    data = data.Replace("DATATRUONG", ("Trường").Equals(item.noidung) ? item.value : "DATATRUONG");

                    data = data.Replace("DATAKDAHOC", ("Khóa Đã Học").Equals(item.noidung) ? item.value : "DATAKDAHOC");

                    data = data.Replace("DATAHP1", ("HP1").Equals(item.noidung) ? item.value : "DATAHP1");
                    data = data.Replace("DATAĐVHT1", ("ĐVHT1").Equals(item.noidung) ? item.value : "DATAĐVHT1");
                    data = data.Replace("DATADATAD1", ("D1").Equals(item.noidung) ? item.value : "DATADATAD1");
                    data = data.Replace("DATAGC1", ("GC1").Equals(item.noidung) ? item.value : "DATAGC1");

                    data = data.Replace("DATAHP2", ("HP2").Equals(item.noidung) ? item.value : "DATAHP2");
                    data = data.Replace("DATAĐVHT2", ("ĐVHT2").Equals(item.noidung) ? item.value : "DATAĐVHT2");
                    data = data.Replace("DATADATAD2", ("D2").Equals(item.noidung) ? item.value : "DATADATAD2");
                    data = data.Replace("DATAGC2", ("GC2").Equals(item.noidung) ? item.value : "DATAGC2");

                    data = data.Replace("DATAHP3", ("HP3").Equals(item.noidung) ? item.value : "DATAHP3");
                    data = data.Replace("DATAĐVHT3", ("ĐVHT3").Equals(item.noidung) ? item.value : "DATAĐVHT3");
                    data = data.Replace("DATADATAD3", ("D3").Equals(item.noidung) ? item.value : "DATADATAD3");
                    data = data.Replace("DATAGC3", ("GC3").Equals(item.noidung) ? item.value : "DATAGC3");

                    data = data.Replace("DATAHP4", ("HP4").Equals(item.noidung) ? item.value : "DATAHP4");
                    data = data.Replace("DATAĐVHT4", ("ĐVHT4").Equals(item.noidung) ? item.value : "DATAĐVHT4");
                    data = data.Replace("DATADATAD4", ("D4").Equals(item.noidung) ? item.value : "DATADATAD4");
                    data = data.Replace("DATAGC4", ("GC4").Equals(item.noidung) ? item.value : "DATAGC4");

                    data = data.Replace("DATAHP5", ("HP5").Equals(item.noidung) ? item.value : "DATAHP5");
                    data = data.Replace("DATAĐVHT5", ("ĐVHT5").Equals(item.noidung) ? item.value : "DATAĐVHT5");
                    data = data.Replace("DATADATAD5", ("D5").Equals(item.noidung) ? item.value : "DATADATAD5");
                    data = data.Replace("DATAGC5", ("GC5").Equals(item.noidung) ? item.value : "DATAGC5");

                    data = data.Replace("DATAHP6", ("HP6").Equals(item.noidung) ? item.value : "DATAHP6");
                    data = data.Replace("DATAĐVHT6", ("ĐVHT6").Equals(item.noidung) ? item.value : "DATAĐVHT6");
                    data = data.Replace("DATADATAD6", ("D6").Equals(item.noidung) ? item.value : "DATADATAD6");
                    data = data.Replace("DATAGC6", ("GC6").Equals(item.noidung) ? item.value : "DATAGC6");

                    data = data.Replace("DATACOTRONGHOCKY", ("Có Trong HP đang học").Equals(item.noidung) ? item.value : "DATACOTRONGHOCKY");
                    data = data.Replace("DATATBCTL", ("TBTL").Equals(item.noidung) ? item.value : "DATATBCTL");
                    data = data.Replace("DATATCDDI", ("Trường Có Điểm Chuyển ĐI").Equals(item.noidung) ? item.value : "DATATCDDI");
                    data = data.Replace("DATATCDDEN", ("Trường Có Điểm Chuyển Đến").Equals(item.noidung) ? item.value : "DATATCDDEN");

                }
            }
            else if (idForm == Constants.HOCCHAM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "MAUdangkyhoccham.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAHOTEN", ("Họ Tên").Equals(item.noidung) ? item.value : "DATAHOTEN");
                    data = data.Replace("DATAMSV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMSV");
                    data = data.Replace("DATANGAYSINH", ("Năm Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATALOP", ("Lớp").Equals(item.noidung) ? item.value : "DATALOP");
                    data = data.Replace("DATAKHOA", ("Khoa").Equals(item.noidung) ? item.value : "DATAKHOA");
                    data = data.Replace("DATAHK", ("Học Kỳ").Equals(item.noidung) ? item.value : "DATAHK");
                    data = data.Replace("DATAKHÓA", ("Khóa").Equals(item.noidung) ? item.value : "DATAKHÓA");
                    data = data.Replace("DATATRUONG", ("Trường").Equals(item.noidung) ? item.value : "DATATRUONG");

                    data = data.Replace("DATAKDAHOC", ("Khóa Đã Học").Equals(item.noidung) ? item.value : "DATAKDAHOC");

                    data = data.Replace("DATAHP1", ("HP1").Equals(item.noidung) ? item.value : "DATAHP1");
                    data = data.Replace("DATAĐVHT1", ("ĐVHT1").Equals(item.noidung) ? item.value : "DATAĐVHT1");
                    data = data.Replace("DATADATAD1", ("D1").Equals(item.noidung) ? item.value : "DATADATAD1");
                    data = data.Replace("DATAGC1", ("GC1").Equals(item.noidung) ? item.value : "DATAGC1");

                    data = data.Replace("DATAHP2", ("HP2").Equals(item.noidung) ? item.value : "DATAHP2");
                    data = data.Replace("DATAĐVHT2", ("ĐVHT2").Equals(item.noidung) ? item.value : "DATAĐVHT2");
                    data = data.Replace("DATADATAD2", ("D2").Equals(item.noidung) ? item.value : "DATADATAD2");
                    data = data.Replace("DATAGC2", ("GC2").Equals(item.noidung) ? item.value : "DATAGC2");

                    data = data.Replace("DATAHP3", ("HP3").Equals(item.noidung) ? item.value : "DATAHP3");
                    data = data.Replace("DATAĐVHT3", ("ĐVHT3").Equals(item.noidung) ? item.value : "DATAĐVHT3");
                    data = data.Replace("DATADATAD3", ("D3").Equals(item.noidung) ? item.value : "DATADATAD3");
                    data = data.Replace("DATAGC3", ("GC3").Equals(item.noidung) ? item.value : "DATAGC3");

                    data = data.Replace("DATAHP4", ("HP4").Equals(item.noidung) ? item.value : "DATAHP4");
                    data = data.Replace("DATAĐVHT4", ("ĐVHT4").Equals(item.noidung) ? item.value : "DATAĐVHT4");
                    data = data.Replace("DATADATAD4", ("D4").Equals(item.noidung) ? item.value : "DATADATAD4");
                    data = data.Replace("DATAGC4", ("GC4").Equals(item.noidung) ? item.value : "DATAGC4");

                    data = data.Replace("DATAHP5", ("HP5").Equals(item.noidung) ? item.value : "DATAHP5");
                    data = data.Replace("DATAĐVHT5", ("ĐVHT5").Equals(item.noidung) ? item.value : "DATAĐVHT5");
                    data = data.Replace("DATADATAD5", ("D5").Equals(item.noidung) ? item.value : "DATADATAD5");
                    data = data.Replace("DATAGC5", ("GC5").Equals(item.noidung) ? item.value : "DATAGC5");

                    data = data.Replace("DATAHP6", ("HP6").Equals(item.noidung) ? item.value : "DATAHP6");
                    data = data.Replace("DATAĐVHT6", ("ĐVHT6").Equals(item.noidung) ? item.value : "DATAĐVHT6");
                    data = data.Replace("DATADATAD6", ("D6").Equals(item.noidung) ? item.value : "DATADATAD6");
                    data = data.Replace("DATAGC6", ("GC6").Equals(item.noidung) ? item.value : "DATAGC6");

                    data = data.Replace("DATACOTRONGHOCKY", ("Có Trong HP đang học").Equals(item.noidung) ? item.value : "DATACOTRONGHOCKY");
                    data = data.Replace("DATATBCTL", ("TBTL").Equals(item.noidung) ? item.value : "DATATBCTL");
                    data = data.Replace("DATATCDDI", ("Trường Có Điểm Chuyển ĐI").Equals(item.noidung) ? item.value : "DATATCDDI");
                    data = data.Replace("DATATCDDEN", ("Trường Có Điểm Chuyển Đến").Equals(item.noidung) ? item.value : "DATATCDDEN");

                }
            }

            // học vượt 
            else if (idForm == Constants.HOCVUOT)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "MAUdangkyhocVUOT.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("DATAHOTEN", ("Họ Tên").Equals(item.noidung) ? item.value : "DATAHOTEN");
                    data = data.Replace("DATAMSV", ("Mã SV").Equals(item.noidung) ? item.value : "DATAMSV");
                    data = data.Replace("DATANGAYSINH", ("Ngày Sinh").Equals(item.noidung) ? item.value : "DATANGAYSINH");
                    data = data.Replace("DATALOP", ("Lớp").Equals(item.noidung) ? item.value : "DATALOP");
                    data = data.Replace("DATAKHOA", ("Khoa").Equals(item.noidung) ? item.value : "DATAKHOA");
                    data = data.Replace("DATANGANH", ("Ngành").Equals(item.noidung) ? item.value : "DATANGANH");
                    data = data.Replace("DATAHOANTAT", ("Hoàn Tất").Equals(item.noidung) ? item.value : "DATAHOANTAT");
                    data = data.Replace("DATAHOCKY1", ("HK1").Equals(item.noidung) ? item.value : "DATAHOCKY1");
                    data = data.Replace("DATANAMHOC1", ("Năm1").Equals(item.noidung) ? item.value : "DATANAMHOC1");
                    data = data.Replace("DATADAT1", ("Đạt1").Equals(item.noidung) ? item.value : "DATADAT1");

                    data = data.Replace("DATAHOCKY2", ("HK2").Equals(item.noidung) ? item.value : "DATAHOCKY2");
                    data = data.Replace("DATANAMHOC2", ("Năm2").Equals(item.noidung) ? item.value : "DATANAMHOC2");
                    data = data.Replace("DATADAT2", ("Đạt2").Equals(item.noidung) ? item.value : "DATADAT2");

                    data = data.Replace("DATAHOCKY3", ("HK3").Equals(item.noidung) ? item.value : "DATAHOCKY3");
                    data = data.Replace("DATANAMHOC3", ("Năm3").Equals(item.noidung) ? item.value : "DATANAMHOC3");
                    data = data.Replace("DATADAT3", ("Đạt3").Equals(item.noidung) ? item.value : "DATADAT3");
                    data = data.Replace("DATADAT3", ("Đạt3").Equals(item.noidung) ? item.value : "DATADAT3");

                    data = data.Replace("DATAHOCKY4", ("HK4").Equals(item.noidung) ? item.value : "DATAHOCKY4");
                    data = data.Replace("DATANAMHOC4", ("Năm4").Equals(item.noidung) ? item.value : "DATANAMHOC4");
                    data = data.Replace("DATADAT4", ("Đạt4").Equals(item.noidung) ? item.value : "DATADAT4");

                    data = data.Replace("DATAHOCKY5", ("HK5").Equals(item.noidung) ? item.value : "DATAHOCKY5");
                    data = data.Replace("DATANAMHOC5", ("Năm5").Equals(item.noidung) ? item.value : "DATANAMHOC5");
                    data = data.Replace("DATADAT5", ("Đạt5").Equals(item.noidung) ? item.value : "DATADAT5");

                    data = data.Replace("DATAHOCKY6", ("HK6").Equals(item.noidung) ? item.value : "DATAHOCKY6");
                    data = data.Replace("DATANAMHOC6", ("Năm6").Equals(item.noidung) ? item.value : "DATANAMHOC6");
                    data = data.Replace("DATADAT6", ("Đạt6").Equals(item.noidung) ? item.value : "DATADAT6");

                    data = data.Replace("MM1", ("MM1").Equals(item.noidung) ? item.value : "MM1");
                    data = data.Replace("HP1", ("HP1").Equals(item.noidung) ? item.value : "HP1");
                    data = data.Replace("DVHT1", ("DVHT1").Equals(item.noidung) ? item.value : "DVHT1");
                    data = data.Replace("GC1", ("GC1").Equals(item.noidung) ? item.value : "GC1");

                    data = data.Replace("MM2", ("MM2").Equals(item.noidung) ? item.value : "MM2");
                    data = data.Replace("HP2", ("HP2").Equals(item.noidung) ? item.value : "HP2");
                    data = data.Replace("DVHT2", ("DVHT2").Equals(item.noidung) ? item.value : "DVHT2");
                    data = data.Replace("GC2", ("GC2").Equals(item.noidung) ? item.value : "GC2");

                    data = data.Replace("MM3", ("MM3").Equals(item.noidung) ? item.value : "MM3");
                    data = data.Replace("HP3", ("HP3").Equals(item.noidung) ? item.value : "HP3");
                    data = data.Replace("DVHT3", ("DVHT3").Equals(item.noidung) ? item.value : "DVHT3");
                    data = data.Replace("GC3", ("GC3").Equals(item.noidung) ? item.value : "GC3");

                    data = data.Replace("MM4", ("MM4").Equals(item.noidung) ? item.value : "MM4");
                    data = data.Replace("HP4", ("HP4").Equals(item.noidung) ? item.value : "HP4");
                    data = data.Replace("DVHT4", ("DVHT4").Equals(item.noidung) ? item.value : "DVHT4");
                    data = data.Replace("GC4", ("GC4").Equals(item.noidung) ? item.value : "GC4");





                }
            }

            else if (idForm == Constants.CAPBANSAOBANG)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "CAPBANSAOBANG.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                }
            }

            else if (idForm == Constants.DANGKYCHUONGTRINHHOCNHANH)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DANGKYCHUONGTRINHHOCNHANH.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                }
            }

            else if (idForm == Constants.DANGKYHOCLAI)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DANGKYHOCLAI.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                    data = data.Replace("[DATA27]", ("[DATA27]").Equals(item.noidung) ? item.value : "[DATA27]");
                    data = data.Replace("[DATA28]", ("[DATA28]").Equals(item.noidung) ? item.value : "[DATA28]");
                    data = data.Replace("[DATA29]", ("[DATA29]").Equals(item.noidung) ? item.value : "[DATA29]");
                    data = data.Replace("[DATA30]", ("[DATA30]").Equals(item.noidung) ? item.value : "[DATA30]");
                    data = data.Replace("[DATA31]", ("[DATA31]").Equals(item.noidung) ? item.value : "[DATA31]");
                    data = data.Replace("[DATA32]", ("[DATA32]").Equals(item.noidung) ? item.value : "[DATA32]");
                    data = data.Replace("[DATA33]", ("[DATA33]").Equals(item.noidung) ? item.value : "[DATA33]");
                    data = data.Replace("[DATA34]", ("[DATA34]").Equals(item.noidung) ? item.value : "[DATA34]");
                    data = data.Replace("[DATA35]", ("[DATA35]").Equals(item.noidung) ? item.value : "[DATA35]");
                    data = data.Replace("[DATA36]", ("[DATA36]").Equals(item.noidung) ? item.value : "[DATA36]");
                    data = data.Replace("[DATA37]", ("[DATA37]").Equals(item.noidung) ? item.value : "[DATA37]");
                    data = data.Replace("[DATA38]", ("[DATA38]").Equals(item.noidung) ? item.value : "[DATA38]");
                    data = data.Replace("[DATA39]", ("[DATA39]").Equals(item.noidung) ? item.value : "[DATA39]");
                    data = data.Replace("[DATA40]", ("[DATA40]").Equals(item.noidung) ? item.value : "[DATA40]");
                    data = data.Replace("[DATA41]", ("[DATA41]").Equals(item.noidung) ? item.value : "[DATA41]");
                }
            }

            else if (idForm == Constants.DANGKYHOCPHANTHAYTHE)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DANGKYHOCPHANTHAYTHE.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                }
            }

            else if (idForm == Constants.DANGKYHOCTHEOTIENDOCHAM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DANGKYHOCTHEOTIENDOCHAM.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                }
            }

            else if (idForm == Constants.DENGHICAPBANGDIEM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DENGHICAPBANGDIEM.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                }
            }

            else if (idForm == Constants.DENGHICHUNGTHUCVANBANG)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DENGHICHUNGTHUCVANBANG.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                }
            }

            else if (idForm == Constants.DIEUCHINHKEHOACHHOCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DIEUCHINHKEHOACHHOCTAP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                    data = data.Replace("[DATA27]", ("[DATA27]").Equals(item.noidung) ? item.value : "[DATA27]");
                    data = data.Replace("[DATA28]", ("[DATA28]").Equals(item.noidung) ? item.value : "[DATA28]");
                    data = data.Replace("[DATA29]", ("[DATA29]").Equals(item.noidung) ? item.value : "[DATA29]");
                    data = data.Replace("[DATA30]", ("[DATA30]").Equals(item.noidung) ? item.value : "[DATA30]");
                    data = data.Replace("[DATA31]", ("[DATA31]").Equals(item.noidung) ? item.value : "[DATA31]");
                    data = data.Replace("[DATA32]", ("[DATA32]").Equals(item.noidung) ? item.value : "[DATA32]");
                    data = data.Replace("[DATA33]", ("[DATA33]").Equals(item.noidung) ? item.value : "[DATA33]");
                    data = data.Replace("[DATA34]", ("[DATA34]").Equals(item.noidung) ? item.value : "[DATA34]");
                    data = data.Replace("[DATA35]", ("[DATA35]").Equals(item.noidung) ? item.value : "[DATA35]");
                }
            }

            else if (idForm == Constants.DOICHIEUCHUONGTRINHDAOTAO)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DOICHIEUCHUONGTRINHDAOTAO.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                    data = data.Replace("[DATA27]", ("[DATA27]").Equals(item.noidung) ? item.value : "[DATA27]");
                    data = data.Replace("[DATA28]", ("[DATA28]").Equals(item.noidung) ? item.value : "[DATA28]");
                    data = data.Replace("[DATA29]", ("[DATA29]").Equals(item.noidung) ? item.value : "[DATA29]");
                    data = data.Replace("[DATA30]", ("[DATA30]").Equals(item.noidung) ? item.value : "[DATA30]");
                    data = data.Replace("[DATA31]", ("[DATA31]").Equals(item.noidung) ? item.value : "[DATA31]");
                    data = data.Replace("[DATA32]", ("[DATA32]").Equals(item.noidung) ? item.value : "[DATA32]");
                    data = data.Replace("[DATA33]", ("[DATA33]").Equals(item.noidung) ? item.value : "[DATA33]");
                    data = data.Replace("[DATA34]", ("[DATA34]").Equals(item.noidung) ? item.value : "[DATA34]");
                    data = data.Replace("[DATA35]", ("[DATA35]").Equals(item.noidung) ? item.value : "[DATA35]");
                    data = data.Replace("[DATA36]", ("[DATA36]").Equals(item.noidung) ? item.value : "[DATA36]");
                    data = data.Replace("[DATA37]", ("[DATA37]").Equals(item.noidung) ? item.value : "[DATA37]");
                    data = data.Replace("[DATA38]", ("[DATA38]").Equals(item.noidung) ? item.value : "[DATA38]");
                    data = data.Replace("[DATA39]", ("[DATA39]").Equals(item.noidung) ? item.value : "[DATA39]");
                    data = data.Replace("[DATA40]", ("[DATA40]").Equals(item.noidung) ? item.value : "[DATA40]");
                }
            }

            else if (idForm == Constants.DONDANGKYCAITHIENDIEM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONDANGKYCAITHIENDIEM.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                }
            }

            else if (idForm == Constants.DONPHUCKHAO)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONPHUCKHAO.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                }
            }

            else if (idForm == Constants.DONRUTBOTHOCPHAN)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONRUTBOTHOCPHAN.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                }
            }

            else if (idForm == Constants.DONXETTOTNGHIEPSOM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXETTOTNGHIEPSOM.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                }
            }

            else if (idForm == Constants.DONXINCHUYENLOP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXINCHUYENLOP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                }
            }

            else if (idForm == Constants.DONXINCHUYENNGANH)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXINCHUYENNGANH.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                }
            }

            else if (idForm == Constants.DONXINNHAPHOC)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXINNHAPHOC.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                }
            }

            else if (idForm == Constants.DONXINTAMNGUNGHOC)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXINTAMNGUNGHOC.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                    data = data.Replace("[DATA27]", ("[DATA27]").Equals(item.noidung) ? item.value : "[DATA27]");
                    data = data.Replace("[DATA28]", ("[DATA28]").Equals(item.noidung) ? item.value : "[DATA28]");
                    data = data.Replace("[DATA29]", ("[DATA29]").Equals(item.noidung) ? item.value : "[DATA29]");
                    data = data.Replace("[DATA30]", ("[DATA30]").Equals(item.noidung) ? item.value : "[DATA30]");
                    data = data.Replace("[DATA31]", ("[DATA31]").Equals(item.noidung) ? item.value : "[DATA31]");
                    data = data.Replace("[DATA32]", ("[DATA32]").Equals(item.noidung) ? item.value : "[DATA32]");
                    data = data.Replace("[DATA33]", ("[DATA33]").Equals(item.noidung) ? item.value : "[DATA33]");
                    data = data.Replace("[DATA34]", ("[DATA34]").Equals(item.noidung) ? item.value : "[DATA34]");
                    data = data.Replace("[DATA35]", ("[DATA35]").Equals(item.noidung) ? item.value : "[DATA35]");
                    data = data.Replace("[DATA36]", ("[DATA36]").Equals(item.noidung) ? item.value : "[DATA36]");
                    data = data.Replace("[DATA37]", ("[DATA37]").Equals(item.noidung) ? item.value : "[DATA37]");
                    data = data.Replace("[DATA38]", ("[DATA38]").Equals(item.noidung) ? item.value : "[DATA38]");
                    data = data.Replace("[DATA39]", ("[DATA39]").Equals(item.noidung) ? item.value : "[DATA39]");
                    data = data.Replace("[DATA40]", ("[DATA40]").Equals(item.noidung) ? item.value : "[DATA40]");
                    data = data.Replace("[DATA41]", ("[DATA41]").Equals(item.noidung) ? item.value : "[DATA41]");
                    data = data.Replace("[DATA42]", ("[DATA42]").Equals(item.noidung) ? item.value : "[DATA42]");
                    data = data.Replace("[DATA43]", ("[DATA43]").Equals(item.noidung) ? item.value : "[DATA43]");
                    data = data.Replace("[DATA44]", ("[DATA44]").Equals(item.noidung) ? item.value : "[DATA44]");
                }
            }

            else if (idForm == Constants.DONXINTHOIHOCRUTHOSO)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "DONXINTHOIHOCRUTHOSO.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                }
            }

            else if (idForm == Constants.GIAYGIOITHIEUTHUCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "GIAYGIOITHIEUTHUCTAP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                }
            }

            else if (idForm == Constants.GIAYXACNHANDECUONG)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "GIAYXACNHANDECUONG.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                }
            }

            else if (idForm == Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                }
            }

            else if (idForm == Constants.HOCHAICHUONGTRINH)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "HOCHAICHUONGTRINH.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                }
            }

            else if (idForm == Constants.KEHOACHHOCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "KEHOACHHOCTAP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                    data = data.Replace("[DATA25]", ("[DATA25]").Equals(item.noidung) ? item.value : "[DATA25]");
                    data = data.Replace("[DATA26]", ("[DATA26]").Equals(item.noidung) ? item.value : "[DATA26]");
                    data = data.Replace("[DATA27]", ("[DATA27]").Equals(item.noidung) ? item.value : "[DATA27]");
                    data = data.Replace("[DATA28]", ("[DATA28]").Equals(item.noidung) ? item.value : "[DATA28]");
                    data = data.Replace("[DATA29]", ("[DATA29]").Equals(item.noidung) ? item.value : "[DATA29]");
                    data = data.Replace("[DATA30]", ("[DATA30]").Equals(item.noidung) ? item.value : "[DATA30]");
                    data = data.Replace("[DATA31]", ("[DATA31]").Equals(item.noidung) ? item.value : "[DATA31]");
                    data = data.Replace("[DATA32]", ("[DATA32]").Equals(item.noidung) ? item.value : "[DATA32]");
                    data = data.Replace("[DATA33]", ("[DATA33]").Equals(item.noidung) ? item.value : "[DATA33]");
                    data = data.Replace("[DATA34]", ("[DATA34]").Equals(item.noidung) ? item.value : "[DATA34]");
                    data = data.Replace("[DATA35]", ("[DATA35]").Equals(item.noidung) ? item.value : "[DATA35]");
                    data = data.Replace("[DATA36]", ("[DATA36]").Equals(item.noidung) ? item.value : "[DATA36]");
                    data = data.Replace("[DATA37]", ("[DATA37]").Equals(item.noidung) ? item.value : "[DATA37]");
                    data = data.Replace("[DATA38]", ("[DATA38]").Equals(item.noidung) ? item.value : "[DATA38]");
                    data = data.Replace("[DATA39]", ("[DATA39]").Equals(item.noidung) ? item.value : "[DATA39]");
                    data = data.Replace("[DATA40]", ("[DATA40]").Equals(item.noidung) ? item.value : "[DATA40]");
                    data = data.Replace("[DATA41]", ("[DATA41]").Equals(item.noidung) ? item.value : "[DATA41]");
                    data = data.Replace("[DATA42]", ("[DATA42]").Equals(item.noidung) ? item.value : "[DATA42]");
                    data = data.Replace("[DATA43]", ("[DATA43]").Equals(item.noidung) ? item.value : "[DATA43]");
                    data = data.Replace("[DATA44]", ("[DATA44]").Equals(item.noidung) ? item.value : "[DATA44]");
                    data = data.Replace("[DATA45]", ("[DATA45]").Equals(item.noidung) ? item.value : "[DATA45]");
                    data = data.Replace("[DATA46]", ("[DATA46]").Equals(item.noidung) ? item.value : "[DATA46]");
                    data = data.Replace("[DATA47]", ("[DATA47]").Equals(item.noidung) ? item.value : "[DATA47]");
                    data = data.Replace("[DATA48]", ("[DATA48]").Equals(item.noidung) ? item.value : "[DATA48]");
                    data = data.Replace("[DATA49]", ("[DATA49]").Equals(item.noidung) ? item.value : "[DATA49]");
                    data = data.Replace("[DATA50]", ("[DATA50]").Equals(item.noidung) ? item.value : "[DATA50]");
                    data = data.Replace("[DATA51]", ("[DATA51]").Equals(item.noidung) ? item.value : "[DATA51]");
                    data = data.Replace("[DATA52]", ("[DATA52]").Equals(item.noidung) ? item.value : "[DATA52]");
                    data = data.Replace("[DATA53]", ("[DATA53]").Equals(item.noidung) ? item.value : "[DATA53]");
                }
            }

            else if (idForm == Constants.MOTACHUONGTRINHDAOTAO)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "MOTACHUONGTRINHDAOTAO.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                }
            }

            else if (idForm == Constants.THICAITHIENDIEM)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "THICAITHIENDIEM.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                    data = data.Replace("[DATA19]", ("[DATA19]").Equals(item.noidung) ? item.value : "[DATA19]");
                    data = data.Replace("[DATA20]", ("[DATA20]").Equals(item.noidung) ? item.value : "[DATA20]");
                    data = data.Replace("[DATA21]", ("[DATA21]").Equals(item.noidung) ? item.value : "[DATA21]");
                    data = data.Replace("[DATA22]", ("[DATA22]").Equals(item.noidung) ? item.value : "[DATA22]");
                    data = data.Replace("[DATA23]", ("[DATA23]").Equals(item.noidung) ? item.value : "[DATA23]");
                    data = data.Replace("[DATA24]", ("[DATA24]").Equals(item.noidung) ? item.value : "[DATA24]");
                }
            }

            else if (idForm == Constants.TOTNGHIEPTAMTHOI)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "TOTNGHIEPTAMTHOI.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                    data = data.Replace("[DATA10]", ("[DATA10]").Equals(item.noidung) ? item.value : "[DATA10]");
                    data = data.Replace("[DATA11]", ("[DATA11]").Equals(item.noidung) ? item.value : "[DATA11]");
                    data = data.Replace("[DATA12]", ("[DATA12]").Equals(item.noidung) ? item.value : "[DATA12]");
                    data = data.Replace("[DATA13]", ("[DATA13]").Equals(item.noidung) ? item.value : "[DATA13]");
                    data = data.Replace("[DATA14]", ("[DATA14]").Equals(item.noidung) ? item.value : "[DATA14]");
                    data = data.Replace("[DATA15]", ("[DATA15]").Equals(item.noidung) ? item.value : "[DATA15]");
                    data = data.Replace("[DATA16]", ("[DATA16]").Equals(item.noidung) ? item.value : "[DATA16]");
                    data = data.Replace("[DATA17]", ("[DATA17]").Equals(item.noidung) ? item.value : "[DATA17]");
                    data = data.Replace("[DATA18]", ("[DATA18]").Equals(item.noidung) ? item.value : "[DATA18]");
                }
            }

            else if (idForm == Constants.XACNHANKETQUAHOCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "XACNHANKETQUAHOCTAP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                }
            }

            else if (idForm == Constants.XACNHANTHOIGIANTHUCTAP)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                thutuc thutuc = db.thutucs.Find(id);
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();



                using (var reader = new StreamReader(Server.MapPath("/static/html/") + "XACNHANTHOIGIANTHUCTAP.html"))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        data += line;
                        line = reader.ReadLine();
                    }
                }
                // thong tin sinh vien
                foreach (chitietthutuc item in chitietthutuc)
                {
                    data = data.Replace("[DATA1]", ("[DATA1]").Equals(item.noidung) ? item.value : "[DATA1]");
                    data = data.Replace("[DATA2]", ("[DATA2]").Equals(item.noidung) ? item.value : "[DATA2]");
                    data = data.Replace("[DATA3]", ("[DATA3]").Equals(item.noidung) ? item.value : "[DATA3]");
                    data = data.Replace("[DATA4]", ("[DATA4]").Equals(item.noidung) ? item.value : "[DATA4]");
                    data = data.Replace("[DATA5]", ("[DATA5]").Equals(item.noidung) ? item.value : "[DATA5]");
                    data = data.Replace("[DATA6]", ("[DATA6]").Equals(item.noidung) ? item.value : "[DATA6]");
                    data = data.Replace("[DATA7]", ("[DATA7]").Equals(item.noidung) ? item.value : "[DATA7]");
                    data = data.Replace("[DATA8]", ("[DATA8]").Equals(item.noidung) ? item.value : "[DATA8]");
                    data = data.Replace("[DATA9]", ("[DATA9]").Equals(item.noidung) ? item.value : "[DATA9]");
                }
            }



            return Content(data);
        }

        // GET: thutucs/Create
        public ActionResult Create()
        {
            var thongtin = db.loaithutucs.ToList();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != 1)
                {
                    thongtin.RemoveAt(i);
                }
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        // POST: thutucs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public ActionResult Create([Bind(Include = "id,name,nguoitao,nguoiduyet")] thutuc thutuc)
        {
            if (ModelState.IsValid)
            {
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                db.thutucs.Add(thutuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thutuc);
        }

        // GET: thutucs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            thutuc thutuc = db.thutucs.Find(id);
            if (thutuc == null)
            {
                return HttpNotFound();
            }
            return View(thutuc);
        }

        // POST: thutucs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public ActionResult Edit([Bind(Include = "id,name,nguoitao,nguoiduyet")] thutuc thutuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thutuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thutuc);
        }

        // GET: thutucs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            thutuc thutuc = db.thutucs.Find(id);
            if (thutuc == null)
            {
                return HttpNotFound();
            }
            return View(thutuc);
        }

        // POST: thutucs/Delete/5
        [HttpPost, ActionName("Delete")]

        public ActionResult DeleteConfirmed(int id, string form)
        {
            thutuc thutuc = db.thutucs.Find(id);
            db.thutucs.Remove(thutuc);
            db.SaveChanges();
            return RedirectToAction(form);
        }

        public ActionResult Duyet(int id, string form)
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            thutuc thutuc = db.thutucs.Find(id);
            thutuc.nguoiduyet = taikhoan.id;
            db.SaveChanges();

            var email = db.taikhoans.Find(thutuc.nguoitao);
            CommonConstants.sendMail(email.email, "Hồ sơ của bạn đã được duyệt");
            return RedirectToAction(form);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CapGiayChungNhan()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPGIAYCHUNGNHAN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }

            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DSCapGiayChungNhan()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CAPGIAYCHUNGNHAN)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CAPGIAYCHUNGNHAN)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSCapGiayChungNhan()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.CAPGIAYCHUNGNHAN).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CAPGIAYCHUNGNHAN).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }
        [HttpPost]
        public ActionResult CapGiayChungNhan([Bind(Include = "id,name")] thutuc thutuc, HttpPostedFileBase file)
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPGIAYCHUNGNHAN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }

            if (ModelState.IsValid)
            {
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.CAPGIAYCHUNGNHAN;
                db.thutucs.Add(thutuc);


                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }

                db.SaveChanges();
                return RedirectToAction("DSCapGiayChungNhan");
            }

            // ViewBag.id_user = new SelectList(db.users, "id", "username", thutuc.id_user);
            return View(thutuc);
        }

        public ActionResult CapNhatThongTin()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPNHATTHONGTIN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }

            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DSCapNhatThongTin()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CAPNHATTHONGTIN)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CAPNHATTHONGTIN)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSCapNhatThongTin()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CAPNHATTHONGTIN)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CAPNHATTHONGTIN)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }
        [HttpPost]
        public ActionResult CapNhatThongTin([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPNHATTHONGTIN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.CAPNHATTHONGTIN;
                db.thutucs.Add(thutuc);


                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }

                db.SaveChanges();
                return RedirectToAction("DSCapNhatThongTin");
            }
            return View("");
        }

        public ActionResult DSCapInTheSinhVien()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CAPINTHESINHVIEN)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CAPINTHESINHVIEN)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult CapInTheSinhVien()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPINTHESINHVIEN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }

            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult CapInTheSinhVien([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPINTHESINHVIEN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.CAPINTHESINHVIEN;
                db.thutucs.Add(thutuc);


                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }

                db.SaveChanges();
                return RedirectToAction("DSCapInTheSinhVien");

            }
            return View(thutuc);
        }

        public ActionResult XinThoiHoc(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            if (hocphan != null && hocphan.Count != 0)
            {
                ViewBag.hocphan = hocphan;
            }



            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.XINTHOIHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult HoSoXinThoiHoc()
        {
            int id = 0;
            try
            {
                id = Int32.Parse(Request.QueryString["id"]);
            }
            catch
            {


            }
            var thutuc = db.thutucs.Find(id);

            if (thutuc != null)
            {
                var hocphan = db.hocphans.Where(i => i.thutuc_id == thutuc.id).ToList();
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.hocphan = hocphan;
                ViewBag.chitietthutuc = chitietthutuc;
            }
            ViewBag.thutuc = thutuc;

            return View();
        }

        public ActionResult DSXinThoiHoc()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSXinThoiHoc");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.XINTHOIHOC)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.XINTHOIHOC)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSXinThoiHoc()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.XINTHOIHOC).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XINTHOIHOC).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XINTHOIHOC).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        [HttpPost]
        public ActionResult XinThoiHoc(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.XINTHOIHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    //thutuc.ngaytao = DateTime.Today;
                    // thutuc.anh = "khongco";
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.XINTHOIHOC;
                    thutuc.name = "Xin Thôi Học";
                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i)];
                        chitietthutuc.value = Request.Form["thutuc" + (i)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }

                    var count = Int32.Parse(Request["count"].Replace("\"", ""));
                    for (int i = 0; i < 10; i++)
                    {
                        var hocphan = new hocphan();
                        hocphan.mahocphan = Request["mahocphan" + i].Replace("\"", "");
                        hocphan.tenhocphan = Request["tenhocphan" + i].Replace("\"", "");
                        hocphan.soDVHT = Request["soDVHT" + i].Replace("\"", "");
                        try
                        {
                            hocphan.thoigianhoclai = DateTime.Parse(Request["thoigianhoclai" + i].Replace("\"", ""));
                        }
                        catch
                        {

                        }

                        hocphan.thutuc_id = thutuc.id;
                        db.hocphans.Add(hocphan);
                    }
                    if (count == 0)
                    {
                        var hocphan = new hocphan();
                        hocphan.mahocphan = Request["mahocphan"].Replace("\"", "");
                        hocphan.tenhocphan = Request["tenhocphan"].Replace("\"", "");
                        hocphan.soDVHT = Request["soDVHT"].Replace("\"", "");
                        hocphan.thoigianhoclai = DateTime.Parse(Request["thoigianhoclai"].Replace("\"", ""));
                        hocphan.thutuc_id = thutuc.id;
                        db.hocphans.Add(hocphan);
                    }
                    db.SaveChanges();
                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + i];
                        chitietthutuc[i].value = Request.Form["thutuc" + i];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }

                    var count = Int32.Parse(Request["count"].Replace("\"", ""));
                    var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
                    for (int i = 0; i < hocphan.Count; i++)
                    {

                        hocphan[i].mahocphan = Request["mahocphan" + i].Replace("\"", "");
                        hocphan[i].tenhocphan = Request["tenhocphan" + i].Replace("\"", "");
                        hocphan[i].soDVHT = Request["soDVHT" + i].Replace("\"", "");
                        try
                        {
                            hocphan[i].thoigianhoclai = DateTime.Parse(Request["thoigianhoclai" + i].Replace("\"", ""));
                        }
                        catch
                        {

                        }
                        db.hocphans.Attach(hocphan[i]);
                        db.Entry(hocphan[i]).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("DSXinThoiHoc");

            }
            return View(thutuc);
        }

        public ActionResult CAPBANSAOBANG(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CAPBANSAOBANG)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DANGKYCHUONGTRINHHOCNHANH(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DANGKYCHUONGTRINHHOCNHANH)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DANGKYHOCLAI(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DANGKYHOCLAI)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DANGKYHOCPHANTHAYTHE(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DANGKYHOCPHANTHAYTHE)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DANGKYHOCTHEOTIENDOCHAM(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DANGKYHOCTHEOTIENDOCHAM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DENGHICAPBANGDIEM(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DENGHICAPBANGDIEM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DENGHICHUNGTHUCVANBANG(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DENGHICHUNGTHUCVANBANG)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DIEUCHINHKEHOACHHOCTAP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DIEUCHINHKEHOACHHOCTAP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DOICHIEUCHUONGTRINHDAOTAO(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DOICHIEUCHUONGTRINHDAOTAO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONDANGKYCAITHIENDIEM(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONDANGKYCAITHIENDIEM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONPHUCKHAO(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONPHUCKHAO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONRUTBOTHOCPHAN(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONRUTBOTHOCPHAN)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONXETTOTNGHIEPSOM(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXETTOTNGHIEPSOM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONXINCHUYENLOP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINCHUYENLOP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONXINCHUYENNGANH(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINCHUYENNGANH)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONXINNHAPHOC(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINNHAPHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONXINTAMNGUNGHOC(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINTAMNGUNGHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DONDONXINTHOIHOCRUTHOSORUTHOSO(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult GIAYGIOITHIEUTHUCTAP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.GIAYGIOITHIEUTHUCTAP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult GIAYXACNHANDECUONG(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
             ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.GIAYXACNHANDECUONG)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult HOCHAICHUONGTRINH(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.HOCHAICHUONGTRINH)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult KEHOACHHOCTAP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.KEHOACHHOCTAP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult MOTACHUONGTRINHDAOTAO(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.MOTACHUONGTRINHDAOTAO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult THICAITHIENDIEM(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.THICAITHIENDIEM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult TOTNGHIEPTAMTHOI(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.TOTNGHIEPTAMTHOI)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult XACNHANKETQUAHOCTAP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.XACNHANKETQUAHOCTAP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult XACNHANTHOIGIANTHUCTAP(int? id)
        {
            thutuc thutuc = db.thutucs.Find(id);
            var hocphan = db.hocphans.Where(i => i.thutuc_id == id).ToList();
            ViewBag.thutuc = thutuc;
            if (thutuc != null)
            {
                var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == thutuc.id).ToList();
                ViewBag.noidung = chitietthutuc;
            }
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.XACNHANTHOIGIANTHUCTAP)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }



        public ActionResult DSCAPBANSAOBANG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSCAPBANSAOBANG");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CAPBANSAOBANG)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CAPBANSAOBANG)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSCAPBANSAOBANG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.CAPBANSAOBANG).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CAPBANSAOBANG).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CAPBANSAOBANG).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDANGKYCHUONGTRINHHOCNHANH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDANGKYCHUONGTRINHHOCNHANH");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DANGKYCHUONGTRINHHOCNHANH)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DANGKYCHUONGTRINHHOCNHANH)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDANGKYCHUONGTRINHHOCNHANH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DANGKYCHUONGTRINHHOCNHANH).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYCHUONGTRINHHOCNHANH).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYCHUONGTRINHHOCNHANH).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDANGKYHOCLAI()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDANGKYHOCLAI");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DANGKYHOCLAI)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DANGKYHOCLAI)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDANGKYHOCLAI()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCLAI).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCLAI).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCLAI).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDANGKYHOCPHANTHAYTHE()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDANGKYHOCPHANTHAYTHE");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DANGKYHOCPHANTHAYTHE)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DANGKYHOCPHANTHAYTHE)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDANGKYHOCPHANTHAYTHE()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCPHANTHAYTHE).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCPHANTHAYTHE).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCPHANTHAYTHE).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDANGKYHOCTHEOTIENDOCHAM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDANGKYHOCTHEOTIENDOCHAM");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DANGKYHOCTHEOTIENDOCHAM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DANGKYHOCTHEOTIENDOCHAM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDANGKYHOCTHEOTIENDOCHAM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCTHEOTIENDOCHAM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCTHEOTIENDOCHAM).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DANGKYHOCTHEOTIENDOCHAM).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDENGHICAPBANGDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDENGHICAPBANGDIEM");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DENGHICAPBANGDIEM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DENGHICAPBANGDIEM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDENGHICAPBANGDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DENGHICAPBANGDIEM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DENGHICAPBANGDIEM).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DENGHICAPBANGDIEM).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDENGHICHUNGTHUCVANBANG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDENGHICHUNGTHUCVANBANG");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DENGHICHUNGTHUCVANBANG)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DENGHICHUNGTHUCVANBANG)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDENGHICHUNGTHUCVANBANG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DENGHICHUNGTHUCVANBANG).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DENGHICHUNGTHUCVANBANG).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DENGHICHUNGTHUCVANBANG).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDIEUCHINHKEHOACHHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDIEUCHINHKEHOACHHOCTAP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DIEUCHINHKEHOACHHOCTAP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DIEUCHINHKEHOACHHOCTAP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDIEUCHINHKEHOACHHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DIEUCHINHKEHOACHHOCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DIEUCHINHKEHOACHHOCTAP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DIEUCHINHKEHOACHHOCTAP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDOICHIEUCHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDOICHIEUCHUONGTRINHDAOTAO");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DOICHIEUCHUONGTRINHDAOTAO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DOICHIEUCHUONGTRINHDAOTAO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDOICHIEUCHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DOICHIEUCHUONGTRINHDAOTAO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DOICHIEUCHUONGTRINHDAOTAO).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DOICHIEUCHUONGTRINHDAOTAO).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONDANGKYCAITHIENDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONDANGKYCAITHIENDIEM");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONDANGKYCAITHIENDIEM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONDANGKYCAITHIENDIEM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONDANGKYCAITHIENDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONDANGKYCAITHIENDIEM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONDANGKYCAITHIENDIEM).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONDANGKYCAITHIENDIEM).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONPHUCKHAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONPHUCKHAO");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONPHUCKHAO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONPHUCKHAO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONPHUCKHAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONPHUCKHAO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONPHUCKHAO).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONPHUCKHAO).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONRUTBOTHOCPHAN()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONRUTBOTHOCPHAN");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONRUTBOTHOCPHAN)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONRUTBOTHOCPHAN)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONRUTBOTHOCPHAN()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONRUTBOTHOCPHAN).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONRUTBOTHOCPHAN).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONRUTBOTHOCPHAN).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONXETTOTNGHIEPSOM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONXETTOTNGHIEPSOM");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXETTOTNGHIEPSOM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXETTOTNGHIEPSOM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONXETTOTNGHIEPSOM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXETTOTNGHIEPSOM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXETTOTNGHIEPSOM).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXETTOTNGHIEPSOM).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONXINCHUYENLOP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONXINCHUYENLOP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINCHUYENLOP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINCHUYENLOP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONXINCHUYENLOP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINCHUYENLOP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINCHUYENLOP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINCHUYENLOP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONXINCHUYENNGANH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONXINCHUYENNGANH");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINCHUYENNGANH)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINCHUYENNGANH)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONXINCHUYENNGANH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINCHUYENNGANH).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINCHUYENNGANH).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINCHUYENNGANH).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONXINNHAPHOC()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONXINNHAPHOC");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINNHAPHOC)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINNHAPHOC)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONXINNHAPHOC()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINNHAPHOC).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINNHAPHOC).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINNHAPHOC).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONXINTAMNGUNGHOC()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONXINTAMNGUNGHOC");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINTAMNGUNGHOC)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINTAMNGUNGHOC)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONXINTAMNGUNGHOC()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINTAMNGUNGHOC).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINTAMNGUNGHOC).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINTAMNGUNGHOC).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSDONDONXINTHOIHOCRUTHOSORUTHOSO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSDONDONXINTHOIHOCRUTHOSORUTHOSO");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDONDONXINTHOIHOCRUTHOSORUTHOSO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINTHOIHOCRUTHOSO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINTHOIHOCRUTHOSO).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINTHOIHOCRUTHOSO).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSGIAYGIOITHIEUTHUCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSGIAYGIOITHIEUTHUCTAP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.GIAYGIOITHIEUTHUCTAP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.GIAYGIOITHIEUTHUCTAP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSGIAYGIOITHIEUTHUCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.GIAYGIOITHIEUTHUCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYGIOITHIEUTHUCTAP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYGIOITHIEUTHUCTAP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSGIAYXACNHANDECUONG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSGIAYXACNHANDECUONG");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.GIAYXACNHANDECUONG)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.GIAYXACNHANDECUONG)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSGIAYXACNHANDECUONG()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.GIAYXACNHANDECUONG).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYXACNHANDECUONG).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYXACNHANDECUONG).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSHOCHAICHUONGTRINH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSHOCHAICHUONGTRINH");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.HOCHAICHUONGTRINH)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.HOCHAICHUONGTRINH)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSHOCHAICHUONGTRINH()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.HOCHAICHUONGTRINH).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.HOCHAICHUONGTRINH).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.HOCHAICHUONGTRINH).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSKEHOACHHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSKEHOACHHOCTAP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.KEHOACHHOCTAP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.KEHOACHHOCTAP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSKEHOACHHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.KEHOACHHOCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.KEHOACHHOCTAP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.KEHOACHHOCTAP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSMOTACHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSMOTACHUONGTRINHDAOTAO");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.MOTACHUONGTRINHDAOTAO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.MOTACHUONGTRINHDAOTAO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSMOTACHUONGTRINHDAOTAO()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.MOTACHUONGTRINHDAOTAO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.MOTACHUONGTRINHDAOTAO).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.MOTACHUONGTRINHDAOTAO).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSTHICAITHIENDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSTHICAITHIENDIEM");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.THICAITHIENDIEM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.THICAITHIENDIEM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSTHICAITHIENDIEM()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.THICAITHIENDIEM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.THICAITHIENDIEM).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.THICAITHIENDIEM).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSTOTNGHIEPTAMTHOI()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSTOTNGHIEPTAMTHOI");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.TOTNGHIEPTAMTHOI)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.TOTNGHIEPTAMTHOI)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSTOTNGHIEPTAMTHOI()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.TOTNGHIEPTAMTHOI).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.TOTNGHIEPTAMTHOI).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.TOTNGHIEPTAMTHOI).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSXACNHANKETQUAHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSXACNHANKETQUAHOCTAP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.XACNHANKETQUAHOCTAP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.XACNHANKETQUAHOCTAP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSXACNHANKETQUAHOCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.XACNHANKETQUAHOCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XACNHANKETQUAHOCTAP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XACNHANKETQUAHOCTAP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult DSXACNHANTHOIGIANTHUCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan.role == 2)
            {
                return RedirectToAction("HSXACNHANTHOIGIANTHUCTAP");
            }
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.XACNHANTHOIGIANTHUCTAP)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.XACNHANTHOIGIANTHUCTAP)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSXACNHANTHOIGIANTHUCTAP()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.XACNHANTHOIGIANTHUCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XACNHANTHOIGIANTHUCTAP).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.XACNHANTHOIGIANTHUCTAP).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }


        public ActionResult TiepTucHoc()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.TIEPTUCHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        public ActionResult DSTiepTucHoc()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.TIEPTUCHOC)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.TIEPTUCHOC)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult ChuyenDiem()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.CHUYENDIEM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult ChuyenDiem([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.CHUYENDIEM).ToList();

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.CHUYENDIEM;

                db.thutucs.Add(thutuc);
                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }
                db.SaveChanges();
                return RedirectToAction("DSChuyenDiem");

            }
            return View(thutuc);
        }
        public ActionResult DSChuyenDiem()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.CHUYENDIEM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.CHUYENDIEM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSChuyenDiem()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.CHUYENDIEM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CHUYENDIEM).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }


        // Học Chậm
        public ActionResult HocCham()
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.HOCCHAM)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult HocCham([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.HOCCHAM).ToList();

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.HOCCHAM;

                db.thutucs.Add(thutuc);
                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }
                db.SaveChanges();
                return RedirectToAction("DSHocCham");

            }
            return View(thutuc);
        }
        public ActionResult DSHocCham()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.HOCCHAM)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.HOCCHAM)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }


        [HttpPost]

        public ActionResult TiepTucHoc([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.TIEPTUCHOC)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.TIEPTUCHOC;
                db.thutucs.Add(thutuc);


                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }

                db.SaveChanges();
                return RedirectToAction("DSTiepTucHoc");

            }
            return View(thutuc);
        }
        // bao luu ket qua hoc tap
        public ActionResult BaoLuuKetQuaHocTap()
        {
            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUAHOCTAP).ToList();
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult BaoLuuKetQuaHocTap([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUAHOCTAP).ToList();

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.BAOLUUKETQUAHOCTAP;

                db.thutucs.Add(thutuc);
                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }
                db.SaveChanges();
                return RedirectToAction("DSBaoLuuKetQuaHocTap");

            }
            return View(thutuc);
        }

        public ActionResult DSBaoLuuKetQuaHocTap()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUAHOCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.BAOLUUKETQUAHOCTAP).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSBaoLuuKetQuaHocTap()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUAHOCTAP).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.BAOLUUKETQUAHOCTAP).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        // bao luu ket qua tuyen sinh
        public ActionResult BaoLuuKetQuaTuyenSinh()
        {
            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUATUYENSINH).ToList();
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult BaoLuuKetQuaTuyenSinh([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUATUYENSINH).ToList();

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.BAOLUUKETQUATUYENSINH;

                db.thutucs.Add(thutuc);
                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }
                db.SaveChanges();
                return RedirectToAction("DSBaoLuuKetQuaTuyenSinh");

            }
            return View(thutuc);
        }

        public ActionResult DSBaoLuuKetQuaTuyenSinh()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUATUYENSINH).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.BAOLUUKETQUATUYENSINH).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSBaoLuuKetQuaTuyenSinh()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.BAOLUUKETQUATUYENSINH).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.BAOLUUKETQUATUYENSINH).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        // mau dang ky hoc vươt
        // bao luu ket qua tuyen sinh
        public ActionResult DangKyHocVuot()
        {
            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.HOCVUOT).ToList();
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult DangKyHocVuot([Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.HOCVUOT).ToList();

            if (ModelState.IsValid)
            {

                //thutuc.ngaytao = DateTime.Today;
                // thutuc.anh = "khongco";
                taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                thutuc.nguoitao = taikhoan.id;
                thutuc.loaithutuc = Constants.HOCVUOT;

                db.thutucs.Add(thutuc);
                db.SaveChanges();
                thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                var chitietthutuc = new chitietthutuc();
                for (int i = 0; i < thongtin.Count; i++)
                {
                    chitietthutuc = new chitietthutuc();
                    chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                    chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                    chitietthutuc.thutuc = thutuc.id;
                    db.chitietthutucs.Add(chitietthutuc);
                }
                db.SaveChanges();
                return RedirectToAction("DSDangKyHocVuot");

            }
            return View(thutuc);
        }

        public ActionResult DSDangKyHocVuot()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.HOCVUOT).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.HOCVUOT).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSDangKyHocVuot()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.HOCVUOT).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.HOCVUOT).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }

        public ActionResult HSHocCham()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.HOCCHAM).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.HOCCHAM).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }


        public ActionResult HSThuTucDaTao()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var thutuc = db.thutucs.Where(i => i.nguoitao == taikhoan.id).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }


        [HttpPost]
        public ActionResult YeuCauSua(int? id, string noidung, string form)
        {
            var thutuc = db.thutucs.Find(id);

            if (thutuc != null)
            {
                thutuc.ghichu = noidung;
                db.SaveChanges();


                // send mail
                var email = db.taikhoans.Find(thutuc.nguoitao);
                CommonConstants.sendMail(email.email, noidung);
            }
            return RedirectToAction(form);
        }


       

        [HttpPost]
        public ActionResult CAPBANSAOBANG(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.CAPBANSAOBANG).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.CAPBANSAOBANG;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + i];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSCAPBANSAOBANG");
            }
            db.SaveChanges();  return View(thutuc);
        }




        [HttpPost]
        public ActionResult DANGKYCHUONGTRINHHOCNHANH(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DANGKYCHUONGTRINHHOCNHANH).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DANGKYCHUONGTRINHHOCNHANH;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDANGKYCHUONGTRINHHOCNHANH");
            }
            db.SaveChanges();  return View(thutuc);
        }


        [HttpPost]
        public ActionResult DANGKYHOCLAI(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCLAI).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DANGKYHOCLAI;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDANGKYHOCLAI");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DANGKYHOCPHANTHAYTHE(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCPHANTHAYTHE).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DANGKYHOCPHANTHAYTHE;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDANGKYHOCPHANTHAYTHE");
            }
            db.SaveChanges();  return View(thutuc);
        }




        [HttpPost]
        public ActionResult DANGKYHOCTHEOTIENDOCHAM(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DANGKYHOCTHEOTIENDOCHAM).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DANGKYHOCTHEOTIENDOCHAM;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDANGKYHOCTHEOTIENDOCHAM");
            }
            db.SaveChanges();  return View(thutuc);
        }


        [HttpPost]
        public ActionResult DENGHICAPBANGDIEM(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DENGHICAPBANGDIEM).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DENGHICAPBANGDIEM;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDENGHICAPBANGDIEM");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DENGHICHUNGTHUCVANBANG(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DENGHICHUNGTHUCVANBANG).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DENGHICHUNGTHUCVANBANG;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDENGHICHUNGTHUCVANBANG");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DIEUCHINHKEHOACHHOCTAP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DIEUCHINHKEHOACHHOCTAP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DIEUCHINHKEHOACHHOCTAP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDIEUCHINHKEHOACHHOCTAP");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DOICHIEUCHUONGTRINHDAOTAO(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DOICHIEUCHUONGTRINHDAOTAO).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DOICHIEUCHUONGTRINHDAOTAO;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDOICHIEUCHUONGTRINHDAOTAO");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONDANGKYCAITHIENDIEM(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONDANGKYCAITHIENDIEM).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONDANGKYCAITHIENDIEM;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONDANGKYCAITHIENDIEM");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONPHUCKHAO(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONPHUCKHAO).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONPHUCKHAO;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONPHUCKHAO");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONRUTBOTHOCPHAN(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONRUTBOTHOCPHAN).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONRUTBOTHOCPHAN;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONRUTBOTHOCPHAN");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONXETTOTNGHIEPSOM(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXETTOTNGHIEPSOM).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXETTOTNGHIEPSOM;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONXETTOTNGHIEPSOM");
            }
            db.SaveChanges();  return View(thutuc);
        }




        [HttpPost]
        public ActionResult DONXINCHUYENLOP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXINCHUYENLOP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXINCHUYENLOP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONXINCHUYENLOP");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONXINCHUYENNGANH(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXINCHUYENNGANH).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXINCHUYENNGANH;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                   


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i + 1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i + 1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }
                db.SaveChanges();

                return RedirectToAction("DSDONXINCHUYENNGANH");
            }

            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult DONXINNHAPHOC(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXINNHAPHOC).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXINNHAPHOC;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONXINNHAPHOC");
            }
            db.SaveChanges();  return View(thutuc);
        }




        [HttpPost]
        public ActionResult DONXINTAMNGUNGHOC(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXINTAMNGUNGHOC).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXINTAMNGUNGHOC;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONXINTAMNGUNGHOC");
            }
            db.SaveChanges();  return View(thutuc);
        }


        public ActionResult DONXINTHOIHOCRUTHOSO(int? id)
        {
            var thongtin = db.loaithutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < thongtin.Count; i++)
            {
                if (thongtin[i].loai != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listID.Add(thongtin[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                thongtin.Remove(item);
            }
            ViewBag.chitietthutuc = thongtin;
            return View();
        }

        [HttpPost]
        public ActionResult DONXINTHOIHOCRUTHOSO(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.DONXINTHOIHOCRUTHOSO).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.DONXINTHOIHOCRUTHOSO;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSDONXINTHOIHOCRUTHOSO");
            }
            db.SaveChanges();  return View(thutuc);
        }

        public ActionResult DSDONXINTHOIHOCRUTHOSO()
        {
            var loaithutuc = db.loaithutucs.ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            List<loaithutuc> listID = new List<loaithutuc>();
            for (int i = 0; i < loaithutuc.Count; i++)
            {
                if (loaithutuc[i].loai != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listID.Add(loaithutuc[i]);

                }
            }
            foreach (loaithutuc item in listID)
            {
                loaithutuc.Remove(item);
            }

            var thutuc = db.thutucs.ToList();
            List<thutuc> listIDThutuc = new List<thutuc>();
            for (int i = 0; i < thutuc.Count; i++)
            {
                if (thutuc[i].loaithutuc != Constants.DONXINTHOIHOCRUTHOSO)
                {
                    listIDThutuc.Add(thutuc[i]);

                }
            }
            foreach (thutuc item in listIDThutuc)
            {
                thutuc.Remove(item);
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }
        public ActionResult HSDONXINTHOIHOCRUTHOSO()
        {
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.DONXINTHOIHOCRUTHOSO).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();

            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.DONXINTHOIHOCRUTHOSO).Where(i => i.nguoiduyet != null).ToList();
            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }







        [HttpPost]
        public ActionResult GIAYGIOITHIEUTHUCTAP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.GIAYGIOITHIEUTHUCTAP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.GIAYGIOITHIEUTHUCTAP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSGIAYGIOITHIEUTHUCTAP");
            }
            db.SaveChanges();  return View(thutuc);
        }




        [HttpPost]
        public ActionResult GIAYXACNHANDECUONG(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.GIAYXACNHANDECUONG).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.GIAYXACNHANDECUONG;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSGIAYXACNHANDECUONG");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO");
            }
            db.SaveChanges();  return View(thutuc);
        }


        [HttpPost]
        public ActionResult HOCHAICHUONGTRINH(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.HOCHAICHUONGTRINH).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.HOCHAICHUONGTRINH;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSHOCHAICHUONGTRINH");
            }
            db.SaveChanges();  return View(thutuc);
        }

        [HttpPost]
        public ActionResult KEHOACHHOCTAP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.KEHOACHHOCTAP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.KEHOACHHOCTAP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSKEHOACHHOCTAP");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult MOTACHUONGTRINHDAOTAO(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.MOTACHUONGTRINHDAOTAO).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.MOTACHUONGTRINHDAOTAO;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSMOTACHUONGTRINHDAOTAO");
            }
            db.SaveChanges();  return View(thutuc);
        }



        [HttpPost]
        public ActionResult THICAITHIENDIEM(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.THICAITHIENDIEM).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.THICAITHIENDIEM;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSTHICAITHIENDIEM");
            }
            return View(thutuc);
        }



        [HttpPost]
        public ActionResult TOTNGHIEPTAMTHOI(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.TOTNGHIEPTAMTHOI).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.TOTNGHIEPTAMTHOI;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + i];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSTOTNGHIEPTAMTHOI");
            }
            return View(thutuc);
        }



        [HttpPost]
        public ActionResult XACNHANKETQUAHOCTAP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.XACNHANKETQUAHOCTAP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.XACNHANKETQUAHOCTAP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + i];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSXACNHANKETQUAHOCTAP");
            }
            return View(thutuc);
        }



        [HttpPost]
        public ActionResult XACNHANTHOIGIANTHUCTAP(int? id, [Bind(Include = "id, name")] thutuc thutuc, HttpPostedFileBase file)
        {

            var thongtin = db.loaithutucs.Where(i => i.loai == Constants.XACNHANTHOIGIANTHUCTAP).ToList();

            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
                    thutuc.nguoitao = taikhoan.id;
                    thutuc.loaithutuc = Constants.XACNHANTHOIGIANTHUCTAP;

                    db.thutucs.Add(thutuc);
                    db.SaveChanges();
                    thutuc = db.thutucs.OrderByDescending(u => u.id).FirstOrDefault();
                    var chitietthutuc = new chitietthutuc();
                    for (int i = 0; i < thongtin.Count; i++)
                    {
                        chitietthutuc = new chitietthutuc();
                        chitietthutuc.noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc.value = Request.Form["thutuc" + (i+1)];
                        chitietthutuc.thutuc = thutuc.id;
                        db.chitietthutucs.Add(chitietthutuc);
                    }
                    db.SaveChanges();


                }
                else
                {
                    var chitietthutuc = db.chitietthutucs.Where(i => i.thutuc == id).ToList();
                    for (int i = 0; i < chitietthutuc.Count; i++)
                    {

                        chitietthutuc[i].noidung = Request.Form["thutucname" + (i+1)];
                        chitietthutuc[i].value = Request.Form["thutuc" + i];
                        chitietthutuc[i].thutuc = id;
                        db.chitietthutucs.Attach(chitietthutuc[i]);
                        db.Entry(chitietthutuc[i]).State = EntityState.Modified;  db.SaveChanges(); 
                    }
                }

                return RedirectToAction("DSXACNHANTHOIGIANTHUCTAP");
            }
            return View(thutuc);
        }


        public ActionResult HenNgayLay(int id, string form, String ngaylay)
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            thutuc thutuc = db.thutucs.Find(id);
            thutuc.ngaylay = Convert.ToDateTime(ngaylay);
            db.SaveChanges();
            var email = db.taikhoans.Find(thutuc.nguoitao);
            CommonConstants.sendMail(email.email, "Bạn có lịch lấy hồ sơ vào ngày "+ ngaylay);
            return RedirectToAction(form);
        }

        public ActionResult DaLay(int id, string form)
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            thutuc thutuc = db.thutucs.Find(id);
            thutuc.tinhtrang = 1;
            db.SaveChanges();
            var email = db.taikhoans.Find(thutuc.nguoitao);
            CommonConstants.sendMail(email.email, "Hồ sơ của bạn đã hoàn tất và được đóng trên hệ thống");
            return RedirectToAction(form);
        }



        public ActionResult TimKiem(string noidung)
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            var loaithutuc = db.loaithutucs.Where(i => i.loai == Constants.CAPBANSAOBANG).ToList();
            var chitietthutuc = db.chitietthutucs.ToList();
            var thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CAPBANSAOBANG).Where(i => i.nguoiduyet != null).ToList();
            if (taikhoan.role == 2)
            {
                thutuc = db.thutucs.Where(i => i.loaithutuc == Constants.CAPBANSAOBANG).Where(i => i.nguoitao == taikhoan.id).ToList();
            }

            ViewBag.loaithutuc = loaithutuc;
            ViewBag.thutuc = thutuc;
            ViewBag.chitietthutuc = chitietthutuc;
            return View();
        }
        public Boolean isSpam()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];

            var thutuc = db.thutucs.Where(i => i.ngaytao == DateTime.Now).Where(i => i.nguoitao == taikhoan.id).ToList();
            if (thutuc.Count > 3)
                return true;
            return false;
        }

        public ActionResult ListThuTuc()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if(taikhoan != null )
            {
                if(taikhoan.role == 2)
                {
                    return View(CommonConstants.ExecuteQuery("exec list_thu_tuc_user @id = '" + taikhoan.id + "'"));
                }
                else
                {
                    return View(CommonConstants.ExecuteQuery("exec list_thu_tuc @id = '" + taikhoan.id + "'"));
                }
                
            }
            return View();
        }



    }
}
