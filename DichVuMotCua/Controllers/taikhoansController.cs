﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DichVuMotCua.Content;
using DichVuMotCua.Models;

namespace DichVuMotCua.Controllers
{
    public class taikhoansController : Controller
    {
        private DBmodels db = new DBmodels();
        

        // GET: taikhoans
        public ActionResult Index()
        {
            taikhoan taikhoan = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (taikhoan == null)
            {
                Response.Redirect("/VanLangLogin/Index");
            }
            else if(taikhoan.role != 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View(db.taikhoans.ToList());
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: taikhoans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            taikhoan taikhoan = db.taikhoans.Find(id);
            if (taikhoan == null)
            {
                return HttpNotFound();
            }
            return View(taikhoan);
        }

        // GET: taikhoans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: taikhoans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,username,name,password,address,email,role")] taikhoan taikhoan)
        {
            if (ModelState.IsValid)
            {
                db.taikhoans.Add(taikhoan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(taikhoan);
        }

        // GET: taikhoans/Edit/5
        public ActionResult Edit(int? id)
        {
            taikhoan user = (taikhoan)Session[CommonConstants.USER_SESSION];
            if (id == null || user == null || user.id != id)
            {
                return RedirectToAction("Index", "Home");
            }
            taikhoan taikhoan = db.taikhoans.Find(id);
            if (taikhoan == null)
            {
                return HttpNotFound();
            }
            return View(taikhoan);
        }

        // POST: taikhoans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,username,name,address")] taikhoan taikhoan, HttpPostedFileBase avatar)
        {
            byte[] bytes;
            using (BinaryReader br = new BinaryReader(avatar.InputStream))
            {
                bytes = br.ReadBytes(avatar.ContentLength);
            }
            if (ModelState.IsValid)
            {
                var tkUpdate = db.taikhoans.Find(taikhoan.id);
                tkUpdate.name = taikhoan.name;
                tkUpdate.address = taikhoan.address;
                tkUpdate.avatar = bytes;
                db.Entry(tkUpdate).State = EntityState.Modified;
                db.SaveChanges();
                Session.Add(CommonConstants.USER_SESSION, tkUpdate);
                return RedirectToAction("Index");
            }
            return View(taikhoan);
        }

        // GET: taikhoans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            taikhoan taikhoan = db.taikhoans.Find(id);
            if (taikhoan == null)
            {
                return HttpNotFound();
            }
            return View(taikhoan);
        }

        // POST: taikhoans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            taikhoan taikhoan = db.taikhoans.Find(id);
            db.taikhoans.Remove(taikhoan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
