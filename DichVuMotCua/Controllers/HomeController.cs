﻿using DichVuMotCua.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DichVuMotCua.Controllers
{
    public class HomeController : BaseLoginController
    {
        public ActionResult Index()
        {
            var result = BaseLogin(null);
            return result;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Logout()
        {
            Session.Add(CommonConstants.USER_SESSION, null);
            return RedirectToAction("Index", "Home");
        }
    }
}