﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DichVuMotCua.Models;

namespace DichVuMotCua.Controllers
{
    public class loaithutucsController : BaseLoginController
    {
        private DBmodels db = new DBmodels();

        // GET: loaithutucs
        public ActionResult Index()
        {
            var result = BaseLogin(db.loaithutucs.ToList());
            return result;


        }

        // GET: loaithutucs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            loaithutuc loaithutuc = db.loaithutucs.Find(id);
            if (loaithutuc == null)
            {
                return HttpNotFound();
            }
            return View(loaithutuc);
        }

        // GET: loaithutucs/Create
        public ActionResult Create()
        {
            List<SelectListItem> listLoai = new List<SelectListItem>();
            listLoai.Add(new SelectListItem
            {
                Text = "Cấp In Thẻ Sinh Viên",
                Value = Constants.CAPINTHESINHVIEN.ToString(),
            });
            listLoai.Add(new SelectListItem
            {
                Text = "Cấp Giấy Chứng Nhận",
                Value = Constants.CAPGIAYCHUNGNHAN.ToString(),
                Selected = true
            });
            listLoai.Add(new SelectListItem
            {
                Text = "Cập Nhật Thông Tin",
                Value = Constants.CAPNHATTHONGTIN.ToString(),
            });

            listLoai.Add(new SelectListItem
            {
                Text = "Tiếp Tục Học",
                Value = Constants.TIEPTUCHOC.ToString(),
            });
            listLoai.Add(new SelectListItem
            {
                Text = "Xin Thôi Học",
                Value = Constants.XINTHOIHOC.ToString(),
            });
            ViewBag.listLoai = listLoai;
            return View();
        }

        // POST: loaithutucs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,loai")] loaithutuc loaithutuc)
        {
            if (ModelState.IsValid)
            {
                db.loaithutucs.Add(loaithutuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(loaithutuc);
        }

        // GET: loaithutucs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            loaithutuc loaithutuc = db.loaithutucs.Find(id);
            if (loaithutuc == null)
            {
                return HttpNotFound();
            }
            return View(loaithutuc);
        }

        // POST: loaithutucs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,loai")] loaithutuc loaithutuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaithutuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(loaithutuc);
        }

        // GET: loaithutucs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            loaithutuc loaithutuc = db.loaithutucs.Find(id);
            if (loaithutuc == null)
            {
                return HttpNotFound();
            }
            return View(loaithutuc);
        }

        // POST: loaithutucs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            loaithutuc loaithutuc = db.loaithutucs.Find(id);
            db.loaithutucs.Remove(loaithutuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
