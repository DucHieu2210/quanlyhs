﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace DichVuMotCua.Controllers
{
    public class WordController : Controller
    {
        // GET: Word
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase files)
        {
            // Verify that the user selected a file
            if (files != null && files.ContentLength > 0)
            {
                // extract only the filename
                var fileName = Path.GetFileName(files.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Start"), fileName);
                files.SaveAs(path);
            }
            MemoryStream target = new MemoryStream();
            files.InputStream.CopyTo(target);
            byte[] data = target.ToArray();
            Session["ByteArray"] = data;
            // redirect back to the index action to show the form once again
            DirectoryInfo convertedDocsDirectory =
                       new DirectoryInfo(Server.MapPath("~/App_Start"));
            if (!convertedDocsDirectory.Exists)
                convertedDocsDirectory.Create();
            Guid g = Guid.NewGuid();
            var htmlFileName = g.ToString() + ".html";
            String a = ConvertToHtml(files);
            ViewBag.data = a;
            return View();
        }


        public static String  ConvertToHtml(HttpPostedFileBase files)
        {
            string content = "";
            try
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open("C:\\Users\\Admin\\Desktop\\sourceASP\\2312\\quanlyhs\\DichVuMotCua\\App_Start\\MAUdangkyhoccham.doc", true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                     content = body.InnerText;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return content;
        }
        }
}