﻿using DichVuMotCua.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DichVuMotCua.Controllers
{
    public class MenuController : Controller
    {
        private DBmodels db = new DBmodels();
        // GET: Menu
        public ActionResult Index()
        {
            
            return View();
        }


        public ActionResult DSHoSo()
        {
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>()
            {

            };

            var taikhoan = (DichVuMotCua.Models.taikhoan)Session[DichVuMotCua.Content.CommonConstants.USER_SESSION];
            List<String> listUrl = new List<string>();
            if (taikhoan != null)
            {
                var lstQuyenhs = db.quyenhss.Where(i => i.id_user == taikhoan.id).ToList();
                foreach (quyenhs item in lstQuyenhs)
                {
                    switch ("DS" + item.name_thutuc)
                    {
                        case "DSXINTHOIHOC": kvpList.Insert(0, new KeyValuePair<string, string>("HSXinThoiHoc", "Xin Thôi Học")); break;
                        case "DSMOTACHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("HSMOTACHUONGTRINHDAOTAO", "Đề Nghị Cấp Giấy Mô Tả Chương Trình Đào Tạo")); break;
                        case "DSTHICAITHIENDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("HSTHICAITHIENDIEM", "Đăng Ký Thi Cải Thiện Điểm")); break;
                        case "DSGIAYXACNHANDECUONG": kvpList.Insert(0, new KeyValuePair<string, string>("HSGIAYXACNHANDECUONG", "Giấy Xác Nhận Đề Cương")); break;
                        case "DSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("HSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO", "Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo")); break;
                        case "DSXACNHANTHOIGIANTHUCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("HSXACNHANTHOIGIANTHUCTAP", "Đề Nghị Cấp Giấy Xác Nhận Thời Gian Học Tập")); break;
                        case "DSCAPBANSAOBANG": kvpList.Insert(0, new KeyValuePair<string, string>("HSCAPBANSAOBANG", "Cấp Bản Sao Văn Bằng")); break;
                        case "DSDANGKYCHUONGTRINHHOCNHANH": kvpList.Insert(0, new KeyValuePair<string, string>("HSDANGKYCHUONGTRINHHOCNHANH", "Đăng Ký Chương Trình Học Nhanh")); break;
                        case "DSTOTNGHIEPTAMTHOI": kvpList.Insert(0, new KeyValuePair<string, string>("HSTOTNGHIEPTAMTHOI", "Đề Nghị Cấp Giấy Tốt Nghiệp Tạm Thời")); break;
                        case "DSDANGKYHOCPHANTHAYTHE": kvpList.Insert(0, new KeyValuePair<string, string>("HSDANGKYHOCPHANTHAYTHE", "Đăng Ký Học Phần Thay Thế")); break;
                        case "DSDANGKYHOCTHEOTIENDOCHAM": kvpList.Insert(0, new KeyValuePair<string, string>("DSDANGKYHOCTHEOTIENDOCHAM", "Đăng ký theo tiến độ chậm")); break;
                        case "DSDENGHICAPBANGDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("HSDENGHICAPBANGDIEM", "Đề Nghị Cấp Bảng Điểm")); break;
                        case "DSDENGHICHUNGTHUCVANBANG": kvpList.Insert(0, new KeyValuePair<string, string>("HSDENGHICHUNGTHUCVANBANG", "Đề Nghị Chứng Thực Văn Bằng")); break;
                        case "DSXACNHANKETQUAHOCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("HSXACNHANKETQUAHOCTAP", "Xác Nhận Kết Quả Học Tập")); break;
                        case "DSDOICHIEUCHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("HSDOICHIEUCHUONGTRINHDAOTAO", "Đối Chiếu Chương Trình Đào Tạo")); break;
                        case "DSDONDANGKYCAITHIENDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONDANGKYCAITHIENDIEM", "Đơn Đăng Ký Cải Thiện Điểm")); break;
                        case "DSDONPHUCKHAO": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONPHUCKHAO", "Đơn Phúc Khảo")); break;
                        case "DSDONRUTBOTHOCPHAN": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONRUTBOTHOCPHAN", "Đơn Rút Bớt Học Phần")); break;
                        case "DSDONXETTOTNGHIEPSOM": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONXETTOTNGHIEPSOM", "Đơn Xét Tốt Nghiệp Sớm")); break;
                        case "DSDONXINCHUYENNGANH": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONXINCHUYENNGANH", "Đơn Xin Chuyển Ngành")); break;
                        case "DSDONXINNHAPHOC": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONXINNHAPHOC", "Đơn Xin Nhập Học")); break;
                        case "DSDONXINTAMNGUNGHOC": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONXINTAMNGUNGHOC", "Đơn Xin Tạm Ngừng Học")); break;
                        case "DSDONXINTHOIHOCRUTHOSO": kvpList.Insert(0, new KeyValuePair<string, string>("HSDONXINTHOIHOCRUTHOSO", "Đơn Xin Thôi Học Rút Hồ Sơ")); break;
                        case "DSGIAYGIOITHIEUTHUCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("HSGIAYGIOITHIEUTHUCTAP", "Giấy Giới Thiệu Thực Tập")); break;
                    }

                }

            }

            ViewBag.listUrl = kvpList;
            return View();
        }

        public ActionResult HoSo()
        {

            return View();
        }
        public ActionResult ThuTuc()
        {
            var session = (DichVuMotCua.Models.taikhoan)Session[DichVuMotCua.Content.CommonConstants.USER_SESSION];
            var isSpam = false;
            if (session != null)
            {
                isSpam = DichVuMotCua.Content.CommonConstants.isSpam(session.id);
            }
            ViewBag.isSpam = isSpam;
            return View();
        }

        public ActionResult DSThuTuc()
        {
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>()
{

};

    var taikhoan = (DichVuMotCua.Models.taikhoan)Session[DichVuMotCua.Content.CommonConstants.USER_SESSION];
            List<String> listUrl = new List<string>();
            if(taikhoan != null)
            {
                var lstQuyenhs = db.quyenhss.Where(i => i.id_user == taikhoan.id).ToList();
                foreach(quyenhs item in lstQuyenhs ) {
                    switch("DS"+item.name_thutuc)
                    {
                        case "DSXINTHOIHOC": kvpList.Insert(0, new KeyValuePair<string, string>("DSXinThoiHoc", "Xin Thôi Học")); break;
                        case "DSMOTACHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("DSMOTACHUONGTRINHDAOTAO", "Đề Nghị Cấp Giấy Mô Tả Chương Trình Đào Tạo")); break;
                        case "DSTHICAITHIENDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("DSTHICAITHIENDIEM", "Đăng Ký Thi Cải Thiện Điểm")); break;
                        case "DSGIAYXACNHANDECUONG": kvpList.Insert(0, new KeyValuePair<string, string>("DSGIAYXACNHANDECUONG", "Giấy Xác Nhận Đề Cương")); break;
                        case "DSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("DSGIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO", "Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo")); break;
                        case "DSXACNHANTHOIGIANTHUCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("DSXACNHANTHOIGIANTHUCTAP", "Đề Nghị Cấp Giấy Xác Nhận Thời Gian Học Tập")); break;
                        case "DSCAPBANSAOBANG": kvpList.Insert(0, new KeyValuePair<string, string>("DSCAPBANSAOBANG", "Cấp Bản Sao Văn Bằng")); break;
                        case "DSDANGKYCHUONGTRINHHOCNHANH": kvpList.Insert(0, new KeyValuePair<string, string>("DSDANGKYCHUONGTRINHHOCNHANH", "Đăng Ký Chương Trình Học Nhanh")); break;
                        case "DSTOTNGHIEPTAMTHOI": kvpList.Insert(0, new KeyValuePair<string, string>("DSTOTNGHIEPTAMTHOI", "Đề Nghị Cấp Giấy Tốt Nghiệp Tạm Thời")); break;
                        case "DSDANGKYHOCPHANTHAYTHE": kvpList.Insert(0, new KeyValuePair<string, string>("DSDANGKYHOCPHANTHAYTHE", "Đăng Ký Học Phần Thay Thế")); break;
                        case "DSDANGKYHOCTHEOTIENDOCHAM": kvpList.Insert(0, new KeyValuePair<string, string>("DSDANGKYHOCTHEOTIENDOCHAM", "Đăng ký theo tiến độ chậm")); break;
                        case "DSDENGHICAPBANGDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("DSDENGHICAPBANGDIEM", "Đề Nghị Cấp Bảng Điểm")); break;
                        case "DSDENGHICHUNGTHUCVANBANG": kvpList.Insert(0, new KeyValuePair<string, string>("DSDENGHICHUNGTHUCVANBANG", "Đề Nghị Chứng Thực Văn Bằng")); break;
                        case "DSXACNHANKETQUAHOCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("DSXACNHANKETQUAHOCTAP", "Xác Nhận Kết Quả Học Tập")); break;
                        case "DSDOICHIEUCHUONGTRINHDAOTAO": kvpList.Insert(0, new KeyValuePair<string, string>("DSDOICHIEUCHUONGTRINHDAOTAO", "Đối Chiếu Chương Trình Đào Tạo")); break;
                        case "DSDONDANGKYCAITHIENDIEM": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONDANGKYCAITHIENDIEM", "Đơn Đăng Ký Cải Thiện Điểm")); break;
                        case "DSDONPHUCKHAO": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONPHUCKHAO", "Đơn Phúc Khảo")); break;
                        case "DSDONRUTBOTHOCPHAN": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONRUTBOTHOCPHAN", "Đơn Rút Bớt Học Phần")); break;
                        case "DSDONXETTOTNGHIEPSOM": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONXETTOTNGHIEPSOM", "Đơn Xét Tốt Nghiệp Sớm")); break;
                        case "DSDONXINCHUYENNGANH": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONXINCHUYENNGANH", "Đơn Xin Chuyển Ngành")); break;
                        case "DSDONXINNHAPHOC": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONXINNHAPHOC", "Đơn Xin Nhập Học")); break;
                        case "DSDONXINTAMNGUNGHOC": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONXINTAMNGUNGHOC", "Đơn Xin Tạm Ngừng Học")); break;
                        case "DSDONXINTHOIHOCRUTHOSO": kvpList.Insert(0, new KeyValuePair<string, string>("DSDONXINTHOIHOCRUTHOSO", "Đơn Xin Thôi Học Rút Hồ Sơ")); break;
                        case "DSGIAYGIOITHIEUTHUCTAP": kvpList.Insert(0, new KeyValuePair<string, string>("DSGIAYGIOITHIEUTHUCTAP", "Giấy Giới Thiệu Thực Tập")); break;
                    }

                }
               
            }

            ViewBag.listUrl = kvpList;

            return View();
        }
    }
}