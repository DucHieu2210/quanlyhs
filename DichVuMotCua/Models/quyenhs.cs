namespace DichVuMotCua.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("quyenhs")]
    public partial class quyenhs
    {
        public int id { get; set; }


        public int? id_user { get; set; }

        public int? id_thutuc { get; set; }

        [StringLength(250)]
        public string name_thutuc { get; set; }

        [StringLength(250)]
        public string name_user { get; set; }

        [StringLength(250)]
        public string thutuc_hienthi { get; set; }
        
    }
}
