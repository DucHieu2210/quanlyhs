namespace DichVuMotCua.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("khoa")]
    public partial class khoa
    {
        public int id { get; set; }

        [StringLength(200)]
        public string name { get; set; }
    }
}
