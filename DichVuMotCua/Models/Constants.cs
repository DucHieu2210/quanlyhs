﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DichVuMotCua.Models
{
    public class Constants
    {
        public static int CAPGIAYCHUNGNHAN = 1;
        public static int CAPINTHESINHVIEN = 2;
        public static int CAPNHATTHONGTIN = 3;
        public static int TIEPTUCHOC = 4;
        public static int XINTHOIHOC = 5;
        public static int BAOLUUKETQUAHOCTAP = 6;
        public static int BAOLUUKETQUATUYENSINH = 7;
        public static int CHUYENDIEM = 8;
        public static int HOCCHAM = 9;
        public static int HOCVUOT = 10;
        public static int CAPBANSAOBANG = 12;
        public static int DANGKYCHUONGTRINHHOCNHANH = 13;
        public static int DANGKYHOCLAI = 14;
        public static int DANGKYHOCPHANTHAYTHE = 15;
        public static int DANGKYHOCTHEOTIENDOCHAM = 16;
        public static int DENGHICAPBANGDIEM = 17;
        public static int DENGHICHUNGTHUCVANBANG = 18;
        public static int DIEUCHINHKEHOACHHOCTAP = 19;
        public static int DOICHIEUCHUONGTRINHDAOTAO = 20;
        public static int DONDANGKYCAITHIENDIEM = 21;
        public static int DONPHUCKHAO = 22;
        public static int DONRUTBOTHOCPHAN = 23;
        public static int DONXETTOTNGHIEPSOM = 24;
        public static int DONXINCHUYENLOP = 25;
        public static int DONXINCHUYENNGANH = 26;
        public static int DONXINNHAPHOC = 27;
        public static int DONXINTAMNGUNGHOC = 28;
        public static int DONXINTHOIHOCRUTHOSO = 29;
        public static int GIAYGIOITHIEUTHUCTAP = 30;
        public static int GIAYXACNHANDECUONG = 31;
        public static int GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO = 32;
        public static int HOCHAICHUONGTRINH = 33;
        public static int KEHOACHHOCTAP = 34;
        public static int MOTACHUONGTRINHDAOTAO = 35;
        public static int THICAITHIENDIEM = 36;
        public static int TOTNGHIEPTAMTHOI = 37;
        public static int XACNHANKETQUAHOCTAP = 38;
        public static int XACNHANTHOIGIANTHUCTAP = 39;


    }
}