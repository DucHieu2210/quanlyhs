namespace DichVuMotCua.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("hocphan")]
    public partial class hocphan
    {
        public int id { get; set; }

        [StringLength(255)]
        public string mahocphan { get; set; }

        [StringLength(255)]
        public string tenhocphan { get; set; }

        [StringLength(255)]
        public string soDVHT { get; set; }

        public DateTime thoigianhoclai { get; set; }

        public int? thutuc_id { get; set; }
    }
}
