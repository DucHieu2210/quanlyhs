namespace DichVuMotCua.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("thutuc")]
    public partial class thutuc
    {
        public int id { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        public int? nguoitao { get; set; }

        public int? nguoiduyet { get; set; }

        public int? loaithutuc { get; set; }

        public string ghichu { get; set; }

        public DateTime? ngaylay { get; set; }

        public int? tinhtrang { get; set; }

        public DateTime? ngaytao { get; set; }
    }
}
