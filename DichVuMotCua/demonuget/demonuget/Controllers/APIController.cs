﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demonuget.Controllers
{
    public class APIController : Controller
    {
        // GET: API
        public ActionResult Index()
        {
            ViewBag.name = "null";
            if (Request.IsAuthenticated)
            {
                ViewBag.name = User.Identity.Name;
            }
            return View();
        }
    }
}