﻿using DichVuMotCua.Models;
using OpenXmlPowerTools.HtmlToWml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Management.Automation;
using System.Net.Mail;
using System.Web;

namespace DichVuMotCua.Content
{
    public class CommonConstants
    {
        public static string USER_SESSION = "USER_SESSION";

        public static string ROLE_SESSION = "ROLE_SESSION";

        public static string ADMIN_SESSION = "ADMIN_SESSION";

        public static Boolean SPAM = false;

        private DBmodels db = new DBmodels();

        public static void sendMail(String email, String body)
        {
            SmtpClient smtpClient = new SmtpClient("smtp-mail.outlook.com", 587); //587
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential("t15ct03@vanlanguni.vn", "VLUt15ct03");

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("t15ct03@vanlanguni.vn", "CTMIS-no-reply");
            mail.To.Add(new MailAddress(email));
            mail.Subject = "Thông báo tại hệ thống Văn Lang";
            mail.Body = String.Format("Chào {0} hồ sơ của bạn có cập nhật với nội dung: {1} \n cảm ơn bạn đã sử dụng dịch vụ.!! \n Đây là tin nhắn tự động bạn vui lòng không trả lời tin nhắn này", email, body);
            smtpClient.Send(mail);
        }




        public static Boolean isSpam(int id)
        {


            String query = String.Format("select * from thutuc where nguoitao = {0} and ngaytao = FORMAT(getdate(), 'yyyyMMdd')", id);
            if (ExecuteQuery(query).Rows.Count > 2)
                return true;
            return false;
        }

        public static int thutucthongbao(int id)
        {


            String query = String.Format ("select * from thutuc where nguoiduyet is null and ngaylay is null and tinhtrang is null and loaithutuc in (select id_thutuc from quyenhs where id_user ={0})", id);
            return ExecuteQuery(query).Rows.Count;
        }

        public static Boolean isRole(String thutuc, int id)
        {

            List<String> listRole = new List<string>();
            listRole.Add("CAPGIAYCHUNGNHAN");
            listRole.Add("CAPINTHESINHVIEN");
            listRole.Add("CAPNHATTHONGTIN");
            listRole.Add("TIEPTUCHOC");
            listRole.Add("XINTHOIHOC");
            listRole.Add("BAOLUUKETQUAHOCTAP");
            listRole.Add("BAOLUUKETQUATUYENSINH");
            listRole.Add("CHUYENDIEM");
            listRole.Add("HOCCHAM");
            listRole.Add("HOCVUOT");
            listRole.Add("CAPBANSAOBANG");
            listRole.Add("DANGKYCHUONGTRINHHOCNHANH");
            listRole.Add("DANGKYHOCLAI");
            listRole.Add("DANGKYHOCPHANTHAYTHE");
            listRole.Add("DANGKYHOCTHEOTIENDOCHAM");
            listRole.Add("DENGHICAPBANGDIEM");
            listRole.Add("DENGHICHUNGTHUCVANBANG");
            listRole.Add("DIEUCHINHKEHOACHHOCTAP");
            listRole.Add("DOICHIEUCHUONGTRINHDAOTAO");
            listRole.Add("DONDANGKYCAITHIENDIEM");
            listRole.Add("DONPHUCKHAO");
            listRole.Add("DONRUTBOTHOCPHAN");
            listRole.Add("DONXETTOTNGHIEPSOM");
            listRole.Add("DONXINCHUYENLOP");
            listRole.Add("DONXINCHUYENNGANH");
            listRole.Add("DONXINNHAPHOC");
            listRole.Add("DONXINTAMNGUNGHOC");
            listRole.Add("DONXINTHOIHOCRUTHOSO");
            listRole.Add("GIAYGIOITHIEUTHUCTAP");
            listRole.Add("GIAYXACNHANDECUONG");
            listRole.Add("GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO");
            listRole.Add("HOCHAICHUONGTRINH");
            listRole.Add("KEHOACHHOCTAP");
            listRole.Add("MOTACHUONGTRINHDAOTAO");
            listRole.Add("THICAITHIENDIEM");
            listRole.Add("TOTNGHIEPTAMTHOI");
            listRole.Add("XACNHANKETQUAHOCTAP");
            listRole.Add("XACNHANTHOIGIANTHUCTAP");

            if (listRole.Contains(thutuc) || listRole.Contains(thutuc.Replace("DS","")) || listRole.Contains(thutuc.Replace("HS","")))
            {
                String query = String.Format("select * from quyenhs where id_user = {0} and name_thutuc = '{1}'", id, thutuc.Replace("HS","").Replace("DS",""));
                if (ExecuteQuery(query).Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                  

               
            }
            return true;
        }

        public static DataTable ExecuteQuery(String query)
        {
            String strSQL = @"Data Source=.\SQLEXPRESS;Initial Catalog=DichVuMotCua;Integrated Security=True";
            DataTable table = new DataTable();
            using (SqlConnection con = new SqlConnection(strSQL))
            {
                con.Open();
                SqlCommand cm = new SqlCommand(query, con);
                SqlDataAdapter adapter = new SqlDataAdapter(cm);
                adapter.Fill(table);
                con.Close();
            }
            return table;
        }
    }
}