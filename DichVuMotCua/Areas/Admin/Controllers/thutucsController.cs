﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DichVuMotCua.Content;
using DichVuMotCua.Models;
using Newtonsoft.Json;

namespace DichVuMotCua.Areas.Admin.Controllers
{
    public class thutucsController : Controller
    {
        private DBmodels db = new DBmodels();
        private static String strSQL = @"Data Source=.\SQLEXPRESS;Initial Catalog=DichVuMotCua;Integrated Security=True";

        // GET: Admin/thutucs
        public ActionResult Index()
        {
            return View(db.thutucs.ToList());
        }

        public ActionResult DSHoSoDaXuLy()
        {

            String query = String.Format("select tt.id, u.email as 'Người tạo' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is not null and tt.tinhtrang = 1", buiderQueryLoaiThutuc());
            return View(ExecuteQuery(query));
        }

        public ActionResult DSThuTucDangXuLy()
        {

            String query = String.Format("select tt.id, u.email as 'Người tạo', u2.email as 'Người Duyệt' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is not null", buiderQueryLoaiThutuc());
            return View(ExecuteQuery(query));
        }

        public ActionResult DSThuTucChuaXuLy()
        {

            String query = String.Format("select tt.id, u.email as 'Người tạo', u2.email as 'Người Duyệt' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is null", buiderQueryLoaiThutuc());
            return View(ExecuteQuery(query));
        }

        public DataTable ExecuteQuery(String query)
        {
            DataTable table = new DataTable();
            using (SqlConnection con = new SqlConnection(strSQL))
            {
                con.Open();
                SqlCommand cm = new SqlCommand(query, con);
                SqlDataAdapter adapter = new SqlDataAdapter(cm);
                adapter.Fill(table);
                con.Close();
            }
            return table;
        }

        public String buiderQueryLoaiThutuc()
        {
            String query = "CASE WHEN tt.loaithutuc = 1 THEN N'Cấp Giấy Chứng Nhận' WHEN tt.loaithutuc = 2 THEN N'Cấp In Thẻ Sinh Viên' WHEN tt.loaithutuc = 3 THEN N'Cập Nhật Thông Tin' WHEN tt.loaithutuc = 4 THEN N'Tiếp Tục Học' WHEN tt.loaithutuc = 5 THEN N'Xin Thôi Học' WHEN tt.loaithutuc = 6 THEN N'Bảo Lưu Kết Quả Học Tập' WHEN tt.loaithutuc = 7 THEN N'Bảo Lưu Kết Quả Tuyển Sinh' WHEN tt.loaithutuc = 8 THEN N'Chuyển Điểm' WHEN tt.loaithutuc = 9 THEN N'Đăng Ký Học Chậm' WHEN tt.loaithutuc = 10 THEN N'Đăng Ký Học Vượt' WHEN tt.loaithutuc = 12 THEN N'Cấp Bản Sao Văn Bằng' WHEN tt.loaithutuc = 13 THEN N'Đăng Ký Chương Trình Học Nhanh' WHEN tt.loaithutuc = 14 THEN N'Đăng Ký Học Lại' WHEN tt.loaithutuc = 15 THEN N'Đăng Ký Học Phần Thay Thế' WHEN tt.loaithutuc = 16 THEN N'Đăng Ký Học Theo Tiến Độ Chậm' WHEN tt.loaithutuc = 17 THEN N'Đề Nghị Cấp Bảng Điểm' WHEN tt.loaithutuc = 18 THEN N'Đề Nghị Chứng Thực Văn Bằng' WHEN tt.loaithutuc = 19 THEN N'Điều Chỉnh Kết Quả Học Tập' WHEN tt.loaithutuc = 20 THEN N'Đối Chiếu Chương Trình Đào Tạo' WHEN tt.loaithutuc = 21 THEN N'Đơn Đăng Ký Cải Thiện Điểm' WHEN tt.loaithutuc = 22 THEN N'Đơn Phúc Khảo' WHEN tt.loaithutuc = 23 THEN N'Đơn Rút Bớt Học Phần' WHEN tt.loaithutuc = 24 THEN N'Đơn Xét Tốt Nghiệp Sớm' WHEN tt.loaithutuc = 25 THEN N'Đơn Xin Chuyển Lớp' WHEN tt.loaithutuc = 26 THEN N'Đơn Xin Chuyển Ngành' WHEN tt.loaithutuc = 27 THEN N'Đơn Xin Nhập Học' WHEN tt.loaithutuc = 28 THEN N'Đơn Xin Tạm Ngừng Học' WHEN tt.loaithutuc = 29 THEN N'Đơn Xin Thôi Học Rút Hồ Sơ' WHEN tt.loaithutuc = 30 THEN N'Giấy Giới Thiệu Thực Tập ' WHEN tt.loaithutuc = 31 THEN N'Giấy Xác Nhận Đề Cương' WHEN tt.loaithutuc = 32 THEN N'Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo' WHEN tt.loaithutuc = 33 THEN N'Học Hai Chương Trình' WHEN tt.loaithutuc = 34 THEN N'Kế Hoạch Học Tập' WHEN tt.loaithutuc = 35 THEN N'Mô Tả Chương Trình Đào Tạo' WHEN tt.loaithutuc = 36 THEN N'Thi Cải Thiện Điểm' WHEN tt.loaithutuc = 37 THEN N'Tốt Nghiệp Tạm Thời' WHEN tt.loaithutuc = 38 THEN N'Xác Nhận Kết Quả Thực Tập' WHEN tt.loaithutuc = 39 THEN N'Xác Nhận Thời Gian Thực Tập' ELSE NULL END as 'Loại Thủ Tục'";
            return query;
        }


        public ActionResult PieChart()
        {
            // hs đã xử lý
            String query = String.Format("select tt.id, u.email as 'Người tạo' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is not null and tt.tinhtrang = 1", buiderQueryLoaiThutuc());
            int hsDaXuLy = ExecuteQuery(query).Rows.Count;

            // thu tuc dang xu ly
            query = String.Format("select tt.id, u.email as 'Người tạo', u2.email as 'Người Duyệt' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is not null", buiderQueryLoaiThutuc());
            int hsDangXuLy = ExecuteQuery(query).Rows.Count;
            //hs chua xu ly 
            query = String.Format("select tt.id, u.email as 'Người tạo', u2.email as 'Người Duyệt' ,  {0} from thutuc tt join taikhoan u on tt.nguoitao = u.id left join taikhoan u2 on tt.nguoiduyet = u2.id where tt.nguoiduyet is null", buiderQueryLoaiThutuc());
            int hsChuaXuLy = ExecuteQuery(query).Rows.Count;
            int total = hsDaXuLy + hsDangXuLy + hsChuaXuLy;

            double DaXuLy = Math.Round((double)hsDaXuLy * 100 / (double)total, 2);
            double DangXuLy = Math.Round((double)hsDangXuLy * 100 / (double)total, 2);
            double ChuaXuLy = Math.Round((double)hsChuaXuLy * 100 / (double)total, 2);

            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints.Add(new DataPoint("(" + hsDaXuLy + ")Thủ Tục Đã Xử Lý", DaXuLy));
            dataPoints.Add(new DataPoint("(" + hsDangXuLy + ")Thủ Tục Đang Xử Lý", DangXuLy));
            dataPoints.Add(new DataPoint("(" + hsChuaXuLy + ")Thủ Tục Chưa Xử Lý", ChuaXuLy));


            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);

            return View();
        }

    }
}
