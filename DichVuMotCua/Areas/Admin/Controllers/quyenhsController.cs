﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DichVuMotCua.Models;

namespace DichVuMotCua.Areas.Admin.Controllers
{
    public class quyenhsController : Controller
    {
        private DBmodels db = new DBmodels();

        // GET: Admin/quyenhs
        public ActionResult Index()
        {
            return View(db.quyenhss.ToList());
        }

        public ActionResult setQuyenTT(int id)
        {
            ViewBag.taikhoan = db.taikhoans.Find(id);
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>()
            {

            };
            kvpList.Insert(0, new KeyValuePair<string, string>("5", "Xin Thôi Học"));
            kvpList.Insert(0, new KeyValuePair<string, string>("12", "Cấp Bản Sao Văn Bằng"));
            kvpList.Insert(0, new KeyValuePair<string, string>("13", "Đăng Ký Chương Trình Học Nhanh"));
            kvpList.Insert(0, new KeyValuePair<string, string>("15", "Đăng Ký Học Phần Thay Thế"));
            kvpList.Insert(0, new KeyValuePair<string, string>("16", "Đăng Ký Học Theo Tiến Độ Chậm"));
            kvpList.Insert(0, new KeyValuePair<string, string>("17", "Đề Nghị Cấp Bảng Điểm"));
            kvpList.Insert(0, new KeyValuePair<string, string>("20", "Đối Chiếu Chương Trình Đào Tạo"));
            kvpList.Insert(0, new KeyValuePair<string, string>("21", "Đơn Đăng Ký Cải Thiện Điểm"));
            kvpList.Insert(0, new KeyValuePair<string, string>("22", "Đơn Phúc Khảo"));
            kvpList.Insert(0, new KeyValuePair<string, string>("23", "Đơn Rút Bớt Học Phần"));
            kvpList.Insert(0, new KeyValuePair<string, string>("24", "Đơn Xét Tốt Nghiệp Sớm"));
            kvpList.Insert(0, new KeyValuePair<string, string>("26", "Đơn Xin Chuyển Ngành"));
            kvpList.Insert(0, new KeyValuePair<string, string>("27", "Đơn Xin Nhập Học"));
            kvpList.Insert(0, new KeyValuePair<string, string>("28", "Đơn Xin Tạm Ngừng Học"));
            kvpList.Insert(0, new KeyValuePair<string, string>("29", "Đơn Xin Thôi Học Rút Hồ Sơ"));
            kvpList.Insert(0, new KeyValuePair<string, string>("30", "Giấy Giới Thiệu Thực Tập"));
            kvpList.Insert(0, new KeyValuePair<string, string>("31", "Giấy Xác Nhận Đề Cương"));
            kvpList.Insert(0, new KeyValuePair<string, string>("32", "Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo"));
            kvpList.Insert(0, new KeyValuePair<string, string>("33", "Học Hai Chương Trình"));
            kvpList.Insert(0, new KeyValuePair<string, string>("35", "Mô Tả Chương Trình Đào Tạo"));
            kvpList.Insert(0, new KeyValuePair<string, string>("36", "Thi Cải Thiện Điểm"));
            kvpList.Insert(0, new KeyValuePair<string, string>("37", "Tốt Nghiệp Tạm Thời"));
            kvpList.Insert(0, new KeyValuePair<string, string>("39", "Xác Nhận Thời Gian Thực Tập"));



            var taikhoan = db.taikhoans.Find(id);
            List<String> listUrl = new List<string>();
            if (taikhoan != null)
            {
                var lstQuyenhs = db.quyenhss.Where(i => i.id_user == taikhoan.id).ToList();
                foreach (quyenhs item in lstQuyenhs)
                {
                    kvpList.RemoveAll(x => x.Key.Equals(item.id_thutuc+""));
                }


            }

            ViewBag.listUrl = kvpList;
            return View();
        }

        public ActionResult dsQuyenTT(int? id)
        {
            ViewBag.taikhoan = db.taikhoans.Find(id);


            return View(db.quyenhss.Where(i => i.id_user == id).ToList());
        }
        // GET: Admin/quyenhs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quyenhs quyenhs = db.quyenhss.Find(id);
            if (quyenhs == null)
            {
                return HttpNotFound();
            }
            return View(quyenhs);
        }

        // GET: Admin/quyenhs/Create
        public ActionResult Create()
        {

            ViewBag.taikhoan = db.taikhoans.Where(i => i.role != 2 ).ToList();
            return View();
        }

        // POST: Admin/quyenhs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_user,id_thutuc,name_thutuc,name_user")] quyenhs quyenhs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.quyenhss.Add(quyenhs);
                    db.SaveChanges();
                    return RedirectToAction("dsQuyenTT", new { id = quyenhs.id_user });
                }
            } catch(Exception)
            {
                return RedirectToAction("dsQuyenTT", new { id = quyenhs.id_user });
            }
            

            return View(quyenhs);
        }

        // GET: Admin/quyenhs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quyenhs quyenhs = db.quyenhss.Find(id);
            if (quyenhs == null)
            {
                return HttpNotFound();
            }
            return View(quyenhs);
        }

        // POST: Admin/quyenhs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_user,id_thutuc,name_thutuc,name_user")] quyenhs quyenhs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quyenhs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(quyenhs);
        }

        // GET: Admin/quyenhs/Delete/5
        public ActionResult Delete(int? id, string forword, int? idUser)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quyenhs quyenhs = db.quyenhss.Find(id);
            db.quyenhss.Remove(quyenhs);
            db.SaveChanges();
            if("dsQuyenTT".Equals(forword))
            {

                return RedirectToAction("dsQuyenTT", new { id = idUser });
            }
            return RedirectToAction("Index");
        }

        // POST: Admin/quyenhs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            quyenhs quyenhs = db.quyenhss.Find(id);
            db.quyenhss.Remove(quyenhs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
