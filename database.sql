﻿
USE [master]
GO
/****** Object:  Database [DichVuMotCua]    Script Date: 6/5/2020 1:22:09 PM ******/
CREATE DATABASE [DichVuMotCua]
GO
USE [DichVuMotCua]
GO
ALTER DATABASE [DichVuMotCua] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DichVuMotCua].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DichVuMotCua] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DichVuMotCua] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DichVuMotCua] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DichVuMotCua] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DichVuMotCua] SET ARITHABORT OFF 
GO
ALTER DATABASE [DichVuMotCua] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DichVuMotCua] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DichVuMotCua] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DichVuMotCua] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DichVuMotCua] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DichVuMotCua] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DichVuMotCua] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DichVuMotCua] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DichVuMotCua] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DichVuMotCua] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DichVuMotCua] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DichVuMotCua] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DichVuMotCua] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DichVuMotCua] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DichVuMotCua] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DichVuMotCua] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DichVuMotCua] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DichVuMotCua] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DichVuMotCua] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DichVuMotCua] SET  MULTI_USER 
GO
ALTER DATABASE [DichVuMotCua] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DichVuMotCua] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DichVuMotCua] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DichVuMotCua] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [DichVuMotCua]
GO
/****** Object:  Table [dbo].[chitiethoso]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chitiethoso](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[noidung] [nvarchar](255) NULL,
	[hoso] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[chitietthutuc]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chitietthutuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[noidung] [nvarchar](255) NULL,
	[thutuc] [int] NULL,
	[value] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hocphan]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hocphan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mahocphan] [nvarchar](255) NULL,
	[tenhocphan] [nvarchar](255) NULL,
	[soDVHT] [nvarchar](255) NULL,
	[thoigianhoclai] [date] NULL,
	[thutuc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hoso]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hoso](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[noidung] [nvarchar](255) NULL,
	[nguoitao] [int] NULL,
	[nguoiduyet] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[loaithutuc]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[loaithutuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[loai] [int] NULL,
	[name] [nvarchar](255) NULL,
	[hienthi] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[quyen]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quyen](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[quyenhs]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quyenhs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_user] [int] NULL,
	[id_thutuc] [int] NULL,
	[name_thutuc] [nvarchar](250) NULL,
	[name_user] [nvarchar](250) NULL,
	[thutuc_hienthi] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taikhoan]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[taikhoan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](100) NULL,
	[name] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[role] [int] NULL,
	[avatar] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[thutuc]    Script Date: 6/5/2020 1:22:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[thutuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[nguoitao] [int] NULL,
	[nguoiduyet] [int] NULL,
	[loaithutuc] [int] NULL,
	[ghichu] [nvarchar](255) NULL,
	[ngaylay] [date] NULL,
	[tinhtrang] [int] NULL,
	[ngaytao] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[chitiethoso] ON 

INSERT [dbo].[chitiethoso] ([id], [noidung], [hoso]) VALUES (1, N'1', 1)
INSERT [dbo].[chitiethoso] ([id], [noidung], [hoso]) VALUES (2, N'dddd', 1)
SET IDENTITY_INSERT [dbo].[chitiethoso] OFF
SET IDENTITY_INSERT [dbo].[chitietthutuc] ON 

INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (1, N'q', 1, N'q')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (2, N'fdfdfdfdf', 0, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (3, N'11111111111111111111', 4, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (4, N'1', 5, N'11111111111111111111111111111')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (5, N'1', 6, N'dddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (6, N'Email', 6, N'dddddddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (7, NULL, 7, N'ggggggggggggggg')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (8, NULL, 8, N'ggggggggggggggg')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (9, NULL, 9, N'fffffffffffff')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (10, N'1', 1, N'rrrrrrrrrr')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (11, N'Email', 1, N'gggg')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (12, N'1', 2, N'ffff')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (13, N'Email', 2, N'ffff')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (14, NULL, 5, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (15, NULL, 5, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (16, NULL, 5, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (17, NULL, 5, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (18, NULL, 5, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (19, NULL, 6, N'22222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (20, NULL, 6, N'222222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (21, NULL, 6, N'2222222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (22, NULL, 6, N'22222222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (23, NULL, 6, N'22222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (24, NULL, 7, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (25, NULL, 7, N'16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (26, NULL, 7, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (27, NULL, 7, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (28, NULL, 7, N'4.6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (29, NULL, 8, N'Họ Và Tên,Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (30, NULL, 8, N'Ngày Sinh,16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (31, NULL, 8, N'Lớp,KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (32, NULL, 8, N'Khoa,CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (33, NULL, 8, N'ĐVHT,3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (34, NULL, 8, N'Thời Gian Nghỉ,12/12/2019')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (35, NULL, 8, N'Thời Gian Đi Học ,12/12/2020')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (36, NULL, 8, N'Lý Do,đéo thích đi học')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (37, N'Họ Và Tên', 9, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (38, N'Ngày Sinh', 9, N'16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (39, N'Lớp', 9, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (40, N'Khoa', 9, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (41, N'ĐVHT', 9, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (42, N'Thời Gian Nghỉ', 9, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (43, N'Thời Gian Đi Học ', 9, N'2020-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (44, N'Lý Do', 9, N'đéo thích đi học')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (45, N'Họ Và Tên', 10, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (46, N'Mã SV', 10, N'0941060373')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (47, N'Ngày Sinh', 10, N'16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (48, N'Lớp', 10, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (49, N'Khoa', 10, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (50, N'Học Kì Đã Học', 10, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (51, N'ĐVHT', 10, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (52, N'Ngày Nghỉ', 10, N'16/12/2019')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (53, N'Ngày Đi Học', 10, N'16/12/2020')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (54, N'Lý Do', 10, N'đéo thích đi học ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (55, N'Họ Và Tên', 11, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (56, N'Mã SV', 11, N'dddddddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (57, N'Ngày Sinh', 11, N'16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (58, N'Lớp', 11, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (59, N'Khoa', 11, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (60, N'Học Kì Đã Học', 11, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (61, N'ĐVHT', 11, N'2020-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (62, N'Ngày Nghỉ', 11, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (63, N'Ngày Đi Học', 11, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (64, N'Lý Do', 11, N'đéo thích học')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (65, N'Họ Và Tên', 12, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (66, N'Ngày Sinh', 12, N'16/02/1995')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (67, N'Mã SV', 12, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (68, N'Lớp', 12, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (69, N'Khoa', 12, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (70, N'Học Kì Đã Học', 12, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (71, N'ĐVHT', 12, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (72, N'Ngày Nghỉ', 12, N'2020-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (73, N'Ngày Đi Học', 12, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (74, N'Lý Do', 12, N'đéo thích học')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (75, N'Khoa', 13, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (76, N'Họ Tên', 13, N'Nguyen Van A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (77, N'Mã SV', 13, N'MSV1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (78, N'Ngày Sinh', 13, N'1999-01-01')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (79, N'Nơi Sinh', 13, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (80, N'Lớp', 13, N'Lớp 1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (81, N'Thường Trú', 13, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (82, N'SĐT', 13, N'034567891')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (83, N'TBC Học Kỳ 1', 13, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (84, N'TBC Học Kỳ 2', 13, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (85, N'TBC Học Kỳ 3', 13, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (86, N'TBC Học Kỳ 4', 13, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (87, N'TBC Học Kỳ 5', 13, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (88, N'TBC Học Kỳ 6', 13, N'6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (89, N'Bảo Lưu Từ', 13, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (90, N'Ngày Học Lại', 13, N'2021-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (91, N'Lý Do', 13, N'Hết Tiền')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (92, N'Khoa', 14, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (93, N'Họ Tên', 14, N'Nguyen Van B')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (94, N'Mã SV', 14, N'MSV1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (95, N'Ngày Sinh', 14, N'1999-01-01')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (96, N'Nơi Sinh', 14, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (97, N'Lớp', 14, N'Lớp 1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (98, N'Thường Trú', 14, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (99, N'SĐT', 14, N'034564789')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (100, N'TBC Học Kỳ 1', 14, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (101, N'TBC Học Kỳ 2', 14, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (102, N'TBC Học Kỳ 3', 14, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (103, N'TBC Học Kỳ 4', 14, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (104, N'TBC Học Kỳ 5', 14, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (105, N'TBC Học Kỳ 6', 14, N'6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (106, N'Bảo Lưu Từ', 14, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (107, N'Ngày Học Lại', 14, N'2021-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (108, N'Lý Do', 14, N'Hết Tiền')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (109, N'Họ Tên', 15, N'Nguyễn Văn A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (110, N'Mã SV', 15, N'dddddddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (111, N'Năm Sinh', 15, N'2000-01-01')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (112, N'Nơi Sinh', 15, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (113, N'Số Báo Danh', 15, N'SBD123')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (114, N'Năm Thi Tuyển', 15, N'2019')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (115, N'Ngành Trúng Tuyển', 15, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (116, N'Mã Ngành', 15, N'CNTT1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (117, N'Tổng Điểm', 15, N'12/24')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (118, N'Bảo Lưu Đến', 15, N'2022')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (119, N'Lý Do', 15, N'abxc')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (120, NULL, 16, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (121, NULL, 16, N'test form')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (122, NULL, 16, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (123, NULL, 16, N'2020-03-19')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (124, NULL, 16, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (125, NULL, 16, N'Lớp 1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (126, NULL, 16, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (127, NULL, 16, N'2222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (128, NULL, 16, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (129, NULL, 16, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (130, NULL, 16, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (131, NULL, 16, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (132, NULL, 16, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (133, NULL, 16, N'6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (134, NULL, 16, N'2020-03-19')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (135, NULL, 16, N'2020-03-21')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (136, NULL, 16, N'Hết Tiền')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (137, N'Khoa', 17, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (138, N'Họ Tên', 17, N'test form')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (139, N'Mã SV', 17, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (140, N'Ngày Sinh', 17, N'2020-03-19')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (141, N'Nơi Sinh', 17, N'sss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (142, N'Lớp', 17, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (143, N'Thường Trú', 17, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (144, N'SĐT', 17, N'1233333333')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (145, N'TBC Học Kỳ 1', 17, N'11')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (146, N'TBC Học Kỳ 2', 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (147, N'TBC Học Kỳ 3', 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (148, N'TBC Học Kỳ 4', 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (149, N'TBC Học Kỳ 5', 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (150, N'TBC Học Kỳ 6', 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (151, N'Bảo Lưu Từ', 17, N'2020-03-19')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (152, NULL, 17, N'2020-03-19')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (153, NULL, 17, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (154, N'Họ Và Tên', 19, N'test form')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (155, N'Ngày Sinh', 19, N'2020-03-18')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (156, N'Mã SV', 19, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (157, N'Lớp', 19, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (158, N'Khoa', 19, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (159, N'Học Kì Đã Học', 19, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (160, N'ĐVHT', 19, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (161, N'Ngày Nghỉ', 19, N'2020-03-16')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (162, N'Ngày Đi Học', 19, N'2020-03-27')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (163, N'Lý Do', 19, N'ddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (164, N'Họ Tên', 20, N'test form')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (165, N'Mã SV', 20, N'dddddddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (166, N'Năm Sinh', 20, N'2020-03-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (167, N'Nơi Sinh', 20, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (168, N'Số Báo Danh', 20, N'SBD123')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (169, N'Năm Thi Tuyển', 20, N'2020-03-20')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (170, N'Ngành Trúng Tuyển', 20, N'2222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (171, N'Mã Ngành', 20, N'đéo thích đi học')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (172, N'Tổng Điểm', 20, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (173, N'Bảo Lưu Đến', 20, N'2020-03-17')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (174, N'Lý Do', 20, N'111111111')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (175, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (176, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (177, NULL, 22, N'KHMT4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (178, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (179, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (180, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (181, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (182, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (183, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (184, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (185, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (186, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (187, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (188, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (189, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (190, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (191, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (192, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (193, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (194, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (195, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (196, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (197, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (198, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (199, NULL, 22, N'd')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (200, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (201, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (202, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (203, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (204, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (205, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (206, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (207, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (208, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (209, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (210, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (211, NULL, 22, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (212, N'Họ Tên', 23, N'Nguyễn Văn Thắng')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (213, N'Mã SV', 23, N'test form')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (214, N'Năm Sinh', 23, N'MSV1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (215, N'Lớp', 23, N'1999-01-01')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (216, N'Khoa', 23, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (217, N'Học Kỳ', 23, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (218, N'Khoa ', 23, N'Hà Nội')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (219, N'Trường', 23, N'2019-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (220, N'Khóa Đã Học', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (221, N'HP1', 23, N'ddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (222, N'HP2', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (223, N'HP3', 23, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (224, N'HP4', 23, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (225, N'HP5', 23, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (226, N'HP6', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (227, N'Có Trong HP đang học', 23, N'2021-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (228, N'TBTL', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (229, N'Trường Có Điểm Chuyển ĐI', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (230, N'Trường Có Điểm Chuyển Đến', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (231, N'ĐVHT1', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (232, N'ĐVHT2', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (233, N'ĐVHT3', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (234, N'ĐVHT4', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (235, N'ĐVHT5', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (236, N'ĐVHT6', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (237, N'D1', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (238, N'D2', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (239, N'D3', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (240, N'D4', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (241, N'D5', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (242, N'D6', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (243, N'GC1', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (244, N'GC2', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (245, N'GC3', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (246, N'GC4', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (247, N'GC5', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (248, N'GC6', 23, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (249, N'Họ Tên', 24, N'fdfdfdfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (250, N'Mã SV', 24, N'dddddddddddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (251, N'Năm Sinh', 24, N'2020-03-10')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (252, N'Lớp', 24, N'11')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (253, N'Khoa', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (254, N'Học Kỳ', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (255, N'Khóa', 24, N'2020-12-12')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (256, N'Trường', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (257, N'Khóa Đã Học', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (258, N'HP1', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (259, N'HP2', 24, N'11')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (260, N'HP3', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (261, N'HP4', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (262, N'HP5', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (263, N'HP6', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (264, N'Có Trong HP đang học', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (265, N'TBTL', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (266, N'Trường Có Điểm Chuyển ĐI', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (267, N'Trường Có Điểm Chuyển Đến', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (268, N'ĐVHT1', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (269, N'ĐVHT2', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (270, N'ĐVHT3', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (271, N'ĐVHT4', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (272, N'ĐVHT5', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (273, N'ĐVHT6', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (274, N'D1', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (275, N'D2', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (276, N'D3', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (277, N'D4', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (278, N'D5', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (279, N'D6', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (280, N'GC1', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (281, N'GC2', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (282, N'GC3', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (283, N'GC4', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (284, N'GC5', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (285, N'GC6', 24, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (286, N'Họ Tên', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (287, N'Mã SV', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (288, N'Ngày Sinh', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (289, N'Lớp', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (290, N'Khoa', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (291, N'Ngành', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (292, N'Số HK', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (293, N'HK1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (294, N'NH1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (295, N'D1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (296, N'HK2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (297, N'NH2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (298, N'D2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (299, N'HK3', 27, N'd')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (300, N'NH3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (301, N'D3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (302, N'HK4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (303, N'NH4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (304, N'D4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (305, N'MH1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (306, N'HP1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (307, N'ĐVHT1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (308, N'GC1', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (309, N'MH2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (310, N'HP2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (311, N'ĐVHT2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (312, N'GC2', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (313, N'MH3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (314, N'HP3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (315, N'ĐVHT3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (316, N'GC3', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (317, N'MH4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (318, N'HP4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (319, N'ĐVHT4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (320, N'GC4', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (321, N'Lý Do', 27, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (322, N'Họ Tên', 28, N'ddd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (323, N'Mã SV', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (324, N'Ngày Sinh', 28, N'2020-03-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (325, N'Lớp', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (326, N'Khoa', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (327, N'Ngành', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (328, NULL, 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (329, N'HK1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (330, N'NH1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (331, N'D1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (332, N'HK2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (333, N'NH2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (334, N'D2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (335, N'HK3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (336, N'NH3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (337, N'D3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (338, N'HK4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (339, N'NH4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (340, N'D4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (341, N'MH1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (342, N'HP1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (343, N'ĐVHT1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (344, N'GC1', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (345, N'MH2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (346, N'HP2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (347, N'ĐVHT2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (348, N'GC2', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (349, N'MH3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (350, N'HP3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (351, N'ĐVHT3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (352, N'GC3', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (353, N'MH4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (354, N'HP4', 28, N'đ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (355, N'ĐVHT4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (356, N'GC4', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (357, N'Lý Do', 28, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (358, NULL, 30, N'ssssssss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (359, NULL, 30, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (360, NULL, 30, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (361, NULL, 30, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (362, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (363, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (364, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (365, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (366, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (367, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (368, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (369, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (370, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (371, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (372, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (373, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (374, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (375, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (376, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (377, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (378, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (379, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (380, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (381, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (382, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (383, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (384, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (385, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (386, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (387, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (388, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (389, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (390, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (391, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (392, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (393, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (394, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (395, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (396, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (397, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (398, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (399, NULL, 30, N's')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (400, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (401, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (402, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (403, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (404, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (405, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (406, NULL, 30, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (407, N'Họ Tên', 31, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (408, N'Mã SV', 31, N'2222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (409, N'Ngày Sinh', 31, N'2020-04-03')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (410, N'Lớp', 31, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (411, N'Khoa', 31, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (412, N'Ngành', 31, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (413, N'Hoàn Tất', 31, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (414, N'HK1', 31, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (415, N'Năm1', 31, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (416, N'Đạt1', 31, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (417, N'HK2', 31, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (418, N'Năm2', 31, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (419, N'Đạt2', 31, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (420, N'HK3', 31, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (421, N'Năm3', 31, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (422, N'Đạt3', 31, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (423, N'HK4', 31, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (424, N'Đạt4', 31, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (425, N'Năm4', 31, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (426, N'HK5', 31, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (427, N'Đạt5', 31, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (428, N'Năm5', 31, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (429, N'HK6', 31, N' 6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (430, N'Đạt6', 31, N'6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (431, N'Năm6', 31, N'6')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (432, N'MM1', 31, N'mh1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (433, N'HP1', 31, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (434, N'DVHT1', 31, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (435, N'GC1', 31, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (436, N'MM2', 31, N'mh2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (437, N'HP2', 31, N'a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (438, N'DVHT2', 31, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (439, N'GC2', 31, N's')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (440, N'MM3', 31, N'mh3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (441, N'HP3', 31, N'a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (442, N'DVHT3', 31, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (443, N'GC3', 31, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (444, N'MM4', 31, N'mh4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (445, N'HP4', 31, N'a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (446, N'DVHT4', 31, N'5')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (447, N'GC4', 31, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (448, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (449, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (450, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (451, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (452, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (453, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (454, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (455, NULL, 31, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (456, N'Họ Và Tên', 35, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (457, N'Ngày Sinh', 35, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (458, N'Mã SV', 35, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (459, N'Lớp', 35, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (460, N'Khoa', 35, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (461, N'Học Kì Đã Học', 35, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (462, N'ĐVHT', 35, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (463, N'Ngày Nghỉ', 35, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (464, N'Ngày Đi Học', 35, N'2020-04-17')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (465, N'Lý Do', 35, N'ee')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (466, N'Họ Và Tên', 36, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (467, N'Ngày Sinh', 36, N'2020-04-06')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (468, N'Mã SV', 36, N'1221')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (469, N'Lớp', 36, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (470, N'Khoa', 36, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (471, N'Học Kì Đã Học', 36, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (472, N'ĐVHT', 36, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (473, N'Ngày Nghỉ', 36, N'2020-04-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (474, N'Ngày Đi Học', 36, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (475, N'Lý Do', 36, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (476, N'Họ Và Tên', 38, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (477, N'Ngày Sinh', 38, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (478, N'Mã SV', 38, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (479, N'Lớp', 38, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (480, N'Khoa', 38, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (481, N'Học Kì Đã Học', 38, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (482, N'ĐVHT', 38, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (483, N'Ngày Nghỉ', 38, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (484, N'Ngày Đi Học', 38, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (485, N'Lý Do', 38, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (486, N'Họ Và Tên', 38, N'nguyen van B')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (487, N'Ngày Sinh', 38, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (488, N'Mã SV', 38, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (489, N'Lớp', 38, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (490, N'Khoa', 38, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (491, N'Học Kì Đã Học', 38, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (492, N'ĐVHT', 38, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (493, N'Ngày Nghỉ', 38, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (494, N'Ngày Đi Học', 38, N'')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (495, N'Lý Do', 38, N'dsdsdsd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (496, N'Họ Và Tên', 39, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (497, N'Ngày Sinh', 39, N'2020-03-30')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (498, N'Mã SV', 39, N'ss')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (499, N'Lớp', 39, N'CNTT')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (500, N'Khoa', 39, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (501, N'Học Kì Đã Học', 39, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (502, N'ĐVHT', 39, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (503, N'Ngày Nghỉ', 39, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (504, N'Ngày Đi Học', 39, N'2020-04-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (505, N'Lý Do', 39, N'dsdsdsds')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (506, N'Họ Và Tên', 40, N'nguyen van a')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (507, N'Ngày Sinh', 40, N'2020-04-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (508, N'Mã SV', 40, N'88')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (509, N'Lớp', 40, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (510, N'Khoa', 40, N'CNTT')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (511, N'Học Kì Đã Học', 40, N'7')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (512, N'ĐVHT', 40, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (513, N'Ngày Nghỉ', 40, N'2020-04-08')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (514, N'Ngày Đi Học', 40, N'2020-04-10')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (515, N'Lý Do', 40, N'sdsad')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (516, NULL, 41, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (517, N'[DATA1]', 41, N'aaa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (518, N'[DATA2]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (519, N'[DATA3]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (520, N'[DATA4]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (521, N'[DATA5]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (522, N'[DATA6]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (523, N'[DATA7]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (524, N'[DATA8]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (525, N'[DATA9]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (526, N'[DATA10]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (527, N'[DATA11]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (528, N'[DATA12]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (529, N'[DATA13]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (530, N'[DATA14]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (531, N'[DATA15]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (532, N'[DATA16]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (533, N'[DATA17]', 41, N'aa')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (534, NULL, 42, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (535, N'[DATA1]', 42, N'fdfd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (536, N'[DATA2]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (537, N'[DATA3]', 42, N'22222222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (538, N'[DATA4]', 42, N'22222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (539, N'[DATA5]', 42, N'dfdfd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (540, N'[DATA6]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (541, N'[DATA7]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (542, N'[DATA8]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (543, N'[DATA9]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (544, N'[DATA10]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (545, N'[DATA11]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (546, N'[DATA12]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (547, N'[DATA13]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (548, N'[DATA14]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (549, N'[DATA15]', 42, N'fdfd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (550, N'[DATA16]', 42, N'fdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (551, N'[DATA17]', 42, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (552, N'Ngày Sinh', 43, N'2020-06-03')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (553, N'Mã SV', 43, N't162279')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (554, N'Lớp', 43, N'22222222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (555, N'Khoa', 43, N'22222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (556, N'Học Kì Đã Học', 43, N'dfdfd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (557, N'ĐVHT', 43, N'3')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (558, N'Ngày Nghỉ', 43, N'2020-06-03')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (559, N'Ngày Đi Học', 43, N'2020-06-04')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (560, N'Lý Do', 43, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (561, NULL, 43, NULL)
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (562, N'Họ Và Tên', 44, N'ddddddddđdddddddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (563, N'Ngày Sinh', 44, N'2020-06-05')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (564, N'Mã SV', 44, N't162279')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (565, N'Lớp', 44, N'sds')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (566, N'Khoa', 44, N'sds')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (567, N'Học Kì Đã Học', 44, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (568, N'ĐVHT', 44, N'e')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (569, N'Ngày Nghỉ', 44, N'2020-06-09')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (570, N'Ngày Đi Học', 44, N'2020-06-05')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (571, N'Lý Do', 44, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (572, N'[DATA1]', 45, N'ddddddddđdddddddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (573, N'[DATA2]', 45, N't162279')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (574, N'[DATA3]', 45, N'2020-06-04')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (575, N'[DATA4]', 45, N'22222')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (576, N'[DATA5]', 45, N'dfdfd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (577, N'[DATA6]', 45, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (578, N'[DATA7]', 45, N'dfdf')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (579, N'[DATA8]', 45, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (580, N'[DATA9]', 45, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (581, N'[DATA10]', 45, N'2')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (582, N'[DATA1]', 46, N'ddddddddđdddddddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (583, N'[DATA2]', 46, N't162279')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (584, N'[DATA3]', 46, N'2020-06-04')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (585, N'[DATA4]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (586, N'[DATA5]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (587, N'[DATA6]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (588, N'[DATA7]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (589, N'[DATA8]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (590, N'[DATA9]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (591, N'[DATA10]', 46, N'f')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (592, N'[DATA1]', 47, N'ddddddddđdddddddddđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (593, N'[DATA2]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (594, N'[DATA3]', 47, N'2020-06-04')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (595, N'[DATA4]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (596, N'[DATA5]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (597, N'[DATA6]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (598, N'[DATA7]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (599, N'[DATA8]', 47, N'd')
GO
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (600, N'[DATA9]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (601, N'[DATA10]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (602, N'[DATA11]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (603, N'[DATA12]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (604, N'[DATA13]', 47, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (605, N'[DATA1]', 48, N'Nguyen Van A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (606, N'[DATA2]', 48, N'thanhle41')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (607, N'[DATA3]', 48, N'2020-06-04')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (608, N'[DATA4]', 48, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (609, N'[DATA5]', 48, N'dđ')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (610, N'[DATA6]', 48, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (611, N'[DATA7]', 48, N'd')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (612, N'[DATA8]', 48, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (613, N'[DATA9]', 48, N'1111')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (614, N'[DATA10]', 48, N'1')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (615, N'[DATA1]', 49, N'Nguyen Van A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (616, N'[DATA2]', 49, N'thanhle41')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (617, N'[DATA3]', 49, N'2020-06-05')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (618, N'[DATA4]', 49, N'444')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (619, N'[DATA5]', 49, N'444')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (620, N'[DATA6]', 49, N'444')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (621, N'[DATA7]', 49, N'44')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (622, N'[DATA8]', 49, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (623, N'[DATA9]', 49, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (624, N'[DATA10]', 49, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (625, N'[DATA11]', 49, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (626, N'[DATA12]', 49, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (627, N'[DATA1]', 50, N'Nguyen Van A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (628, N'[DATA2]', 50, N'thanhle41')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (629, N'[DATA3]', 50, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (630, N'[DATA4]', 50, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (631, N'[DATA5]', 50, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (632, N'[DATA6]', 50, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (633, N'[DATA7]', 50, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (634, N'[DATA1]', 51, N'Nguyen Van A')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (635, N'[DATA2]', 51, N'thanhle41')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (636, N'[DATA3]', 51, N'2020-06-11')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (637, N'[DATA4]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (638, N'[DATA5]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (639, N'[DATA6]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (640, N'[DATA7]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (641, N'[DATA8]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (642, N'[DATA9]', 51, N'4')
INSERT [dbo].[chitietthutuc] ([id], [noidung], [thutuc], [value]) VALUES (643, N'[DATA10]', 51, N'4')
SET IDENTITY_INSERT [dbo].[chitietthutuc] OFF
SET IDENTITY_INSERT [dbo].[hocphan] ON 

INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (32, N'HP1', N'Học Phần 1', N'4', CAST(0xF3400B00 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (33, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (34, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (35, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (36, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (37, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (38, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (39, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (40, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (41, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (42, N'HP1', N'Học Phần 1', N'4', CAST(0xF3400B00 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (43, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (44, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (45, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (46, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (47, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (48, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (49, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (50, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (51, N'', N'', N'', CAST(0x00000000 AS Date), 38)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (52, N'HP1', N'Học Phần 1', N'2', CAST(0xF2400B00 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (53, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (54, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (55, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (56, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (57, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (58, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (59, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (60, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (61, N'', N'', N'', CAST(0x00000000 AS Date), 39)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (62, N'HP1', N'Học Phần 1', N'', CAST(0xEB400B00 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (63, N'HP2', N'Học Phần 1', N'', CAST(0xF5400B00 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (64, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (65, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (66, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (67, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (68, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (69, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (70, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (71, N'', N'', N'', CAST(0x00000000 AS Date), 40)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (72, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (73, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (74, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (75, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (76, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (77, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (78, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (79, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (80, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (81, N'3', N'3', N'1', CAST(0x2A410B00 AS Date), 43)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (82, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (83, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (84, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (85, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (86, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (87, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (88, N'4', N'4', N'4', CAST(0x2C410B00 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (89, N'', N'', N'', CAST(0x00000000 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (90, N'', N'', N'', CAST(0x00000000 AS Date), 44)
INSERT [dbo].[hocphan] ([id], [mahocphan], [tenhocphan], [soDVHT], [thoigianhoclai], [thutuc_id]) VALUES (91, N'', N'', N'', CAST(0x00000000 AS Date), 44)
SET IDENTITY_INSERT [dbo].[hocphan] OFF
SET IDENTITY_INSERT [dbo].[hoso] ON 

INSERT [dbo].[hoso] ([id], [noidung], [nguoitao], [nguoiduyet]) VALUES (1, N'z', 1, 1)
SET IDENTITY_INSERT [dbo].[hoso] OFF
SET IDENTITY_INSERT [dbo].[loaithutuc] ON 

INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (15, 5, N'Họ Và Tên', N'Họ Và Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (16, 5, N'Ngày Sinh', N'Ngày Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (17, 5, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (18, 5, N'Lớp', N'Lớp')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (19, 5, N'Khoa', N'Khoa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (20, 5, N'Học Kì Đã Học', N'Học Kì Đã Học')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (21, 5, N'ĐVHT', N'ĐVHT')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (22, 5, N'Ngày Nghỉ', N'Ngày Nghỉ')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (23, 5, N'Ngày Đi Học', N'Ngày Đi Học')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (24, 5, N'Lý Do', N'Lý Do')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (25, 6, N'Khoa', N'Khoa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (26, 6, N'Họ Tên', N'Họ Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (27, 6, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (28, 6, N'Ngày Sinh', N'Ngày Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (29, 6, N'Nơi Sinh', N'Nơi Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (30, 6, N'Lớp', N'Lớp')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (31, 6, N'Thường Trú', N'Thường Trú')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (32, 6, N'SĐT', N'SĐT')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (33, 6, N'TBC Học Kỳ 1', N'TBC Học Kỳ 1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (34, 6, N'TBC Học Kỳ 2', N'TBC Học Kỳ 2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (35, 6, N'TBC Học Kỳ 3', N'TBC Học Kỳ 3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (36, 6, N'TBC Học Kỳ 4', N'TBC Học Kỳ 4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (37, 6, N'TBC Học Kỳ 5', N'TBC Học Kỳ 5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (38, 6, N'TBC Học Kỳ 6', N'TBC Học Kỳ 6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (39, 6, N'Bảo Lưu Từ', N'Bảo Lưu Từ')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (40, 6, N'Ngày Học Lại', N'Ngày Học Lại')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (41, 6, N'Lý Do', N'Lý Do')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (42, 7, N'Họ Tên', N'Họ Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (43, 7, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (44, 7, N'Năm Sinh', N'Năm Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (45, 7, N'Nơi Sinh', N'Nơi Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (46, 7, N'Số Báo Danh', N'Số Báo Danh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (47, 7, N'Năm Thi Tuyển', N'Năm Thi Tuyển')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (48, 7, N'Ngành Trúng Tuyển', N'Ngành Trúng Tuyển')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (49, 7, N'Mã Ngành', N'Mã Ngành')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (50, 7, N'Tổng Điểm', N'Tổng Điểm')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (51, 7, N'Bảo Lưu Đến', N'Bảo Lưu Đến')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (52, 7, N'Lý Do', N'Lý Do')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (53, 8, N'Họ Tên', N'Họ Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (54, 8, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (55, 8, N'Năm Sinh', N'Năm Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (56, 8, N'Lớp', N'Lớp')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (57, 8, N'Khoa', N'Khoa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (58, 8, N'Học Kỳ', N'Học Kỳ')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (59, 8, N'Khóa', N'Khóa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (60, 8, N'Trường', N'Trường')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (61, 8, N'Khóa Đã Học', N'Khóa Đã Học')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (62, 8, N'HP1', N'HP1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (63, 8, N'HP2', N'HP2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (64, 8, N'HP3', N'HP3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (65, 8, N'HP4', N'HP4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (66, 8, N'HP5', N'HP5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (67, 8, N'HP6', N'HP6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (68, 8, N'Có Trong HP đang học', N'Có Trong HP đang học')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (69, 8, N'TBTL', N'TBTL')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (70, 8, N'Trường Có Điểm Chuyển ĐI', N'Trường Có Điểm Chuyển ĐI')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (71, 8, N'Trường Có Điểm Chuyển Đến', N'Trường Có Điểm Chuyển Đến')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (72, 8, N'ĐVHT1', N'ĐVHT1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (73, 8, N'ĐVHT2', N'ĐVHT2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (74, 8, N'ĐVHT3', N'ĐVHT3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (75, 8, N'ĐVHT4', N'ĐVHT4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (76, 8, N'ĐVHT5', N'ĐVHT5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (77, 8, N'ĐVHT6', N'ĐVHT6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (78, 8, N'D1', N'D1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (79, 8, N'D2', N'D2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (80, 8, N'D3', N'D3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (81, 8, N'D4', N'D4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (82, 8, N'D5', N'D5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (83, 8, N'D6', N'D6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (84, 8, N'GC1', N'GC1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (85, 8, N'GC2', N'GC2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (86, 8, N'GC3', N'GC3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (87, 8, N'GC4', N'GC4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (88, 8, N'GC5', N'GC5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (89, 8, N'GC6', N'GC6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (90, 9, N'Họ Tên', N'Họ Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (91, 9, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (92, 9, N'Ngày Sinh', N'Ngày Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (93, 9, N'Lớp', N'Lớp')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (94, 9, N'Khoa', N'Khoa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (95, 9, N'Ngành', N'Ngành')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (96, 9, N'Số HK', N'Số HK')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (97, 9, N'HK1', N'HK1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (98, 9, N'NH1', N'NH1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (99, 9, N'D1', N'D1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (100, 9, N'HK2', N'HK2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (101, 9, N'NH2', N'NH2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (102, 9, N'D2', N'D2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (103, 9, N'HK3', N'HK3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (104, 9, N'NH3', N'NH3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (105, 9, N'D3', N'D3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (106, 9, N'HK4', N'HK4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (107, 9, N'NH4', N'NH4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (108, 9, N'D4', N'D4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (109, 9, N'MH1', N'MH1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (110, 9, N'HP1', N'HP1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (111, 9, N'ĐVHT1', N'ĐVHT1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (112, 9, N'GC1', N'GC1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (113, 9, N'MH2', N'MH2')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (114, 9, N'HP2', N'HP2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (115, 9, N'ĐVHT2', N'ĐVHT2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (116, 9, N'GC2', N'GC2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (117, 9, N'MH3', N'MH3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (118, 9, N'HP3', N'HP3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (119, 9, N'ĐVHT3', N'ĐVHT3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (120, 9, N'GC3', N'GC3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (121, 9, N'MH4', N'MH4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (122, 9, N'HP4', N'HP4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (123, 9, N'ĐVHT4', N'ĐVHT4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (124, 9, N'GC4', N'GC4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (125, 9, N'Lý Do', N'Lý Do')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (126, 10, N'Họ Tên', N'Họ Tên')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (127, 10, N'Mã SV', N'Mã SV')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (128, 10, N'Ngày Sinh', N'Ngày Sinh')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (129, 10, N'Lớp', N'Lớp')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (130, 10, N'Khoa', N'Khoa')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (131, 10, N'Ngành', N'Ngành')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (132, 10, N'Hoàn Tất', N'Hoàn Tất')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (133, 10, N'HK1', N'HK1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (134, 10, N'Năm1', N'Năm1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (135, 10, N'Đạt1', N'Đạt1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (136, 10, N'HK2', N'HK2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (137, 10, N'Năm2', N'Năm2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (138, 10, N'Đạt2', N'Đạt2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (139, 10, N'HK3', N'HK3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (140, 10, N'Năm3', N'Năm3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (141, 10, N'Đạt3', N'Đạt3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (142, 10, N'HK4', N'HK4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (143, 10, N'Đạt4', N'Đạt4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (144, 10, N'Năm4', N'Năm4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (145, 10, N'HK5', N'HK5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (146, 10, N'Đạt5', N'Đạt5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (147, 10, N'Năm5', N'Năm5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (148, 10, N'HK6', N'HK6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (149, 10, N'Đạt6', N'Đạt6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (150, 10, N'Năm6', N'Năm6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (151, 10, N'MM1', N'MM1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (152, 10, N'HP1', N'HP1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (153, 10, N'DVHT1', N'DVHT1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (154, 10, N'GC1', N'GC1')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (155, 10, N'MM2', N'MM2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (156, 10, N'HP2', N'HP2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (157, 10, N'DVHT2', N'DVHT2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (158, 10, N'GC2', N'GC2')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (159, 10, N'MM3', N'MM3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (160, 10, N'HP3', N'HP3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (161, 10, N'DVHT3', N'DVHT3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (162, 10, N'GC3', N'GC3')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (163, 10, N'MM4', N'MM4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (164, 10, N'HP4', N'HP4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (165, 10, N'DVHT4', N'DVHT4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (166, 10, N'GC4', N'GC4')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (167, 10, N'MM5', N'MM5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (168, 10, N'HP5', N'HP5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (169, 10, N'DVHT5', N'DVHT5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (170, 10, N'GC5', N'GC5')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (171, 10, N'MM6', N'MM6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (172, 10, N'HP6', N'HP6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (173, 10, N'DVHT6', N'DVHT6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (174, 10, N'GC6', N'GC6')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (175, 11, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (176, 11, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (177, 11, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (178, 11, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (179, 11, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (180, 11, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (181, 11, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (182, 11, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (183, 11, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (184, 11, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (185, 11, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (186, 11, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (187, 11, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (188, 11, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (189, 11, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (190, 11, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (191, 11, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (192, 11, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (193, 12, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (194, 12, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (195, 12, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (196, 12, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (197, 12, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (198, 12, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (199, 12, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (200, 12, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (201, 12, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (202, 12, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (203, 12, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (204, 12, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (205, 12, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (206, 12, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (207, 12, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (208, 12, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (209, 12, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (210, 13, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (211, 13, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (212, 13, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (213, 13, N'[DATA4]', N'[DATA4]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (214, 13, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (215, 13, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (216, 13, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (217, 13, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (218, 13, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (219, 13, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (220, 13, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (221, 13, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (222, 13, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (223, 13, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (224, 13, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (225, 13, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (226, 13, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (227, 13, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (228, 13, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (229, 13, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (230, 13, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (231, 13, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (232, 13, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (233, 13, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (234, 13, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (235, 13, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (236, 14, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (237, 14, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (238, 14, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (239, 14, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (240, 14, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (241, 14, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (242, 14, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (243, 14, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (244, 14, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (245, 14, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (246, 14, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (247, 14, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (248, 14, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (249, 14, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (250, 14, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (251, 14, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (252, 14, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (253, 14, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (254, 14, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (255, 14, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (256, 14, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (257, 14, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (258, 14, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (259, 14, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (260, 14, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (261, 14, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (262, 14, N'[DATA27]', N'[DATA27]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (263, 14, N'[DATA28]', N'[DATA28]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (264, 14, N'[DATA29]', N'[DATA29]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (265, 14, N'[DATA30]', N'[DATA30]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (266, 14, N'[DATA31]', N'[DATA31]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (267, 14, N'[DATA32]', N'[DATA32]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (268, 14, N'[DATA33]', N'[DATA33]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (269, 14, N'[DATA34]', N'[DATA34]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (270, 14, N'[DATA35]', N'[DATA35]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (271, 14, N'[DATA36]', N'[DATA36]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (272, 14, N'[DATA37]', N'[DATA37]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (273, 14, N'[DATA38]', N'[DATA38]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (274, 14, N'[DATA39]', N'[DATA39]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (275, 14, N'[DATA40]', N'[DATA40]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (276, 14, N'[DATA41]', N'[DATA41]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (277, 15, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (278, 15, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (279, 15, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (280, 15, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (281, 15, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (282, 15, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (283, 15, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (284, 15, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (285, 15, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (286, 15, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (287, 15, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (288, 15, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (289, 15, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (290, 15, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (291, 15, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (292, 16, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (293, 16, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (294, 16, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (295, 16, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (296, 16, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (297, 16, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (298, 16, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (299, 16, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (300, 16, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (301, 16, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (302, 16, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (303, 16, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (304, 16, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (305, 16, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (306, 16, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (307, 16, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (308, 16, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (309, 16, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (310, 16, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (311, 16, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (312, 16, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (313, 16, N'[DATA22]', N'[DATA22]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (314, 16, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (315, 16, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (316, 17, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (317, 17, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (318, 17, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (319, 17, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (320, 17, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (321, 17, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (322, 17, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (323, 17, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (324, 17, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (325, 17, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (326, 17, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (327, 17, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (328, 18, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (329, 18, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (330, 18, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (331, 18, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (332, 18, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (333, 18, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (334, 18, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (335, 18, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (336, 18, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (337, 18, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (338, 18, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (339, 18, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (340, 18, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (341, 19, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (342, 19, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (343, 19, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (344, 19, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (345, 19, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (346, 19, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (347, 19, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (348, 19, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (349, 19, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (350, 19, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (351, 19, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (352, 19, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (353, 19, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (354, 19, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (355, 19, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (356, 19, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (357, 19, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (358, 19, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (359, 19, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (360, 19, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (361, 19, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (362, 19, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (363, 19, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (364, 19, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (365, 19, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (366, 19, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (367, 19, N'[DATA27]', N'[DATA27]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (368, 19, N'[DATA28]', N'[DATA28]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (369, 19, N'[DATA29]', N'[DATA29]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (370, 19, N'[DATA30]', N'[DATA30]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (371, 19, N'[DATA31]', N'[DATA31]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (372, 19, N'[DATA32]', N'[DATA32]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (373, 19, N'[DATA33]', N'[DATA33]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (374, 19, N'[DATA34]', N'[DATA34]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (375, 19, N'[DATA35]', N'[DATA35]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (376, 20, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (377, 20, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (378, 20, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (379, 20, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (380, 20, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (381, 20, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (382, 20, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (383, 20, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (384, 20, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (385, 20, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (386, 20, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (387, 20, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (388, 20, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (389, 20, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (390, 20, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (391, 20, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (392, 20, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (393, 20, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (394, 20, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (395, 20, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (396, 20, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (397, 20, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (398, 20, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (399, 20, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (400, 20, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (401, 20, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (402, 20, N'[DATA27]', N'[DATA27]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (403, 20, N'[DATA28]', N'[DATA28]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (404, 20, N'[DATA29]', N'[DATA29]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (405, 20, N'[DATA30]', N'[DATA30]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (406, 20, N'[DATA31]', N'[DATA31]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (407, 20, N'[DATA32]', N'[DATA32]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (408, 20, N'[DATA33]', N'[DATA33]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (409, 20, N'[DATA34]', N'[DATA34]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (410, 20, N'[DATA35]', N'[DATA35]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (411, 20, N'[DATA36]', N'[DATA36]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (412, 20, N'[DATA37]', N'[DATA37]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (413, 20, N'[DATA38]', N'[DATA38]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (414, 20, N'[DATA39]', N'[DATA39]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (415, 20, N'[DATA40]', N'[DATA40]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (416, 21, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (417, 21, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (418, 21, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (419, 21, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (420, 21, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (421, 21, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (422, 21, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (423, 21, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (424, 21, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (425, 21, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (426, 21, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (427, 21, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (428, 21, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (429, 21, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (430, 21, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (431, 21, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (432, 21, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (433, 21, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (434, 21, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (435, 21, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (436, 21, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (437, 21, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (438, 21, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (439, 21, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (440, 21, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (441, 22, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (442, 22, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (443, 22, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (444, 22, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (445, 22, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (446, 22, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (447, 22, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (448, 23, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (449, 23, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (450, 23, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (451, 23, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (452, 23, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (453, 23, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (454, 23, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (455, 23, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (456, 23, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (457, 23, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (458, 23, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (459, 23, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (460, 23, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (461, 23, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (462, 23, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (463, 23, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (464, 23, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (465, 23, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (466, 23, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (467, 23, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (468, 23, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (469, 23, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (470, 23, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (471, 23, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (472, 23, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (473, 23, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (474, 24, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (475, 24, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (476, 24, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (477, 24, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (478, 24, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (479, 24, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (480, 24, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (481, 24, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (482, 24, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (483, 24, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (484, 25, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (485, 25, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (486, 25, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (487, 25, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (488, 25, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (489, 25, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (490, 25, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (491, 25, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (492, 25, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (493, 25, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (494, 25, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (495, 26, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (496, 26, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (497, 26, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (498, 26, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (499, 26, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (500, 26, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (501, 26, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (502, 26, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (503, 26, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (504, 26, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (505, 26, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (506, 26, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (507, 27, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (508, 27, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (509, 27, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (510, 27, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (511, 27, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (512, 27, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (513, 27, N'[DATA7]', N'[DATA7]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (514, 27, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (515, 27, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (516, 27, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (517, 27, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (518, 27, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (519, 27, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (520, 27, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (521, 28, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (522, 28, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (523, 28, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (524, 28, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (525, 28, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (526, 28, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (527, 28, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (528, 28, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (529, 28, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (530, 28, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (531, 28, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (532, 28, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (533, 28, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (534, 28, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (535, 28, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (536, 28, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (537, 28, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (538, 28, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (539, 28, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (540, 28, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (541, 28, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (542, 28, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (543, 28, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (544, 28, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (545, 28, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (546, 28, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (547, 28, N'[DATA27]', N'[DATA27]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (548, 28, N'[DATA28]', N'[DATA28]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (549, 28, N'[DATA29]', N'[DATA29]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (550, 28, N'[DATA30]', N'[DATA30]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (551, 28, N'[DATA31]', N'[DATA31]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (552, 28, N'[DATA32]', N'[DATA32]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (553, 28, N'[DATA33]', N'[DATA33]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (554, 28, N'[DATA34]', N'[DATA34]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (555, 28, N'[DATA35]', N'[DATA35]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (556, 28, N'[DATA36]', N'[DATA36]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (557, 28, N'[DATA37]', N'[DATA37]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (558, 28, N'[DATA38]', N'[DATA38]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (559, 28, N'[DATA39]', N'[DATA39]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (560, 28, N'[DATA40]', N'[DATA40]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (561, 28, N'[DATA41]', N'[DATA41]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (562, 28, N'[DATA42]', N'[DATA42]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (563, 28, N'[DATA43]', N'[DATA43]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (564, 28, N'[DATA44]', N'[DATA44]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (565, 29, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (566, 29, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (567, 29, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (568, 29, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (569, 29, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (570, 29, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (571, 29, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (572, 29, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (573, 29, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (574, 30, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (575, 30, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (576, 30, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (577, 30, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (578, 30, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (579, 30, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (580, 30, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (581, 31, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (582, 31, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (583, 31, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (584, 31, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (585, 31, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (586, 31, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (587, 31, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (588, 31, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (589, 31, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (590, 31, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (591, 32, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (592, 32, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (593, 32, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (594, 32, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (595, 32, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (596, 32, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (597, 32, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (598, 32, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (599, 32, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (600, 32, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (601, 32, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (602, 32, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (603, 33, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (604, 33, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (605, 33, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (606, 33, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (607, 33, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (608, 33, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (609, 33, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (610, 33, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (611, 33, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (612, 33, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (613, 33, N'[DATA11]', N'[DATA11]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (614, 33, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (615, 33, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (616, 33, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (617, 33, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (618, 34, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (619, 34, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (620, 34, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (621, 34, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (622, 34, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (623, 34, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (624, 34, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (625, 34, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (626, 34, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (627, 34, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (628, 34, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (629, 34, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (630, 34, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (631, 34, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (632, 34, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (633, 34, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (634, 34, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (635, 34, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (636, 34, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (637, 34, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (638, 34, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (639, 34, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (640, 34, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (641, 34, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (642, 34, N'[DATA25]', N'[DATA25]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (643, 34, N'[DATA26]', N'[DATA26]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (644, 34, N'[DATA27]', N'[DATA27]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (645, 34, N'[DATA28]', N'[DATA28]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (646, 34, N'[DATA29]', N'[DATA29]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (647, 34, N'[DATA30]', N'[DATA30]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (648, 34, N'[DATA31]', N'[DATA31]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (649, 34, N'[DATA32]', N'[DATA32]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (650, 34, N'[DATA33]', N'[DATA33]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (651, 34, N'[DATA34]', N'[DATA34]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (652, 34, N'[DATA35]', N'[DATA35]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (653, 34, N'[DATA36]', N'[DATA36]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (654, 34, N'[DATA37]', N'[DATA37]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (655, 34, N'[DATA38]', N'[DATA38]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (656, 34, N'[DATA39]', N'[DATA39]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (657, 34, N'[DATA40]', N'[DATA40]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (658, 34, N'[DATA41]', N'[DATA41]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (659, 34, N'[DATA42]', N'[DATA42]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (660, 34, N'[DATA43]', N'[DATA43]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (661, 34, N'[DATA44]', N'[DATA44]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (662, 34, N'[DATA45]', N'[DATA45]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (663, 34, N'[DATA46]', N'[DATA46]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (664, 34, N'[DATA47]', N'[DATA47]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (665, 34, N'[DATA48]', N'[DATA48]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (666, 34, N'[DATA49]', N'[DATA49]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (667, 34, N'[DATA50]', N'[DATA50]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (668, 34, N'[DATA51]', N'[DATA51]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (669, 34, N'[DATA52]', N'[DATA52]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (670, 34, N'[DATA53]', N'[DATA53]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (671, 35, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (672, 35, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (673, 35, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (674, 35, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (675, 35, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (676, 35, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (677, 35, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (678, 35, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (679, 35, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (680, 35, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (681, 36, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (682, 36, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (683, 36, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (684, 36, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (685, 36, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (686, 36, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (687, 36, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (688, 36, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (689, 36, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (690, 36, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (691, 36, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (692, 36, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (693, 36, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (694, 36, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (695, 36, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (696, 36, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (697, 36, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (698, 36, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (699, 36, N'[DATA19]', N'[DATA19]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (700, 36, N'[DATA20]', N'[DATA20]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (701, 36, N'[DATA21]', N'[DATA21]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (702, 36, N'[DATA22]', N'[DATA22]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (703, 36, N'[DATA23]', N'[DATA23]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (704, 36, N'[DATA24]', N'[DATA24]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (705, 37, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (706, 37, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (707, 37, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (708, 37, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (709, 37, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (710, 37, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (711, 37, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (712, 37, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (713, 37, N'[DATA9]', N'[DATA9]')
GO
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (714, 37, N'[DATA10]', N'[DATA10]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (715, 37, N'[DATA11]', N'[DATA11]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (716, 37, N'[DATA12]', N'[DATA12]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (717, 37, N'[DATA13]', N'[DATA13]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (718, 37, N'[DATA14]', N'[DATA14]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (719, 37, N'[DATA15]', N'[DATA15]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (720, 37, N'[DATA16]', N'[DATA16]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (721, 37, N'[DATA17]', N'[DATA17]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (722, 37, N'[DATA18]', N'[DATA18]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (723, 38, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (724, 38, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (725, 38, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (726, 38, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (727, 38, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (728, 38, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (729, 38, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (730, 38, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (731, 38, N'[DATA9]', N'[DATA9]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (732, 39, N'[DATA1]', N'[DATA1]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (733, 39, N'[DATA2]', N'[DATA2]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (734, 39, N'[DATA3]', N'[DATA3]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (735, 39, N'[DATA4]', N'[DATA4]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (736, 39, N'[DATA5]', N'[DATA5]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (737, 39, N'[DATA6]', N'[DATA6]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (738, 39, N'[DATA7]', N'[DATA7]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (739, 39, N'[DATA8]', N'[DATA8]')
INSERT [dbo].[loaithutuc] ([id], [loai], [name], [hienthi]) VALUES (740, 39, N'[DATA9]', N'[DATA9]')
SET IDENTITY_INSERT [dbo].[loaithutuc] OFF
SET IDENTITY_INSERT [dbo].[quyen] ON 

INSERT [dbo].[quyen] ([id], [name]) VALUES (1, N'Công Tác Học Vụ')
INSERT [dbo].[quyen] ([id], [name]) VALUES (2, N'Học Sinh')
INSERT [dbo].[quyen] ([id], [name]) VALUES (3, N'Công Tác Sinh Viên')
SET IDENTITY_INSERT [dbo].[quyen] OFF
SET IDENTITY_INSERT [dbo].[quyenhs] ON 

INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (2, 1, 12, N'CAPBANSAOBANG', N'admin', N'Cấp Bản Sao Văn Bằng')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (3, NULL, 15, N'DANGKYHOCPHANTHAYTHE', NULL, N'Đăng Ký Học Phần Thay Thế')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (4, NULL, 12, N'CAPBANSAOBANG', NULL, N'Cấp Bản Sao Văn Bằng')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (29, 3, 12, N'CAPBANSAOBANG', N't15ct03@vanlanguni.vn', N'Cấp Bản Sao Văn Bằng')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (30, 3, 22, N'DONPHUCKHAO', N't15ct03@vanlanguni.vn', N'Đơn Phúc Khảo')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (31, 2, 12, N'CAPBANSAOBANG', N't162279@vanlanguni.vn', N'Cấp Bản Sao Văn Bằng')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (32, 3, 23, N'DONRUTBOTHOCPHAN', N't15ct03@vanlanguni.vn', N'Đơn Rút Bớt Học Phần')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (33, 2, 15, N'DANGKYHOCPHANTHAYTHE', N't162279@vanlanguni.vn', N'Đăng Ký Học Phần Thay Thế')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (34, 2, 26, N'DONXINCHUYENNGANH', N't162279@vanlanguni.vn', N'Đơn Xin Chuyển Ngành')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (35, 2, 28, N'DONXINTAMNGUNGHOC', N't162279@vanlanguni.vn', N'Đơn Xin Tạm Ngừng Học')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (36, 2, 31, N'GIAYXACNHANDECUONG', N't162279@vanlanguni.vn', N'Giấy Xác Nhận Đề Cương')
INSERT [dbo].[quyenhs] ([id], [id_user], [id_thutuc], [name_thutuc], [name_user], [thutuc_hienthi]) VALUES (37, 2, 33, N'HOCHAICHUONGTRINH', N't162279@vanlanguni.vn', N'Học Hai Chương Trình')
SET IDENTITY_INSERT [dbo].[quyenhs] OFF
SET IDENTITY_INSERT [dbo].[taikhoan] ON 

INSERT [dbo].[taikhoan] ([id], [username], [name], [password], [address], [email], [role], [avatar]) VALUES (1, N'admin', N'Nguyen Van A', N'admin', NULL, N'admin', 2, NULL)
INSERT [dbo].[taikhoan] ([id], [username], [name], [password], [address], [email], [role], [avatar]) VALUES (2, NULL, N'ddddddddđdddddddddđ', NULL, N'dddddddddd', N't162279@vanlanguni.vn', 2, 0x89504E470D0A1A0A0000000D49484452000000BC0000004F0806000000514FAF1C000000017352474200AECE1CE90000000467414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8640000385349444154785EED9D057814D7D78777378100350A1428C10224210984102078716829EE1A084E204409C1DD82BB6B70095EDCAD4829C5DDDD4A692915DADF77CEDD99CDDDD9D94D82957EFFCEF3BCCF8EED46F6BD67CE95B9634899A536527DEE98941A5CB2D4B121A5AB352E59EB5A93AD1E5CB23B205B7DA28185945913B653B8D68329532D18B47C5E2771E87773886B5D6BE8E75991BDA17D94738CB4AE62399683C8D9C88CBCEED618467A35E6708C2167133AB7298C4CAE663A04C290DB31C65C2D60CADDD28C7B108CB95BC09847C29DA1631E410AADE9BC048C1E6D60F2D490B7AD15466F3ACFA70DD10EA67CEDC5ABD19BA175C224E800938F1923BFE6EF2830FA120582CDAF32BC8F30F97532533084E892086130F9879B2914213016B6C560485F0DC64F1D63485FDD0AA3960C3560CC58D33E9FE9ECB3A2168C24B031536D2B0CB4DF94B90E515760C85CCF21C6CFEBC394A581C0C8B8125949B66CC9203B892661CC41C26930E524E174F60B723615A21A484A43AEE6D6E46648C43C8C463E2D2C691E929070726F0567923101128F60210D0A4692D1E849024A383179DBC19970F26A2F30314242DAE743FB4840E77C1D158213F0E944AF846F67BBA4209C4848A782F44A523AFB771138150C55088333918224749629140E2792CF99291209A722111A685F40249C03A204298A46C3594B312D314851AC3B9C8B13257A587092702ED993844F47C27E6A8D295D0D2B0C829A12145DD36BF88C0415D07A061D2CC7ED9091A26D461257C198B1AE994CB4CE64AC4F525344CD4C522A183FD742D151250BE14A646D0C8380A2A605759F0ED9485699EC24AE843147731B9CDC5A5830111C590DB95A92B41C55655A91C06678DD9A84738C74DC90872227E34E72331E242FE34E91955E8D9E14456DA0A82A93D78CC98B222B61F2A1C8AA6064487213C96DCA1F0CA7FC24AE6F880249EB1B0A1361A475630142793515A0FD2A7E6130FA7581813092E08C81F6990917180B52649530F8D36BA148228A22AE194391AE30EA11100D5340371849E6C4E92E3015EF01236122B98D4C298592BD88DE2C3C45D84F1330D1B689849631A62319258CE94944990CC46724A5234858C75064CED4C82E4E9949C6CC246B267A553066A6086C05455801AD7F4E64A1755712352B896B03EF6728EACA64A774C01139283AE720A97332148509036D5BC1FB18B72033F27A2E929930E626991D60C84D915B4072E721C9DD296550301206F7F614D93B58E3A9435EA6230C5E04456D2B487633BC1E02437E92377F2841C2AA1420711DE147E73314C90D7E24B4E518AF137E91091454F027C90B754DA048B47D487843D118A2BB3524B785A23D12284651BC38C172979228D9D78C211DA50F691D63F894B027BB8281A4677465270C89C2D2539456D0933E4168C7983E6F46690D456305034BAFA2155CBB3F1B09ABC1989DA188CDE460C1E9352745633B58E4D6486E83109B04CFC3629BD713682B30E621C10913092E637027913D4862194F9257262FE1457893D08C4F676BF211F959748222B88122BAC197C465F293B4BEAABC0E10C2F3AB822AB41EAAE0854964464F6E3D84E024338B6C973E30949028A9508A24674AF733A38DF07ABC2BE10D9F51CEED48F8CF29AA270153168EEE09185C25B4915EEC97B6B3DB4675213C4775213A476F8EE2841B476E3DDE80F014D505229A131E1CD11D21457539BA736457F121F9194B642728954988EEAF11E119DE16519CE4D622096F5485D78AAF273B5394223CA52D06CACFF5E1C2D02B81120A94C6A811DE58CA22BC6D0EAFC5C0D273DECEE94D7A8AFA3A18323054203ED3C79CA33B40913E21BDD1C239BABEE02A9CF258B69325BCB45F93B35BE7ED247F4E4E6BE84A9093A175373D340520174BAF81727583C8D529BD7167C113B6CDF03E2A0894BF0B286FB78624A73CDD0A91BE4828D21B9594C6E84D8233AAF894C71B288FE794C698BFB342084139B9457E6B8C22AF3723AE082287A70223A0ED82AAF4091895426024E90585597A330686F27501E5ECFA28D2334AAEAE857377357F375005555092A0FC9D3152B43796A2889F24E1B9D29A9EA4278C0C5542B5A89553235540F530644C0C169FAE104A25D594B9BE0D5695521DE4CAAC81F7898A2B1504AB4A29C92DA3DDEF50788245CF41FB19213D61233C477F09A5126B2137918721F1DD1578DD0A457A2E0C8CD42223F024E9A9A26A055550ADE08A2BC3D273A5D59BA54FC0E0430542084FDB54713552B44F905E155F5D3763A2D447C548155C4301169D52237EE5CA2DE5F246925E5B59654CFE9166A8A2AAA2565C8D4518A5B2AAC1227E5116DF2CBFB6D2EA44290FA3955E545C0953A95E04457B21F3A72A6A2B8C063E266D1BD393D83292F0E6486F47788EF4024A7F04E675738B0C89AE22A427C159720583A081C0B6752641722B487843169259262B4BEE004B0B0D89AEC2D20B587686A496C9494213E68AAC0E14E565CCD293D48C25A24BB273F4B7A43666B8B5C63AD24BE2E765D96D11A24B9828CAAB1845C42728D20BD4CA2BA73802CEEFA51C5F41B4D6E8C0B227E4F25C595520D119B3F414ED49786EA131A98848AF8ACFEB5D6122C9658C14E11320C159780D26AAAC3246154A691835BDE1161AD14A239A1D459EAEA0A42E56C8C70923EDD34F6912B049696C72F604388D3129C82D37AAE082CF13D6F564670C9CD624869CCE38C022BB0683909E04D781F37B3D4C94C7CB18455A4352137A2D3432AAF822CF4FB6F024B484892AB0329CE2A8985B6A64E155E939B549404E69CC5034572BB7B2E832527A23521C7F969D645690531CB3F0D15658447784243BB7D4184B90E044423ECF2D35FF096F173DD9993723BC59F6D7139E1069CDBF537859FA44852FF29FF0BAE80AAE45476E3DF46467DE1BE155D965DE81F05A5E4578597A4112843768484C784B8B8D2ABA8AF11584E77D56B2BF01E1F530501EFF4685E7161B1DB9F5D0939D7923C213EF44784F730B4D9284171D510A6F52781DD955DEA5F022DA0BE1B915462B7462680B040F2DD008AF4A6FE20A6922C233BAEDF53CCC80A56724E1ED89AF2B396169A2949B27B5CD9176D0153E3B5554191DE9193DE9651CB5D15B4457D62DB2F3BA68B591C44F447873D3A5B97952165F95DF5A7805B97D9E85E7367A1947C2AB2441764615DE40955715ADFC8CB99DDE567AC6AEF02A4A4A634971DEB6F04989F0CCDB149ED16D93FF1F13DE46FEFF844F2249103E65E67A489BBB193ECDD31CA95C49484E51B4424BBC0DE18D591A230549FA917B2B7C9AB70DD2F0802E16F91D0BEF9CBB9540DDFEA785B748AF27BC2AFDFB24BCB66756F4CE264F7813C9EEF4B68477716D801A4D0763D6D4D59831391E1DC22721A04A0C7215EE84CFF2B6820B77F468E4B6919D9185D7C8AE2F7C239848E2B49EAD91BD7067F8558C418336A3113B6229E6CFDE800E115391B1007DF1AF237CB6E409EF44E216ACDA07159BC62295479B7F56786D8A634F78213D09CF636D5E47F844A44FAAF03643119223BC92CBB3EC16E1E571336274A466DB4670DA6F530024E10B57EC8A233B8FE2C198C9B83B78249EAEDB841FBF3D8ADD2BB662DCE8A568113C0E25BFEE81BCC53B2383670BA4F89CA33F7744590B6F5389D54A4F05C148AF1FE50E44CE229D50A84A77540F1C81BE831761EDC24DB8BAF3209E6DDF8D0713A7E3DEE051B8B7E7109A75990C27EE3C5285E7CEA864C92F09AF601E6793000F2E732FD30D5E157AE003AFF668DF631E4E9EBF8562B50692F04148453206D41A805C65E88BB323BE0C8B9FDABB0352E66D2FE4170540959E508707CBEB3C2C5895DB4A72098317494D18BD3BDBE21322B0115E411D7E90BA5057A46629497A93182199207E0A123875D16E70629159701D84FC1AE96584F085A8E24A92CBC8C29B871DF4B0925E8B89A467CC113E6D0D812CFAEB08DFACC3189C9CB31CE703CAE394AB97E07CB14AB8DE3A04F7474DC493D51B7069DD76AC99BD1643862D44732A00656AF4867BD1107CC8375728E98F3DE15DB2374556FF8E28F26577D46A3902D1FDE763FEF4D538B2620B9E6DDB8D47B3E2702BB2272E7D5917673C0A997F876C3E7834631EBA0F5D820F3D482449F84FBDDBA271F00434EE34D142838E13908FAE109E65A250A7ED58F3FECE93687D1CBCCAD33F9844FFC4A73D1A044F24910708D15D2802FB5334AFDC6C04E2561DC0C8E91B29BAF7459DF613B07EFB0F981FBF1F651A0C45798AF6DF9FBE8E5EA3E251B6F17054A0F33314A288497297A2E3F53A4DC6C7BE24236DE728DD150DBB4C4548FF4568D3632E8AD51D2CC49723BD3315AA8A2D47A371F80C41DD90A9F0FAAA8F95EC6E157AA271E42CF8D51A2C6EEE5085FFB2DD24348E9A03E77C24B0227B9A82E128D164144A341D6D11DCB3DA40D40E9D81CC657A88EDB4C5A35133643A4286AC44F0A0E5A8D066323E08200949741792D4AFC14804F5598C90E1F168D82D0E39AA0ED2153F29C29B9228BC23E97585D793FE55846FD1793C4ECD5884F385CB5984B790D55B4878A94A1DDC0C89C683F1537167E96A1C5EB40E8B66AC45DFA18BD1A4C35894AAD60BD9FC3A50F4E7B1F00DE84A40A941A56EA8153402117DE662EA94D5D83E7F1DAEAFDC8087B317E04EAF41B85ABF25CE152C6DFB33151E4D9B839EC397E2238DF0B98A8761D5C623D875F00CFE7CF9172E5EBD8765EBBE45432A0461FDE270FFD14FD87FF402B6EC3989EBB71E61259D9BCABD35BC2B74C76FBFFF81B9CBF70AE1D31708C698997465B9F95044F4B631733074F2065CBEFE00DFEC3C811D07CEE2E4B99B58BBED7B2A00C7519FC4DEBCE7142E5CB987BAC19344245FB9E93BFCFCFC37E4ADD4132E742598B56C2F1E3DF9193BBF3D87635448F61CBE80724D4758A537690A74C2DEA31771F4D4356CA4DFF1D4C5DB58B6F1283CBFEC2B6477F20A46AB1EF3C1CBF24DC7C47E55F8E3E76E89FDA979FC8B227C96323D11B7F63016AD3F6A113E6CD84ADC7FFC332AB69928B6BB8F5D8B07B4BD7ED7296C3D781E67E96F08ECB910264A63029A8EC116DA77EEEA7DF17AE9E623CC88FF16E9CA907074FCAD081FD0DD22BD23F1CDC27F625FFA372EBC168ABCE7FCCBE04A9DE6B81DD90B0F264CC785B9CBB063EE1ACC99BB098346AF400F4A51264D5F8FF5F337E0C4FC78DC99360F77FB0EC5F5C08EB858A62A4EBBF9EA7FB6067BC2A7740B845BF150D46C3D1ACF7FFD1D3317EF428E62A1484B919F85BF75F7095A864F836FA5EE5844919B25CF562CCCAEF017A8C0F419BD0AB5DB4DC0E90BB73175C14EE4ABDC0B45AAF7C7951B0F70E3CE6394A648EE5DB927097F122FA9902D5AFB2DDCCB77B712DE9BDEC3C7A6D1EF5398D2A1C0A89938F8FD65840F5E62165E913E8D6FB0103E62E832E4AFD60FCDBACEC21F7FBEC494C5BB85F09F97EA8689F43B3CFBE5375CA502DB246AF66B099FBE540C6EDF7F8A5574D5F2F87A108A35198D3DDF5DC68E431791EE8B9E088D5D85CB2479E4C835F0AD37020B367C873B0F7F42AEAF07BF5DE125E9F564676C72783D6C04D7A22BFCE2A409AFE174EE82B850FA2B5C6D1884DB51BDF160DC54DC1A3F1D77FB0CC1F5A04EB854B936CEE62B4605C55BF7FD8EB0125ECEDD096EBD09A8D61BBF906CA3676CB4E4ED2CFC9D7B3FA2F7C815A8DD660C45EA1F70EDD643A4CDDFC122FC1C129E539C74BE1DE9BD9BB066CB31F890B02D49502E2C25EAD0E5DC8D7271CADF27CCDD867397EF8ABC9D85DFB8EB044ED1D560CFE1F3684729CB06FA7C213CC95EB5F558BCFCEB2F946D120B67CACB73958D415DBA2A14ACD19F64374777BEDDAF12A533E729C24E5DB21B4D22672205E5FB7FFF0D9CA4C2C6C27FD17414AED055671B5D61B61F3C87A8D89570E1161849F8547CBB9E46F803DF5F41B398F982B87587F1D3CF2F84F0BE7586D155EF190AD61B0E63BE30A42ED2155124F791533744741F34632BD6EC3C09AFDAC3448A5384F635E9BE001F972219F58417D2133AC233467F4D1EAFC86E81532929BAEB096F95D2E8492EA32BB90C8FA2E4961A85169DC6BDB2F05ACE7A0590E0C5713A477EDDE3C9E155857F405FEE714A270E5174E5E3AB3651E423C1ED091F4FC7BDE85807AAACDEA148E85D91BE00E56EA8AE43968A2B802A3CA73A1CC147CFDA8C9D07CFE2FAED4716E16B52EECFC207D41E04CF8A3DB166DB71C46F3E868E7DE22CC23B5141684D05E5D6BD27D8B0EB24064FD9206ED666E14F91F0CE3EC16810360367A890358F9E83B1F37660D3DED3C85F7D60A2C273145FBFFB94E0F4A5BBF8FD8F3F85F0851B8E10D13E4D619296847721295B51BEFE3D7D56E50E53316AFE2E2CFAE63BE4FC8A7E865281B5F08E84D7F2AF11FE4DF2AAC2DF2699A2062E44B90683B178CD4191A3672A146295D2B0F019FC3A61DCECCD16E19B864EC38DDB8F51892AA5CEB95A0BE117AE3E6815E159F8E153BF41A1EAFDB065EF2921B82A7CE516A3C5F697416390A17028BAC5AEA002F1185316EEB208CF3317E4A4C87FE4E455440D5F0E777A5F4A9F8408EF5A3A060BD61EC2D367BFE2F0896B14E91F89485DA3E3E44485DFB4EF0C0A358C158C9ABB034F7E7A2E84F7AE3918F728087CD97EB210FE838068F49FB209DFD2E7FBD4198EDE933662F38173A2E2CA92D70A9B852133B7E2B3B2BDC5F65B115E4D6934A9CDDB119E91C6CBFF7F13FE26E5DC5F358F85315B53546A328C84F9154DBA4C41B6A26142CE43C72FA356DBB1E8D277014E9CBD29A27C3AAA48E62C1985635491DC77E402CA361C869A6DC6E1479266FB813356C20FA3A89C8252939EA3E2F1F8C75F2CC2BB96881495E87D949F97AC3F14ADBACDC13DCA8565E1E51CBEEFF8B5284B856BDCBC6D5410FF44CCC855285C77A8F899DF1EBF823EE3D761E4ACAD22F2870F5D8E0FFCC31D0ABF68FD11DD1CDE85DE7793D2BC0BD71EE08B161350276CB6D8DE4292A72E1A8DE0C12BF023153096FCEBCE33B0FDF0453CA4BF4B8DF8EF81F08EB1959B22B903DE6BE18791F0EE24BC4DAF6C1314FD5A117E3A092F6EEC6E46C22F50841F41DBCD91AF627721505CFC7EB8E46E8DE614C5394F7FFCE3CF78425FEA4E92B94CFD21E23E5853CE1668D479B268F5E1969E4774CE0E3A9EA72C7D411AE1B90932639130D16A2384A73A8013C9CC4D940F1E3F1339F3437AE5969A9A541956DBE3990FF293F0472E8A82C8E7DE79F0140326AE43DA4261A849919C2BAA81D173F141C150A40B88C4DCF883384E05D3AFC6607A552AAD0548F8BC9D0459BEE881B83587B09085576EF8D6B6D2940D1A8F9B777FC483273F8BD69A833F5C43F16663457B7CF64AFD3169C93EF13B3FA2FFC9C327BF88E64BBE12F07127BF480B26597A19AB02D0D582457A457C93457C925CC2184069A484457E3DC1B5FC2F08CF3851444F933B0829B9375499C9C099D653E7690D27EE50A26D13A52EBC9D8A870A640B440A12375DBE8EF02E17839CC522F001456A3E978F719AE3E4D6121FE66D0B8F72DD90A74CB4B9B7956F00A1F799487217CFB614DD2952D33AC34D91DCC964E29E55DA66E9B94DDE8B227E2E2A287CCCD99D673348109E49EDD35188CFA4C9172CDAE6B967D5C9AB23525305B540CD41E83E7A354A361E8194F93A23B56F084C798391CA37146924D919935767AAD48622251D538577A62B40EAC21162BE1A16DE243A9D22442AD36DCC3AD1F9E45480F273D101158E94FE5170ADD80F5E3587227DE99E484112F331B5834A26F188AF2F3CCBAEF29FF03A2426BC185C96855219EDD41D7670CAD1D22237C3EB8C8967395084B7C00586CFD50E2D505184D7431D6E608546782D22DDE1A1040A8D22668A2B47F488784B67943CB5872CBC0AE7F8AAF06A6AA385F37A4E6D9CA832CAB31CA8523BE23FE1DF116F4C783AF62145E6061D2752AA335244FC8C94B357A1FCB966EBB1702B415FE2EB0AAFD9FFBAC25B24977903C21BF2752142CD2451781BDE7BE113C12CBC75C7D3198FC256F2BD6D4E67CF277A744FE74CE8944AB6F00EA4CF1610269AE91E506EDEB1C75C741FB64C745A71BE5EB7DDF8371AE17521E955EC0A6F6F4E1B8BF409C2CBBCB2F08AF4C9165F2B3C5F2DF8E66F965E889F20BC8C2CBFA1708C359A026011DE9096274CD5175D454F6A47F0E030ADF0D75B75C6A52A7571C6DDDFB2EF4D73266F113166E772D5FAB852AB29AED46C82B3BE252DC79324BC2CBB03E15D280F5FB6FE90E89462D1395DB84695C36193D6E173FF102BE1CD03CACCB2BF31E11907D23B14DE22BDADEC8C95F0F28C65BAC24BB2BFAAF4890A6F1DE575D10AAFF24F097FAD45473C3F7C4C8C79B958BE064EBB15B01C4B0A67F2F8E3C2175571CEFF0BCB5082D3B9FDC400B5CBD51BE17A9B10DC1B3E0E4F96C6E3C9A215B8D36F28CE6952AA37293C93D6BBBD88EE63666C42EC940D080C9B26223FCBEE30C2EB49AF277462BC2BE11D4AAF48AEE0E41B86548522A9929BF49C5E08AEE5DF2E3C4BFAE3F2D5F8EBF9733CDBBA13B7227AE07CD10A6230992CA50DD97C70BE4879DCEED60F3F6DD88CC7F316E3DEE091B8D37320EE0D1B83274B56E2F9F73FE0E58F4FF1E2E4193C9838435C49F43ECB9EF01FE609426097C9681E3219151A0E850BCF1DA3119E2BA3256B0F44D3902942ECCC14C55D48BA86C113D182B69BD07EFFAF7A5B64578577C9D30AB5DA8E4360F834D46C33169FF0C4471AE1B97992C7C93025EA0D411A9E204923F727544750CFE11195C9113E63F128D4EB320D81D173D02C6A36DC2BF726B16D850FEC360FCDA3E7A25AFB29F8AC38A5092CBC247D813A43D1A8EB1C04F68843918623C4644C2CB991C4CE54A617AA779E814E8397A3FBD875881CB11A4D63E2E0DF60A4A539D21EFF72E1A9D2AA233C8B7DA9522DFC72F008FE7EF9127FDCBB8F1F291A5F0BEC205211594C0B24FB8D0EE1783C7F319DFF40B417EB2DBFDFB889C77317E15AD3B638E56008823DE1F96E281E05C99D3E7B0F9D43993A0329DA532EAFE6F3599BC1B57017ACDEFC9D18B978F7FE538AE4A1C851341C771F3C15EDF2DC213464C25A1BE13FCDDF11474F5C15E75CBE7E1FBD47AEA448CF513E41F84E7D1688E3CCC4F9DB91A5187DD91AE13D2BF6B09C3365E14E2BE1ED55625976A3473B946B3E0A37EF3E11EFE576F1B6BDE2E0ECC5F9BB750EFFE34FBF8A5697F357EF232A369EA23CEDF762CCC2478F5A8DABB71F9B3B95A66F464A6ECECC178682F54760FAF203B878FD21D56B5E2ADF0A4467138FA46CDB7F29D296A4CAA38EEC8C65D8818A457A929D1183CB74249730EAC9CE14614878B5E2CAC2ABE8C96E46BF079611378C70EB0C4FB8AA4CBADAA2D30412DEB69586C7C3706EFD386E09FEB8730F7F53A5EFD7E327712BBC3B2E94A88CD3B9FCACCE67E1EFF61F869FF71CC0CB273F2AFFC684E5E54F3FE3D7EF8E532A33568CB84CEC6AC1C2F722E13F26E1E539299D5C9B60D6A29DE2339F3E7B8EC1635789FD5C18CC694E1334EC305E140A5E78882F47FD861D278A6D5EFEFEFB6F5120B217A5FC95445749E71B2C86FFAA0B178CF28D8659E5F49183162B4781D9CBF6206B31FAD2D52B80223C8F9C549779F1FB6D5B6D0813492EC3B30EA7F20E464B8AECF23237FE00DCCAF504CF6A905081EDA01C352F3CF6A6722BAA7C2BE90DB7CDF79DB04114085EC6CEDB8954247CDAA231D8B4EFACF8FBB957F8D28D8788DFFA03B67F7B41142E5EAEDE7A8CF2AD27D94D73AC223B21E7F402125EF4B63A406EB161B42D3966E9897729BC805394621571B7DF303C3F7A5CA422CF76EC11A909A728DA348773F6CB5F371063E77FA6F3C442FFDCDF2E5DC1CFFB0EE2E1E499B858E66BF1B9563F47077BC233951A0C161FCD5FDA92D50790926FE7538477CED11C3316EEC08BDFFE10E7F41CBE5C7440ED3D7C5E6CABCB55FAB29B874E75283C7F3E170C177EA4CC3B103E6799EE58BFE384F24EF3C283C2AAB41AE750781E9A306BC5017CC40F30B023BC0BE5EB83A76DC15F7FFD2D387CF23A2AB79D8CBCD506A360BD111843E7A8D2AFDA7E02E97962D3B724BCDC6223F3CF0BCF909C57EA36C7B3CDDB453EFFCBA1A3B44E429D39875F48E22BB59B590B4C05E072B546F8EDE265B3EC17AFE0F99163E2D641FE1CB9E9D1118E8477F50B1683C47839FCFD25047C455F8E227CC6021DB075CF49718C871EB8978E4226BF4E62CC390BCC5DFE2C083370EC6A31A4C05AF8BBE2BDEAC26951977E0BDEBAF0FCF084A275878A56A4977FFD25068DFDA2AC378B9C45690DA732FAC2F3C2431482FB2FD1157ECCDC1D247C28EED2158B171E4B53ABCB4C4A7142A9726BC6ADCA006CD97F4E1C7FFEE20FE4A0EDFF4DE189737EA5F070EA6CFCF9F0117EDE7F08F763C70BD9FF7AF1022F4E9DC5D90209CD89677D8AE1119DFB377D519CAFB3ECBF9E384DF97D04A541496FED71247C1AB716183E71ADF872B86D3DA26F9C45F80A0D07E3C4D91BE2D8B7C72EE113AF766819315D6CFFF8F43926CDDB864DBB4E88118AEBB67E0FEF0A9437EA08CF232B4F9EBF29A221DF41E5568ABEC8B7283CA733AD62E689F7B0BCD316EFC6A63DA7C4F6EAADC791A7925A79B5169E0BC6D153D7459AB26ADB0FC853B9AFAEF029F3878A42CFCB890B77F031CFECABC8CE9828BF77ABD25F447BCEF379B8C1DB12DE36A74F10DE3CD84C477895E40B6F165D2529C273D4BE5A3750A4357FFFF6BB10F9C99278FC79FF21C8083C9C36D77C1E45FAABF55B50BEFE0C2F9F51CE7EEC075120EE0E1E95EC367D47C23B676D8AAF9A0C135F1E77288DE311938AF04327AE11919D97A088694893A7B51850C6CBB9CB779093040DEDBB406CDF23B15A844FB3CC6E20A734DC56FF457D73EAF4CBAFBF61C2DCAD22977728BC825678B52030AAF0F23EC6B544572A60B7C57BBED975129E24F8C8995BC4F6B35F5EA074E358AB664A75E18165F5BA980B34A7715D63A94E4351BEEFF8F536C2F3158E171E75E94272CAC20B38E293E47A5884B7576955E1CF551EACA0273BA327BB157A95D6772A3CC1A9C88DE048FC76F9AA48557EBF761D3FEFDA87BF7E7D21E05CFFAC57517163361F7F71FEA29885E0D9969DA22D5FEF331DE14878264FF1305CBB49058E96F55B8F2157B150D182B374CD41B18F5B71DC4B45C2AB6C34289853B9FC0B3BF69F16F93CF7AEF2F227453CEE754DC14D9BB4DF4A7892EC83BC6D3187A4E6E5ECC53BA81634FAAD09EF55A58F88D27C4599B362BF78544EAFD1AB4581E6FD753B4F31B7D628D2AB0B47F7CC25A3313ECE5C91E711951583C6275978BFBAB1881EB5163DC6AE3733CE4C06BEE3492A004916DEF28C28FBD2FF2B8457B950AA8AE82C7AF9F427D1D6CE690E2F3FEFDA8BEB41C162FDCF478FF10BA53EBF91F47CEB9FDEE7244662C27FE2D10AFD47AD103F8F5B645A864E41FEF2DDB07D9F390DE0A1BFAE853AA3DFE878B1FD82F2D25EB12BC460B1825FF6C29E43E67C75C7FE3328FC751F5DE1B9D7357FA51EE2868C975460F86EA99E23CC3F939737253C8FAF0F1DB8449CCFCD91FDC7AF831349CD39FDEE43E6CA36DFF49DAB3CD55594CE297561E139E2D7E838058F9F9A2B9D7D48D8E133B62449F8A05E8BC4FDB32FA9A071C15217AF1A43CC39FEAB0AEF40FAC484FFE7531A0D972AD612F7AF726F2C4FB3F1D78BDFC0EDF50F27CF12A9CC8B33E74505975B73CEE62FA1FB19899198F09CD6546B1E2BA2F4734A397AC72E43589FF9A252CA51B245D81464C8DF41DCC5C40B0BDCB6EB4C31882C286206566C382CF63F21491A759AA42B3CEF4BE3D906A15469E59F73EFE15331AB81BABC29E1B9A34A95953F3F6AE87234A434A559C42C6C3F7056ECE7A550CD41960E2A7551854F5320143D46AF115273A1F9FECC4D515079D1179E6424E13396EE29C6BF2FD9780CD794FF152F5ED549781F9EC02999C21790A47F1DE119E3272CF9D712AAF8FAF3D4307AB31BC89D4FBA1D4F8970C6B3B0E8897DB67517EEF61B2A5A6C441A73EE021ECD5948698CB9158723BEDEFB93020B2F3A9EF8291BFC084C09557AEF3251A2958603D3B2B5DF62C5FA43E2CBBA73FF4714AAD213A56AF6C74FCA976E6FE1C21143690DA72F7AC2335EE5BA63FF910B2202CA5170CEB2BD243C7DC1C9105E0BCBCF1D5549593AF45D08179E654C13E1C5CCC39E1D51A6F9181C39794DECE7A8AD565285F0F9BA8896295EF89ED7AC657AC3C927CC8AB8B5472C7F9F59F850D159A562E057457C463B7498311420D17530FA91F884280492F05AF145C5B5500C4129CDFB223CC35375FC4682BF7CFA148FE396E2AF9F7F11B93CF7C83EFFF6A89848C9A6732A192445F84F3C5A8B7378B94B92734F2A2F2B361CA29C3E0C53E3B68B1C9817BEB1FBE80F572C7C4F2902575A79E159080A7ED55B5FF86C2D28C76F853A6DC78B7A81BC08E18BD2972CF5C4BE8AF0B1D3368A737FA54A27CF917394A455F98E84E688CD0BCF6690A93809624778EE810D19B8CC92CAA88B2AFC77A7CDAD577C7CC0A48DF8B4680C4CDEA1828F49B6E59BBF7728BC2011E18D3AB2275FF81EEF9FF05C89BD3774B4E8857D71E1125556778928FFD7F35F45EB0C8F82D47B5F52498AF09CD6D46F3BD6D22AC3CBEF14C5024326C3D5BF13CE5FBE23F671EB4540D53EF02EDBCD02E7EDD3179A2B7ADC0A52A3F5187163B79EF04C768AE4B397EEB18DF08908CF85A971E8342B1A854E4599C6C39192EFA0F26C875F5FFC2ECE3D4B91B76483E1F0F9B2AFC0FBABBEF0AD36005BF69D16C779F1A4CAAD23E17357EA8BF82DC7C5954B5D54E12BB79E44A993B930DCB8F3043DC6ACC357EDA61053113E345E447E5EF8BDDC21C529CF7FC24B706A73BBE740FCB2F7209E2C5B85DF6FDC12BDB177078DD03D3F392445786E8AF4ADD00DDCFAA22E97AEDD43E95AFD502B689425829F3A7713DA871E3BBBB544973E7142769678C0B8D5C8F34557BBC29B72B444E5A623C4500575498AF0F6168EE43C659F7F8DFE629B25DB461568F901C7E6F135ED3166CE36D109C54B9F716BC5AD80EAA2159E9B249B46CDC1E51BE6162C5E54E19D49D8BE13BEC1ADFBE6FFCB0B0A0E7CAF2BA3F64C738158B6E97B64FCA2D7FB2DBC9ED8426EE99805DECF502556EF069064C13DAB5FD6358F881C3E568CA53957B8ACFEB9C9C091F01648784E6B3A44CFC44E929E193A7E0D7214094178DFF9D8B4F3076CD97D122D28A25A84970699F956EE8149F3B789396626CEDB8A7C957A881BBE799B27719285673EF6EE80A6215311BFF1A838A7DB9065C858902A761AE1798E499E6ECF112C773AFF100453A1DB79F01C36EC3C811651B329CD692350856702EA0D15636A76D0FB967D731429BC83B1F3D079C1B4257B44C4373FECD83CB0ECC382E168DB7B21D6EE3841E75C40A7014B9182275CF5EA820F48B8767D1663E1BA2394EF5F1705E3DC95FB3878FC2A9652C5352276157254EA2F3AA2587819165EADC4AAD808EF4B0140464F78EED46274C467DE6FE1157848F1F922E570A5414BDDE3C925A9C29B5C9B228B5F3002BEEA25E0A9B753646F06AF2FA268BB37FC2BF784936B735DE17918718E121108A8DE0F7E5FF6024FB8EA4BE7F37621A5A952169EF9C0A31D3CCBC6D039FD91AB6457B8F013B835C2A7F66E8F805A031D5284E0E6485F4AB578BB40D5FE70A148AD0A2F1E67AF08CF11DCA3526F14A93344C86FA26D7E657CBEEE6F233C93DA37948ED1CFAA371C39CAF512BDAF2C3C938244CD59A11FCAB598805A9D66E0EBF65351BAE958E4AE32C0B643CA9B0AF4EB08AF489F3CE1BB275D782D36054086846FD466244EC4AD121D477AE2259B4446412685D339F38BE6CEA8FE0B90C68DA4D3939D51D21AFB34214870424F78ABF9E3EDA1115EC598BDA5991C24B946781B742AAB36B891E48C2CBCFC387B1521B71ED6C2CB709A235084B78B24B785448497531CC646F67F5A782BE949F86295A37174EBB7B858A1A6AE7CFF043CEAF2D682E568DE79124C3C09939EEC8CAEE432FF0F84D74AAF2B3BF37AC2734B8D91B0929DD10ACF9D51126F5578F3CCC1AAE4BCAEA22F3BE35078228D6B032C5ABA137726CEA44AA89D9B3BDE213C0EFF56580C76C6EF40514A510C99486C0523499EA54007F41FB9C296512B13213E81D112635625C2EAC419CBAC3133EE5559AB619DE871B56282CA7A3B6C303351CB370A1BD17F92046F5BF66D42FFC9097CC6C38365F1B5C2AB115E13E565D139BD11B0F056D293D8DC01A552283A017FAABCFA93F0FE2CFCC724A98ED47AE889AE624A6BBD5DA07418BE3B741A0FA6CEC659EF005D11DF053CC0EC66680CAEEE3B8AD65D26C185F27363C686164C247DCA2C4D90216F1B41FABCADCD78D13A91C1BBAD0DE91532F8B4D3257DBE0E66F2771464F00DB622BD6FA7A451A0B320835F880DE90B1254417548C1500B19FCC390A150B83585351489D01089F445A29021A0AB2D45A3156290A15802E98B76437A5E2FDE1DE94B303D90A1A41967169505D748AF4D67E46D759F2ABA892467ACD685F024B684D1BF9B3505BB93F8A2A7B596AEDC7AC8426BD10ACF4386BD8A76C2379B0EE3D76B377033385257C8B746361F5CAC58133F6DDA865327AFA051DBD1247B632BD955E1650C991A9A51521CB5B952464D6F7824A51E22BD11298EF97E58F9B63F71EB9F4E3A634376826F0524D49987650C6E3A298C1635A5218CB9DAC294BB9D156A53A50A0F2E93311206F78E02A307A53184BC6EF2EC4484506A93804181531A91BAE8E5F05A34115ECBBF427835BD714A5F1B0D5B8FC0B5EBF7F174F7015CAADAC03CF5F51BA888EA92DD474CCD717FC274DCBD7607A326AF85AB2F7D799647E7D817DE9924360958F457139E5B789C73B4400ABE858FE55684E71BBE2D3392E909AEC194931F68DC0A69F307E3E0B14B285A6BA0D8D60AEFECDE06293D5960EBB134293CDBC2999B21ED08EF4C32F36032597817EF60A4F4EA682D7C1ECAE315E96512A47720BC2ABD9EE432AAF076A4FF57092F485713697336C180114B71E3FC35DC9E380B17CB56130F3FD095F615E089977850D98D9068DCFEEE14D66FF90E25AAF6542497D117DE256B5394AF3300758246A174CDBE62A830B7E6244778963D7B9110C40C598AC1E3D720578948217C26BFCE1831F51B71D3F74779492E1DC165B8438A9F0555AFE324741BBA0CADA367E3EBA031C85C98725E4978539ED6621683F9F107E05599F26355768FB6183A65031A864CB3C8AE0AEF94A73DB2958CC6170D63F165D038A42B444291ECCE54411D366D1302BBCEB6155E23BD33575615E99D48EE7401DD60F252A4572AAC427CDA6789F23236D2D3BED715DEEF2D08AF228BADE250789E379E5E9D32D4867FD908C42DDE814B7B8EE07AF701385F9C6FDE4EDE1C3556D095E21C45747EBED3CDB55B70F8D845346E3F96A2350B6D7E389AF59301AD8557F9306720464C5A871367AEA3EB8005D8B0ED7B34ED34111FE46A811C8543E051221CB98B852265F6E6F82C5F7BB8D3B647A908B8D0FBCCB237A1BCBE1D5A474C134FF43870F4A29871D8B34C349A749E2C7A60F906EFBE54C14DE3DE16794A7745AE9251F894A7EDD008EF4C42F318791E6753ACE600F4A6F7700F6CC6425DE04C913C53A1507856E88ECF03C210183943F4D436099D261E99C3B2576C3E12B13C03B214DD1943CED6F8C43704B397EFC3A8995BB06DFF59540C1C2DA42F450560D2829D70AFD00B1FE4EB4C05A80FF294EF05CFCAFD441E9F2A5F1764FBA207BCBEEC8FD28D47C1853B9D48788F4AFD316FF521642FDB1B1F168C44EECAFD059F51EEEE44B2BAD27EF7AF062247C57E706681F50A801CE175A5E7BC3E417A5976467444F951459591A497F9478457F9306B43D46E3E0CAB57EFC5A5E5EB71AD7D38CE172E2BA2B4AED47638EB5D54CC2E7683AE1847F6FE803EC39720BD27453F21B639A22755F8F49EAD3162E25A6CDEF903F295ED8A6113D6885BFEEAB41A8D89B33763CBEE13E2A6109F3251183DED1B8C99FE0D0E51AA51A5F150217CC6FCED85EC13E85CBE0DB07DB7D9E8DC7B3E8EFC700579497A1E5CC6519F6F010C0C9B8EB895FB3161CE56D46C3D0ECE14AD3FF6EA2060D9597A1E74C6331CB0F05317ECC047DE1471E99867B9180C18B7060B561F44E4E02598B964372E5EBB8F7D472FE0D0F12B628A0F7E86D4B445BB84EC9CBAA4A34AEB873E9DC476768AEE0BD71CC2E8595BC57DAD2C7BC49065885BFDAD78FF8E83E750B8D620F1D086B5DB7EC081635710DC6F31AA044DC0F0E99BC5CD203397EF475ABEB99B845FB7E324EE3EF8099D062C4360B738CC5D75080BD61E41C4B055F0AF3702E3E27663FAB2FD88A7CFFA9C1F6AF6868497112D38EFB3F08C91F665F76D83F09EB3B06DCD6E5C9D3A1FD79AB717122796DF732A249E02387024BEDFB01B13A6AD47A10AD1949A3490C44E9EF01EC5C270E3D623ACDE74044D822788693BE62CD985B94B77A14DE4742C58B917B317EF14529FBF7407CED99A5164BD81C973B7C2396B33546F3182643E86765D676031C9D8B8D324340B99226636E0C99DF8B1396DBACE42795A3F73F136CA52CAC2BDABEE5F44C3B5489810BF6A8BD1A2754644794A4D781CFBE6DD2751E8EBBEA2129B8AA277107D06DF3492B7620F14AED15F3C0A731E159ED97405384D9FCBC2F368CEAAADC6C2E8D61A990322D092D29452F58789289F392012C1BD1760DF918B58B9E9188AD71B260A56E66251E2395133E97358F84BF4B36349701E46D075783CC6CEDD8E7E13D6E39BDDA750B6D9184A6528AD21E14F9CBF8DD92B0FC2A7DA60EC3E7C11CD49FAC98BF762D094CDE2A921B1B3B7A360DD589CBC7847447A1BD9DF91F006BF187A258C9F90803A523B42165BC5710E6F2BBCD8473867AC8BC2E5233178F40A7CBB6A3B6E8E9880CB351AEB4EA8C457800B25ABE056B7BE38B3782DE6CDDD845A81C3F1113FDBD546ECE409EF4B51FDD6DDC7E83174093A769B85D51B8F80EF7E8ADF70180346AFA4A87D1143C6AD16C77918B00F9D7FE3F623D4A62B804B8E4004854D15C2470F5C84EF4E5C1537844C99BF1DC74E5E133319F00311BCCB7543DEB2D1620ECAE25409CD539ABE08929B85AF4E397AC526B194E2048B7D1CEDCF52C1DA7DE89CB9C586F8D0BB03BAF45B8825EB0EA108C9EEFB656F213F3FFE72DDF6E3622C8E1B1520BE59DCAF5A3F217CA6C2E1681A3603C5EB0CA14A6C7B348B9889E611B310DC67A1186C16317899B851A462F3D1626C7BFB5E71F8A2F10891EE340A9D813397EEA255CC7C8C99B31D1BF79CC688995B91A52489A3E4F03C7953FBBE4B50AAC918ECA7AB41EB9E8B442118326D0BA62DDD8FAE23D72032761536ED3D832CFCC81B3DE113ADB8BE51E1ABC3F80949EC80A408EF08B3E4D25D516959767A55663A30D23A4B5BA5C1404C9EB11EE7E237E1CE8058F31C94B9FD84E8E78B56C4CDCE5D7175FE72C42FD88436619391CDB71D4C2C7286BA663E63C139CADBA227B98A295343346A370E8B56EE4368EF7908E939173529627B940C4737123896727B8EF891FDE250A27A5F4C9EB70D41E1D349EEC54849154C27AA989624014751C534A2DF028C9BB519355A8E4638AD4FA22B40DBE85998476216ADDE8F84EE20E6A10CEDB300951AC5C2E01A2830666D61C14014A75466115D29068C5D230A00F7C0F218FA92B50761D8A4F5088A9C89AF02472166D87214A77DD154B9E567456529162E1E8159ADCD38AB561B862BB9B53A4C20C997A06DF7B9E8D86B010A561B202ABD1D7AC661E9FA2368D37D3ECA3519293AA902EA0CC5B8793B10366839A652D49E4F69CF024A87DCCA516AA25460E7AC3C8076BD16A240F5C124F83E0C98F40D4653E168D9230E2DE8B3BA0C5E21E4E7B135A9785C8C52A9B5546E19BD0240709BBDC081EC097435439557152BE1399D31A734D5742597795DE1192BE1D575497855DA2CF9DAA2419B51581CB719D7566DC49385CBC57C927757AEC7D6C59B10D5672EF2950E47CA2C0D1344577945E119AF921102AE9C7EE6D3D6DC3C49B9B96FF968741F42396CA321700BE80297EC81C85D3C029EA5A29082D6D561066972052157D170B816EC8CAC85BBE053CAB9991C54514DE3DE1A79E8FC946E41A259323D45710F8AEE9F51FA228B2EC315DABC94AFE7A5B447159E4943690D0F30E3B1F49CFEB8D1FAC7DE1D91DEAF33DCE9DC14EE6D9087DE97997E17ADF0CC075449E6AB003F52278D4FB04873DCCBF5A482124515E15EF4DE9EC844694F8E523148E5DD495454FB90FCF3E20F62083FB8ECF855F87C35C0227CCEB2F49E8A7D4425B66ADB49184669501EAAE866A2F77FE01F8E9C748C0795B9704B8B4676953721BC896467DE1FE139AACBA2DB119E3165AC078FA29D51A3D95074889A864EDD66A071BB31285C311A1FBB35B33AD78AD7105E45ED7052F930774B14AC18237A58CDCD92096369ECA2763C693AA02CB832B6915D862597B18CB3D160CA11244868AE247444D722EE81D5B4E2C8EDF48C91522096DAA7EA00546D33111502C7A252CB71F8C88F0A9322BC4C8662D108A81F2B1EA3639988551A51698FC48457531A191BE9F347090C0528AD21DE2FE155EC082FC3913C956B2338652299758E5BF116849779A3C267E157B3F0A66C24EF3F28BC2ABD3DE19994DE21489D3F14261E3C26496E8534A8ECFF95F07AA862F32C0662260349764695DB2E3CF3417A125F454FE844E142615F7A464F72197DC9651C0BAFBD0B4A9F04E1ED457AADF00E61F95978A5F735C948C2ABD2CBE20BE1D58E272D92E85C7915D8115EED7D957B611D0AAF915E4F78151BE17D15587CAB4AACA5D2FA66846754B95F5978597A5DA113E35F223CCBFE8684E728FFA684D74A9F54E155E9AD8610F354DB82FFC7C2335AD1557425977947C26BF9370B6F496DD494E60D08CFA8D2F390035DD9195576795D57788AF2FF84F0427AC2D25CA9086FF8F86B5DC965F4C4B6418CAB37AFBF92F0CC7FC22759788BEC6F41788685375094B7115D45165EDDF69010378033EFA1F0868F1DB7C55B899D446CA44F4B955307988527D155D2D785912436F2AB82BEE45AB85D5E813B9AA8222B93A8F0EAF0606988B035247D6612DB1EAAFC7C1714C105408B41A0489F88F8F6B0925EA9B89A587A09033F0059204BCE9555157DD92D08E929AD913092DC3206778AE212460FEE813563603C29A5D1C2D22B680B802ABAB9138A655784CF17614BFE4892DC8C219F19557C4BBBBC82E87862DE1FE125D91564D9932EBC35DC032B93A8F01969FFEB084F6823BE8DF09F2BE8089F54E9B5515E6DAD915B6DF485D711DB1E49129EA496307A74B6C2C0E848AF0A2F8FB0B4115EED69158227482D6389EC9A6DB55D3EA17D3E46F0CE8417CD923C6E472129C26BA5D7133A31FE35C26BE4D7935C2629C28B47DCFFC3C20BE9932BBC486908CB70837FA3F092EC49165E4973FE13DE96FF29E17D28A5F121E9659229BCA1403733AAF08EA4D7133A31922FBC596E2BD9A57DFFEF84570557D114003DC9ADD0565C35E28B94467442BD86F06EFFB0F0AAF46F44F8685BE1B5BC8EF05AB4826BB1082E2317004615F9755A7234C2CB24557843A626562426BCBC2DE07DB2EC7A2891DEAEFCDC3BAB915D8DFA42FA9C2C37ED13A8F23B165E6D8EB4DA9F4CE1655E49784B2B8D8C8EF092F432AAF05A0CBEFF66E15F071DD155DE2BE155EC49EF4078D14C492457781B5E21A591F99F125E9BD2E8492E9324E15FAB8D5E4147749524099F89C848D24BBC55E199E40ACFEB3CE3C16B086F89F6FF099F34922B3C93A8F0AAF424AE3103E5E35A9993828EE82AEF95F089E5F43AC20BD1497A0B3993297C4EED08CA764910BEA38DE82A42767BC2CBB31DC8C26BC51790F0DE2438F3BAC21B3FAC0AC347D56C91A4D7133A315E4578C6F4691D0B36C7D5FD4AABCDAB5564AD7B63597247C8D37830C64C247922298D1613C92BC385C048222715597C358A5B89AD6023BF83796D181389AD625444B7C0D3F2714F6BA2C2D37E7E55111D50AAF424B73D3C4204464F125C83296FA81546AF30225C605061F9AD64E7F67ADAAFED81157041882662DE3FE119BBC22BC738D2BF2BE16DE1289F3CE16D10D2EBCBAD8756787BD813DE9EF46F4678CDF083B72CBC457AADF09AA86F1BE5BB11EF50F8449B25EDECD7434D6FFEA78467D9932BBC722FAC3DE9DF17E1D57595B7237C0C1183FF03A2E872A2F781A8800000000049454E44AE426082)
INSERT [dbo].[taikhoan] ([id], [username], [name], [password], [address], [email], [role], [avatar]) VALUES (3, NULL, N'ddd', NULL, N'dd', N't15ct03@vanlanguni.vn', 3, 0x89504E470D0A1A0A0000000D49484452000000BC0000004F0806000000514FAF1C000000017352474200AECE1CE90000000467414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8640000385349444154785EED9D057814D7D78777378100350A1428C10224210984102078716829EE1A084E204409C1DD82BB6B70095EDCAD4829C5DDDD4A692915DADF77CEDD99CDDDD9D94D82957EFFCEF3BCCF8EED46F6BD67CE95B9634899A536527DEE98941A5CB2D4B121A5AB352E59EB5A93AD1E5CB23B205B7DA28185945913B653B8D68329532D18B47C5E2771E87773886B5D6BE8E75991BDA17D94738CB4AE62399683C8D9C88CBCEED618467A35E6708C2167133AB7298C4CAE663A04C290DB31C65C2D60CADDD28C7B108CB95BC09847C29DA1631E410AADE9BC048C1E6D60F2D490B7AD15466F3ACFA70DD10EA67CEDC5ABD19BA175C224E800938F1923BFE6EF2830FA120582CDAF32BC8F30F97532533084E892086130F9879B2914213016B6C560485F0DC64F1D63485FDD0AA3960C3560CC58D33E9FE9ECB3A2168C24B031536D2B0CB4DF94B90E515760C85CCF21C6CFEBC394A581C0C8B8125949B66CC9203B892661CC41C26930E524E174F60B723615A21A484A43AEE6D6E46648C43C8C463E2D2C691E929070726F0567923101128F60210D0A4692D1E849024A383179DBC19970F26A2F30314242DAE743FB4840E77C1D158213F0E944AF846F67BBA4209C4848A782F44A523AFB771138150C55088333918224749629140E2792CF99291209A722111A685F40249C03A204298A46C3594B312D314851AC3B9C8B13257A587092702ED993844F47C27E6A8D295D0D2B0C829A12145DD36BF88C0415D07A061D2CC7ED9091A26D461257C198B1AE994CB4CE64AC4F525344CD4C522A183FD742D151250BE14A646D0C8380A2A605759F0ED9485699EC24AE843147731B9CDC5A5830111C590DB95A92B41C55655A91C06678DD9A84738C74DC90872227E34E72331E242FE34E91955E8D9E14456DA0A82A93D78CC98B222B61F2A1C8AA6064487213C96DCA1F0CA7FC24AE6F880249EB1B0A1361A475630142793515A0FD2A7E6130FA7581813092E08C81F6990917180B52649530F8D36BA148228A22AE194391AE30EA11100D5340371849E6C4E92E3015EF01236122B98D4C298592BD88DE2C3C45D84F1330D1B689849631A62319258CE94944990CC46724A5234858C75064CED4C82E4E9949C6CC246B267A553066A6086C05455801AD7F4E64A1755712352B896B03EF6728EACA64A774C01139283AE720A97332148509036D5BC1FB18B72033F27A2E929930E626991D60C84D915B4072E721C9DD296550301206F7F614D93B58E3A9435EA6230C5E04456D2B487633BC1E02437E92377F2841C2AA1420711DE147E73314C90D7E24B4E518AF137E91091454F027C90B754DA048B47D487843D118A2BB3524B785A23D12284651BC38C172979228D9D78C211DA50F691D63F894B027BB8281A4677465270C89C2D2539456D0933E4168C7983E6F46690D456305034BAFA2155CBB3F1B09ABC1989DA188CDE460C1E9352745633B58E4D6486E83109B04CFC3629BD713682B30E621C10913092E637027913D4862194F9257262FE1457893D08C4F676BF211F959748222B88122BAC197C465F293B4BEAABC0E10C2F3AB822AB41EAAE0854964464F6E3D84E024338B6C973E30949028A9508A24674AF733A38DF07ABC2BE10D9F51CEED48F8CF29AA270153168EEE09185C25B4915EEC97B6B3DB4675213C4775213A476F8EE2841B476E3DDE80F014D505229A131E1CD11D21457539BA736457F121F9194B642728954988EEAF11E119DE16519CE4D622096F5485D78AAF273B5394223CA52D06CACFF5E1C2D02B81120A94C6A811DE58CA22BC6D0EAFC5C0D273DECEE94D7A8AFA3A18323054203ED3C79CA33B40913E21BDD1C239BABEE02A9CF258B69325BCB45F93B35BE7ED247F4E4E6BE84A9093A175373D340520174BAF81727583C8D529BD7167C113B6CDF03E2A0894BF0B286FB78624A73CDD0A91BE4828D21B9594C6E84D8233AAF894C71B288FE794C698BFB342084139B9457E6B8C22AF3723AE082287A70223A0ED82AAF4091895426024E90585597A330686F27501E5ECFA28D2334AAEAE857377357F375005555092A0FC9D3152B43796A2889F24E1B9D29A9EA4278C0C5542B5A89553235540F530644C0C169FAE104A25D594B9BE0D5695521DE4CAAC81F7898A2B1504AB4A29C92DA3DDEF50788245CF41FB19213D61233C477F09A5126B2137918721F1DD1578DD0A457A2E0C8CD42223F024E9A9A26A055550ADE08A2BC3D273A5D59BA54FC0E0430542084FDB54713552B44F905E155F5D3763A2D447C548155C4301169D52237EE5CA2DE5F246925E5B59654CFE9166A8A2AAA2565C8D4518A5B2AAC1227E5116DF2CBFB6D2EA44290FA3955E545C0953A95E04457B21F3A72A6A2B8C063E266D1BD393D83292F0E6486F47788EF4024A7F04E675738B0C89AE22A427C159720583A081C0B6752641722B487843169259262B4BEE004B0B0D89AEC2D20B587686A496C9494213E68AAC0E14E565CCD293D48C25A24BB273F4B7A43666B8B5C63AD24BE2E765D96D11A24B9828CAAB1845C42728D20BD4CA2BA73802CEEFA51C5F41B4D6E8C0B227E4F25C595520D119B3F414ED49786EA131A98848AF8ACFEB5D6122C9658C14E11320C159780D26AAAC3246154A691835BDE1161AD14A239A1D459EAEA0A42E56C8C70923EDD34F6912B049696C72F604388D3129C82D37AAE082CF13D6F564670C9CD624869CCE38C022BB0683909E04D781F37B3D4C94C7CB18455A4352137A2D3432AAF822CF4FB6F024B484892AB0329CE2A8985B6A64E155E939B549404E69CC5034572BB7B2E832527A23521C7F969D645690531CB3F0D15658447784243BB7D4184B90E044423ECF2D35FF096F173DD9993723BC59F6D7139E1069CDBF537859FA44852FF29FF0BAE80AAE45476E3DF46467DE1BE155D965DE81F05A5E4578597A4112843768484C784B8B8D2ABA8AF11584E77D56B2BF01E1F530501EFF4685E7161B1DB9F5D0939D7923C213EF44784F730B4D9284171D510A6F52781DD955DEA5F022DA0BE1B915462B7462680B040F2DD008AF4A6FE20A6922C233BAEDF53CCC80A56724E1ED89AF2B396169A2949B27B5CD9176D0153E3B5554191DE9193DE9651CB5D15B4457D62DB2F3BA68B591C44F447873D3A5B97952165F95DF5A7805B97D9E85E7367A1947C2AB2441764615DE40955715ADFC8CB99DDE567AC6AEF02A4A4A634971DEB6F04989F0CCDB149ED16D93FF1F13DE46FEFF844F2249103E65E67A489BBB193ECDD31CA95C49484E51B4424BBC0DE18D591A230549FA917B2B7C9AB70DD2F0802E16F91D0BEF9CBB9540DDFEA785B748AF27BC2AFDFB24BCB66756F4CE264F7813C9EEF4B68477716D801A4D0763D6D4D59831391E1DC22721A04A0C7215EE84CFF2B6820B77F468E4B6919D9185D7C8AE2F7C239848E2B49EAD91BD7067F8558C418336A3113B6229E6CFDE800E115391B1007DF1AF237CB6E409EF44E216ACDA07159BC62295479B7F56786D8A634F78213D09CF636D5E47F844A44FAAF03643119223BC92CBB3EC16E1E571336274A466DB4670DA6F530024E10B57EC8A233B8FE2C198C9B83B78249EAEDB841FBF3D8ADD2BB662DCE8A568113C0E25BFEE81BCC53B2383670BA4F89CA33F7744590B6F5389D54A4F05C148AF1FE50E44CE229D50A84A77540F1C81BE831761EDC24DB8BAF3209E6DDF8D0713A7E3DEE051B8B7E7109A75990C27EE3C5285E7CEA864C92F09AF601E6793000F2E732FD30D5E157AE003AFF668DF631E4E9EBF8562B50692F04148453206D41A805C65E88BB323BE0C8B9FDABB0352E66D2FE4170540959E508707CBEB3C2C5895DB4A72098317494D18BD3BDBE21322B0115E411D7E90BA5057A46629497A93182199207E0A123875D16E70629159701D84FC1AE96584F085A8E24A92CBC8C29B871DF4B0925E8B89A467CC113E6D0D812CFAEB08DFACC3189C9CB31CE703CAE394AB97E07CB14AB8DE3A04F7474DC493D51B7069DD76AC99BD1643862D44732A00656AF4867BD1107CC8375728E98F3DE15DB2374556FF8E28F26577D46A3902D1FDE763FEF4D538B2620B9E6DDB8D47B3E2702BB2272E7D5917673C0A997F876C3E7834631EBA0F5D820F3D482449F84FBDDBA271F00434EE34D142838E13908FAE109E65A250A7ED58F3FECE93687D1CBCCAD33F9844FFC4A73D1A044F24910708D15D2802FB5334AFDC6C04E2561DC0C8E91B29BAF7459DF613B07EFB0F981FBF1F651A0C45798AF6DF9FBE8E5EA3E251B6F17054A0F33314A288497297A2E3F53A4DC6C7BE24236DE728DD150DBB4C4548FF4568D3632E8AD51D2CC49723BD3315AA8A2D47A371F80C41DD90A9F0FAAA8F95EC6E157AA271E42CF8D51A2C6EEE5085FFB2DD24348E9A03E77C24B0227B9A82E128D164144A341D6D11DCB3DA40D40E9D81CC657A88EDB4C5A35133643A4286AC44F0A0E5A8D066323E08200949741792D4AFC14804F5598C90E1F168D82D0E39AA0ED2153F29C29B9228BC23E97585D793FE55846FD1793C4ECD5884F385CB5984B790D55B4878A94A1DDC0C89C683F1537167E96A1C5EB40E8B66AC45DFA18BD1A4C35894AAD60BD9FC3A50F4E7B1F00DE84A40A941A56EA8153402117DE662EA94D5D83E7F1DAEAFDC8087B317E04EAF41B85ABF25CE152C6DFB33151E4D9B839EC397E2238DF0B98A8761D5C623D875F00CFE7CF9172E5EBD8765EBBE45432A0461FDE270FFD14FD87FF402B6EC3989EBB71E61259D9BCABD35BC2B74C76FBFFF81B9CBF70AE1D31708C698997465B9F95044F4B631733074F2065CBEFE00DFEC3C811D07CEE2E4B99B58BBED7B2A00C7519FC4DEBCE7142E5CB987BAC19344245FB9E93BFCFCFC37E4ADD4132E742598B56C2F1E3DF9193BBF3D87635448F61CBE80724D4758A537690A74C2DEA31771F4D4356CA4DFF1D4C5DB58B6F1283CBFEC2B6477F20A46AB1EF3C1CBF24DC7C47E55F8E3E76E89FDA979FC8B227C96323D11B7F63016AD3F6A113E6CD84ADC7FFC332AB69928B6BB8F5D8B07B4BD7ED7296C3D781E67E96F08ECB910264A63029A8EC116DA77EEEA7DF17AE9E623CC88FF16E9CA907074FCAD081FD0DD22BD23F1CDC27F625FFA372EBC168ABCE7FCCBE04A9DE6B81DD90B0F264CC785B9CBB063EE1ACC99BB098346AF400F4A51264D5F8FF5F337E0C4FC78DC99360F77FB0EC5F5C08EB858A62A4EBBF9EA7FB6067BC2A7740B845BF150D46C3D1ACF7FFD1D3317EF428E62A1484B919F85BF75F7095A864F836FA5EE5844919B25CF562CCCAEF017A8C0F419BD0AB5DB4DC0E90BB73175C14EE4ABDC0B45AAF7C7951B0F70E3CE6394A648EE5DB927097F122FA9902D5AFB2DDCCB77B712DE9BDEC3C7A6D1EF5398D2A1C0A89938F8FD65840F5E62165E913E8D6FB0103E62E832E4AFD60FCDBACEC21F7FBEC494C5BB85F09F97EA8689F43B3CFBE5375CA502DB246AF66B099FBE540C6EDF7F8A5574D5F2F87A108A35198D3DDF5DC68E431791EE8B9E088D5D85CB2479E4C835F0AD37020B367C873B0F7F42AEAF07BF5DE125E9F564676C72783D6C04D7A22BFCE2A409AFE174EE82B850FA2B5C6D1884DB51BDF160DC54DC1A3F1D77FB0CC1F5A04EB854B936CEE62B4605C55BF7FD8EB0125ECEDD096EBD09A8D61BBF906CA3676CB4E4ED2CFC9D7B3FA2F7C815A8DD660C45EA1F70EDD643A4CDDFC122FC1C129E539C74BE1DE9BD9BB066CB31F890B02D49502E2C25EAD0E5DC8D7271CADF27CCDD867397EF8ABC9D85DFB8EB044ED1D560CFE1F3684729CB06FA7C213CC95EB5F558BCFCEB2F946D120B67CACB73958D415DBA2A14ACD19F64374777BEDDAF12A533E729C24E5DB21B4D22672205E5FB7FFF0D9CA4C2C6C27FD17414AED055671B5D61B61F3C87A8D89570E1161849F8547CBB9E46F803DF5F41B398F982B87587F1D3CF2F84F0BE7586D155EF190AD61B0E63BE30A42ED2155124F791533744741F34632BD6EC3C09AFDAC3448A5384F635E9BE001F972219F58417D2133AC233467F4D1EAFC86E81532929BAEB096F95D2E8492EA32BB90C8FA2E4961A85169DC6BDB2F05ACE7A0590E0C5713A477EDDE3C9E155857F405FEE714A270E5174E5E3AB3651E423C1ED091F4FC7BDE85807AAACDEA148E85D91BE00E56EA8AE43968A2B802A3CA73A1CC147CFDA8C9D07CFE2FAED4716E16B52EECFC207D41E04CF8A3DB166DB71C46F3E868E7DE22CC23B5141684D05E5D6BD27D8B0EB24064FD9206ED666E14F91F0CE3EC16810360367A890358F9E83B1F37660D3DED3C85F7D60A2C273145FBFFB94E0F4A5BBF8FD8F3F85F0851B8E10D13E4D619296847721295B51BEFE3D7D56E50E53316AFE2E2CFAE63BE4FC8A7E865281B5F08E84D7F2AF11FE4DF2AAC2DF2699A2062E44B90683B178CD4191A3672A146295D2B0F019FC3A61DCECCD16E19B864EC38DDB8F51892AA5CEB95A0BE117AE3E6815E159F8E153BF41A1EAFDB065EF2921B82A7CE516A3C5F697416390A17028BAC5AEA002F1185316EEB208CF3317E4A4C87FE4E455440D5F0E777A5F4A9F8408EF5A3A060BD61EC2D367BFE2F0896B14E91F89485DA3E3E44485DFB4EF0C0A358C158C9ABB034F7E7A2E84F7AE3918F728087CD97EB210FE838068F49FB209DFD2E7FBD4198EDE933662F38173A2E2CA92D70A9B852133B7E2B3B2BDC5F65B115E4D6934A9CDDB119E91C6CBFF7F13FE26E5DC5F358F85315B53546A328C84F9154DBA4C41B6A26142CE43C72FA356DBB1E8D277014E9CBD29A27C3AAA48E62C1985635491DC77E402CA361C869A6DC6E1479266FB813356C20FA3A89C8252939EA3E2F1F8C75F2CC2BB96881495E87D949F97AC3F14ADBACDC13DCA8565E1E51CBEEFF8B5284B856BDCBC6D5410FF44CCC855285C77A8F899DF1EBF823EE3D761E4ACAD22F2870F5D8E0FFCC31D0ABF68FD11DD1CDE85DE7793D2BC0BD71EE08B161350276CB6D8DE4292A72E1A8DE0C12BF023153096FCEBCE33B0FDF0453CA4BF4B8DF8EF81F08EB1959B22B903DE6BE18791F0EE24BC4DAF6C1314FD5A117E3A092F6EEC6E46C22F50841F41DBCD91AF627721505CFC7EB8E46E8DE614C5394F7FFCE3CF78425FEA4E92B94CFD21E23E5853CE1668D479B268F5E1969E4774CE0E3A9EA72C7D411AE1B90932639130D16A2384A73A8013C9CC4D940F1E3F1339F3437AE5969A9A541956DBE3990FF293F0472E8A82C8E7DE79F0140326AE43DA4261A849919C2BAA81D173F141C150A40B88C4DCF883384E05D3AFC6607A552AAD0548F8BC9D0459BEE881B83587B09085576EF8D6B6D2940D1A8F9B777FC483273F8BD69A833F5C43F16663457B7CF64AFD3169C93EF13B3FA2FFC9C327BF88E64BBE12F07127BF480B26597A19AB02D0D582457A457C93457C925CC2184069A484457E3DC1B5FC2F08CF3851444F933B0829B9375499C9C099D653E7690D27EE50A26D13A52EBC9D8A870A640B440A12375DBE8EF02E17839CC522F001456A3E978F719AE3E4D6121FE66D0B8F72DD90A74CB4B9B7956F00A1F799487217CFB614DD2952D33AC34D91DCC964E29E55DA66E9B94DDE8B227E2E2A287CCCD99D673348109E49EDD35188CFA4C9172CDAE6B967D5C9AB23525305B540CD41E83E7A354A361E8194F93A23B56F084C798391CA37146924D919935767AAD48622251D538577A62B40EAC21162BE1A16DE243A9D22442AD36DCC3AD1F9E45480F273D101158E94FE5170ADD80F5E3587227DE99E484112F331B5834A26F188AF2F3CCBAEF29FF03A2426BC185C96855219EDD41D7670CAD1D22237C3EB8C8967395084B7C00586CFD50E2D505184D7431D6E608546782D22DDE1A1040A8D22668A2B47F488784B67943CB5872CBC0AE7F8AAF06A6AA385F37A4E6D9CA832CAB31CA8523BE23FE1DF116F4C783AF62145E6061D2752AA335244FC8C94B357A1FCB966EBB1702B415FE2EB0AAFD9FFBAC25B24977903C21BF2752142CD2451781BDE7BE113C12CBC75C7D3198FC256F2BD6D4E67CF277A744FE74CE8944AB6F00EA4CF1610269AE91E506EDEB1C75C741FB64C745A71BE5EB7DDF8371AE17521E955EC0A6F6F4E1B8BF409C2CBBCB2F08AF4C9165F2B3C5F2DF8E66F965E889F20BC8C2CBFA1708C359A026011DE9096274CD5175D454F6A47F0E030ADF0D75B75C6A52A7571C6DDDFB2EF4D73266F113166E772D5FAB852AB29AED46C82B3BE252DC79324BC2CBB03E15D280F5FB6FE90E89462D1395DB84695C36193D6E173FF102BE1CD03CACCB2BF31E11907D23B14DE22BDADEC8C95F0F28C65BAC24BB2BFAAF4890A6F1DE575D10AAFF24F097FAD45473C3F7C4C8C79B958BE064EBB15B01C4B0A67F2F8E3C2175571CEFF0BCB5082D3B9FDC400B5CBD51BE17A9B10DC1B3E0E4F96C6E3C9A215B8D36F28CE6952AA37293C93D6BBBD88EE63666C42EC940D080C9B26223FCBEE30C2EB49AF277462BC2BE11D4AAF48AEE0E41B86548522A9929BF49C5E08AEE5DF2E3C4BFAE3F2D5F8EBF9733CDBBA13B7227AE07CD10A6230992CA50DD97C70BE4879DCEED60F3F6DD88CC7F316E3DEE091B8D37320EE0D1B83274B56E2F9F73FE0E58F4FF1E2E4193C9838435C49F43ECB9EF01FE609426097C9681E3219151A0E850BCF1DA3119E2BA3256B0F44D3902942ECCC14C55D48BA86C113D182B69BD07EFFAF7A5B64578577C9D30AB5DA8E4360F834D46C33169FF0C4471AE1B97992C7C93025EA0D411A9E204923F727544750CFE11195C9113E63F128D4EB320D81D173D02C6A36DC2BF726B16D850FEC360FCDA3E7A25AFB29F8AC38A5092CBC247D813A43D1A8EB1C04F68843918623C4644C2CB991C4CE54A617AA779E814E8397A3FBD875881CB11A4D63E2E0DF60A4A539D21EFF72E1A9D2AA233C8B7DA9522DFC72F008FE7EF9127FDCBB8F1F291A5F0BEC205211594C0B24FB8D0EE1783C7F319DFF40B417EB2DBFDFB889C77317E15AD3B638E56008823DE1F96E281E05C99D3E7B0F9D43993A0329DA532EAFE6F3599BC1B57017ACDEFC9D18B978F7FE538AE4A1C851341C771F3C15EDF2DC213464C25A1BE13FCDDF11474F5C15E75CBE7E1FBD47AEA448CF513E41F84E7D1688E3CCC4F9DB91A5187DD91AE13D2BF6B09C3365E14E2BE1ED55625976A3473B946B3E0A37EF3E11EFE576F1B6BDE2E0ECC5F9BB750EFFE34FBF8A5697F357EF232A369EA23CEDF762CCC2478F5A8DABB71F9B3B95A66F464A6ECECC178682F54760FAF203B878FD21D56B5E2ADF0A4467138FA46CDB7F29D296A4CAA38EEC8C65D8818A457A929D1183CB74249730EAC9CE14614878B5E2CAC2ABE8C96E46BF079611378C70EB0C4FB8AA4CBADAA2D30412DEB69586C7C3706EFD386E09FEB8730F7F53A5EFD7E327712BBC3B2E94A88CD3B9FCACCE67E1EFF61F869FF71CC0CB273F2AFFC684E5E54F3FE3D7EF8E532A33568CB84CEC6AC1C2F722E13F26E1E539299D5C9B60D6A29DE2339F3E7B8EC1635789FD5C18CC694E1334EC305E140A5E78882F47FD861D278A6D5EFEFEFB6F5120B217A5FC95445749E71B2C86FFAA0B178CF28D8659E5F49183162B4781D9CBF6206B31FAD2D52B80223C8F9C549779F1FB6D5B6D0813492EC3B30EA7F20E464B8AECF23237FE00DCCAF504CF6A905081EDA01C352F3CF6A6722BAA7C2BE90DB7CDF79DB04114085EC6CEDB8954247CDAA231D8B4EFACF8FBB957F8D28D8788DFFA03B67F7B41142E5EAEDE7A8CF2AD27D94D73AC223B21E7F402125EF4B63A406EB161B42D3966E9897729BC805394621571B7DF303C3F7A5CA422CF76EC11A909A728DA348773F6CB5F371063E77FA6F3C442FFDCDF2E5DC1CFFB0EE2E1E499B858E66BF1B9563F47077BC233951A0C161FCD5FDA92D50790926FE7538477CED11C3316EEC08BDFFE10E7F41CBE5C7440ED3D7C5E6CABCB55FAB29B874E75283C7F3E170C177EA4CC3B103E6799EE58BFE384F24EF3C283C2AAB41AE750781E9A306BC5017CC40F30B023BC0BE5EB83A76DC15F7FFD2D387CF23A2AB79D8CBCD506A360BD111843E7A8D2AFDA7E02E97962D3B724BCDC6223F3CF0BCF909C57EA36C7B3CDDB453EFFCBA1A3B44E429D39875F48E22BB59B590B4C05E072B546F8EDE265B3EC17AFE0F99163E2D641FE1CB9E9D1118E8477F50B1683C47839FCFD25047C455F8E227CC6021DB075CF49718C871EB8978E4226BF4E62CC390BCC5DFE2C083370EC6A31A4C05AF8BBE2BDEAC26951977E0BDEBAF0FCF084A275878A56A4977FFD25068DFDA2AC378B9C45690DA732FAC2F3C2431482FB2FD1157ECCDC1D247C28EED2158B171E4B53ABCB4C4A7142A9726BC6ADCA006CD97F4E1C7FFEE20FE4A0EDFF4DE189737EA5F070EA6CFCF9F0117EDE7F08F763C70BD9FF7AF1022F4E9DC5D90209CD89677D8AE1119DFB377D519CAFB3ECBF9E384DF97D04A541496FED71247C1AB716183E71ADF872B86D3DA26F9C45F80A0D07E3C4D91BE2D8B7C72EE113AF766819315D6CFFF8F43926CDDB864DBB4E88118AEBB67E0FEF0A9437EA08CF232B4F9EBF29A221DF41E5568ABEC8B7283CA733AD62E689F7B0BCD316EFC6A63DA7C4F6EAADC791A7925A79B5169E0BC6D153D7459AB26ADB0FC853B9AFAEF029F3878A42CFCB890B77F031CFECABC8CE9828BF77ABD25F447BCEF379B8C1DB12DE36A74F10DE3CD84C477895E40B6F165D2529C273D4BE5A3750A4357FFFF6BB10F9C99278FC79FF21C8083C9C36D77C1E45FAABF55B50BEFE0C2F9F51CE7EEC075120EE0E1E95EC367D47C23B676D8AAF9A0C135F1E77288DE311938AF04327AE11919D97A088694893A7B51850C6CBB9CB779093040DEDBB406CDF23B15A844FB3CC6E20A734DC56FF457D73EAF4CBAFBF61C2DCAD22977728BC825678B52030AAF0F23EC6B544572A60B7C57BBED975129E24F8C8995BC4F6B35F5EA074E358AB664A75E18165F5BA980B34A7715D63A94E4351BEEFF8F536C2F3158E171E75E94272CAC20B38E293E47A5884B7576955E1CF551EACA0273BA327BB157A95D6772A3CC1A9C88DE048FC76F9AA48557EBF761D3FEFDA87BF7E7D21E05CFFAC57517163361F7F71FEA29885E0D9969DA22D5FEF331DE14878264FF1305CBB49058E96F55B8F2157B150D182B374CD41B18F5B71DC4B45C2AB6C34289853B9FC0B3BF69F16F93CF7AEF2F227453CEE754DC14D9BB4DF4A7892EC83BC6D3187A4E6E5ECC53BA81634FAAD09EF55A58F88D27C4599B362BF78544EAFD1AB4581E6FD753B4F31B7D628D2AB0B47F7CC25A3313ECE5C91E711951583C6275978BFBAB1881EB5163DC6AE3733CE4C06BEE3492A004916DEF28C28FBD2FF2B8457B950AA8AE82C7AF9F427D1D6CE690E2F3FEFDA8BEB41C162FDCF478FF10BA53EBF91F47CEB9FDEE7244662C27FE2D10AFD47AD103F8F5B645A864E41FEF2DDB07D9F390DE0A1BFAE853AA3DFE878B1FD82F2D25EB12BC460B1825FF6C29E43E67C75C7FE3328FC751F5DE1B9D7357FA51EE2868C975460F86EA99E23CC3F939737253C8FAF0F1DB8449CCFCD91FDC7AF831349CD39FDEE43E6CA36DFF49DAB3CD55594CE297561E139E2D7E838058F9F9A2B9D7D48D8E133B62449F8A05E8BC4FDB32FA9A071C15217AF1A43CC39FEAB0AEF40FAC484FFE7531A0D972AD612F7AF726F2C4FB3F1D78BDFC0EDF50F27CF12A9CC8B33E74505975B73CEE62FA1FB19899198F09CD6546B1E2BA2F4734A397AC72E43589FF9A252CA51B245D81464C8DF41DCC5C40B0BDCB6EB4C31882C286206566C382CF63F21491A759AA42B3CEF4BE3D906A15469E59F73EFE15331AB81BABC29E1B9A34A95953F3F6AE87234A434A559C42C6C3F7056ECE7A550CD41960E2A7551854F5320143D46AF115273A1F9FECC4D515079D1179E6424E13396EE29C6BF2FD9780CD794FF152F5ED549781F9EC02999C21790A47F1DE119E3272CF9D712AAF8FAF3D4307AB31BC89D4FBA1D4F8970C6B3B0E8897DB67517EEF61B2A5A6C441A73EE021ECD5948698CB9158723BEDEFB93020B2F3A9EF8291BFC084C09557AEF3251A2958603D3B2B5DF62C5FA43E2CBBA73FF4714AAD213A56AF6C74FCA976E6FE1C21143690DA72F7AC2335EE5BA63FF910B2202CA5170CEB2BD243C7DC1C9105E0BCBCF1D5549593AF45D08179E654C13E1C5CCC39E1D51A6F9181C39794DECE7A8AD565285F0F9BA8896295EF89ED7AC657AC3C927CC8AB8B5472C7F9F59F850D159A562E057457C463B7498311420D17530FA91F884280492F05AF145C5B5500C4129CDFB223CC35375FC4682BF7CFA148FE396E2AF9F7F11B93CF7C83EFFF6A89848C9A6732A192445F84F3C5A8B7378B94B92734F2A2F2B361CA29C3E0C53E3B68B1C9817BEB1FBE80F572C7C4F2902575A79E159080A7ED55B5FF86C2D28C76F853A6DC78B7A81BC08E18BD2972CF5C4BE8AF0B1D3368A737FA54A27CF917394A455F98E84E688CD0BCF6690A93809624778EE810D19B8CC92CAA88B2AFC77A7CDAD577C7CC0A48DF8B4680C4CDEA1828F49B6E59BBF7728BC2011E18D3AB2275FF81EEF9FF05C89BD3774B4E8857D71E1125556778928FFD7F35F45EB0C8F82D47B5F52498AF09CD6D46F3BD6D22AC3CBEF14C5024326C3D5BF13CE5FBE23F671EB4540D53EF02EDBCD02E7EDD3179A2B7ADC0A52A3F5187163B79EF04C768AE4B397EEB18DF08908CF85A971E8342B1A854E4599C6C39192EFA0F26C875F5FFC2ECE3D4B91B76483E1F0F9B2AFC0FBABBEF0AD36005BF69D16C779F1A4CAAD23E17357EA8BF82DC7C5954B5D54E12BB79E44A993B930DCB8F3043DC6ACC357EDA61053113E345E447E5EF8BDDC21C529CF7FC24B706A73BBE740FCB2F7209E2C5B85DF6FDC12BDB177078DD03D3F392445786E8AF4ADD00DDCFAA22E97AEDD43E95AFD502B689425829F3A7713DA871E3BBBB544973E7142769678C0B8D5C8F34557BBC29B72B444E5A623C4500575498AF0F6168EE43C659F7F8DFE629B25DB461568F901C7E6F135ED3166CE36D109C54B9F716BC5AD80EAA2159E9B249B46CDC1E51BE6162C5E54E19D49D8BE13BEC1ADFBE6FFCB0B0A0E7CAF2BA3F64C738158B6E97B64FCA2D7FB2DBC9ED8426EE99805DECF502556EF069064C13DAB5FD6358F881C3E568CA53957B8ACFEB9C9C091F01648784E6B3A44CFC44E929E193A7E0D7214094178DFF9D8B4F3076CD97D122D28A25A84970699F956EE8149F3B789396626CEDB8A7C957A881BBE799B27719285673EF6EE80A6215311BFF1A838A7DB9065C858902A761AE1798E499E6ECF112C773AFF100453A1DB79F01C36EC3C811651B329CD692350856702EA0D15636A76D0FB967D731429BC83B1F3D079C1B4257B44C4373FECD83CB0ECC382E168DB7B21D6EE3841E75C40A7014B9182275CF5EA820F48B8767D1663E1BA2394EF5F1705E3DC95FB3878FC2A9652C5352276157254EA2F3AA2587819165EADC4AAD808EF4B0140464F78EED46274C467DE6FE1157848F1F922E570A5414BDDE3C925A9C29B5C9B228B5F3002BEEA25E0A9B753646F06AF2FA268BB37FC2BF784936B735DE17918718E121108A8DE0F7E5FF6024FB8EA4BE7F37621A5A952169EF9C0A31D3CCBC6D039FD91AB6457B8F013B835C2A7F66E8F805A031D5284E0E6485F4AB578BB40D5FE70A148AD0A2F1E67AF08CF11DCA3526F14A93344C86FA26D7E657CBEEE6F233C93DA37948ED1CFAA371C39CAF512BDAF2C3C938244CD59A11FCAB598805A9D66E0EBF65351BAE958E4AE32C0B643CA9B0AF4EB08AF489F3CE1BB275D782D36054086846FD466244EC4AD121D477AE2259B4446412685D339F38BE6CEA8FE0B90C68DA4D3939D51D21AFB34214870424F78ABF9E3EDA1115EC598BDA5991C24B946781B742AAB36B891E48C2CBCFC387B1521B71ED6C2CB709A235084B78B24B785448497531CC646F67F5A782BE949F86295A37174EBB7B858A1A6AE7CFF043CEAF2D682E568DE79124C3C09939EEC8CAEE432FF0F84D74AAF2B3BF37AC2734B8D91B0929DD10ACF9D51126F5578F3CCC1AAE4BCAEA22F3BE35078228D6B032C5ABA137726CEA44AA89D9B3BDE213C0EFF56580C76C6EF40514A510C99486C0523499EA54007F41FB9C296512B13213E81D112635625C2EAC419CBAC3133EE5559AB619DE871B56282CA7A3B6C303351CB370A1BD17F92046F5BF66D42FFC9097CC6C38365F1B5C2AB115E13E565D139BD11B0F056D293D8DC01A552283A017FAABCFA93F0FE2CFCC724A98ED47AE889AE624A6BBD5DA07418BE3B741A0FA6CEC659EF005D11DF053CC0EC66680CAEEE3B8AD65D26C185F27363C686164C247DCA2C4D90216F1B41FABCADCD78D13A91C1BBAD0DE91532F8B4D3257DBE0E66F2771464F00DB622BD6FA7A451A0B320835F880DE90B1254417548C1500B19FCC390A150B83585351489D01089F445A29021A0AB2D45A3156290A15802E98B76437A5E2FDE1DE94B303D90A1A41967169505D748AF4D67E46D759F2ABA892467ACD685F024B684D1BF9B3505BB93F8A2A7B596AEDC7AC8426BD10ACF4386BD8A76C2379B0EE3D76B377033385257C8B746361F5CAC58133F6DDA865327AFA051DBD1247B632BD955E1650C991A9A51521CB5B952464D6F7824A51E22BD11298EF97E58F9B63F71EB9F4E3A634376826F0524D49987650C6E3A298C1635A5218CB9DAC294BB9D156A53A50A0F2E93311206F78E02A307A53184BC6EF2EC4484506A93804181531A91BAE8E5F05A34115ECBBF427835BD714A5F1B0D5B8FC0B5EBF7F174F7015CAADAC03CF5F51BA888EA92DD474CCD717FC274DCBD7607A326AF85AB2F7D799647E7D817DE9924360958F457139E5B789C73B4400ABE858FE55684E71BBE2D3392E909AEC194931F68DC0A69F307E3E0B14B285A6BA0D8D60AEFECDE06293D5960EBB134293CDBC2999B21ED08EF4C32F36032597817EF60A4F4EA682D7C1ECAE315E96512A47720BC2ABD9EE432AAF076A4FF57092F485713697336C180114B71E3FC35DC9E380B17CB56130F3FD095F615E089977850D98D9068DCFEEE14D66FF90E25AAF6542497D117DE256B5394AF3300758246A174CDBE62A830B7E6244778963D7B9110C40C598AC1E3D720578948217C26BFCE1831F51B71D3F74779492E1DC165B8438A9F0555AFE324741BBA0CADA367E3EBA031C85C98725E4978539ED6621683F9F107E05599F26355768FB6183A65031A864CB3C8AE0AEF94A73DB2958CC6170D63F165D038A42B444291ECCE54411D366D1302BBCEB6155E23BD33575615E99D48EE7401DD60F252A4572AAC427CDA6789F23236D2D3BED715DEEF2D08AF228BADE250789E379E5E9D32D4867FD908C42DDE814B7B8EE07AF701385F9C6FDE4EDE1C3556D095E21C45747EBED3CDB55B70F8D845346E3F96A2350B6D7E389AF59301AD8557F9306720464C5A871367AEA3EB8005D8B0ED7B34ED34111FE46A811C8543E051221CB98B852265F6E6F82C5F7BB8D3B647A908B8D0FBCCB237A1BCBE1D5A474C134FF43870F4A29871D8B34C349A749E2C7A60F906EFBE54C14DE3DE16794A7745AE9251F894A7EDD008EF4C42F318791E6753ACE600F4A6F7700F6CC6425DE04C913C53A1507856E88ECF03C210183943F4D436099D261E99C3B2576C3E12B13C03B214DD1943CED6F8C43704B397EFC3A8995BB06DFF59540C1C2DA42F450560D2829D70AFD00B1FE4EB4C05A80FF294EF05CFCAFD441E9F2A5F1764FBA207BCBEEC8FD28D47C1853B9D48788F4AFD316FF521642FDB1B1F168C44EECAFD059F51EEEE44B2BAD27EF7AF062247C57E706681F50A801CE175A5E7BC3E417A5976467444F951459591A497F9478457F9306B43D46E3E0CAB57EFC5A5E5EB71AD7D38CE172E2BA2B4AED47638EB5D54CC2E7683AE1847F6FE803EC39720BD27453F21B639A22755F8F49EAD3162E25A6CDEF903F295ED8A6113D6885BFEEAB41A8D89B33763CBEE13E2A6109F3251183DED1B8C99FE0D0E51AA51A5F150217CC6FCED85EC13E85CBE0DB07DB7D9E8DC7B3E8EFC700579497A1E5CC6519F6F010C0C9B8EB895FB3161CE56D46C3D0ECE14AD3FF6EA2060D9597A1E74C6331CB0F05317ECC047DE1471E99867B9180C18B7060B561F44E4E02598B964372E5EBB8F7D472FE0D0F12B628A0F7E86D4B445BB84EC9CBAA4A34AEB873E9DC476768AEE0BD71CC2E8595BC57DAD2C7BC49065885BFDAD78FF8E83E750B8D620F1D086B5DB7EC081635710DC6F31AA044DC0F0E99BC5CD203397EF475ABEB99B845FB7E324EE3EF8099D062C4360B738CC5D75080BD61E41C4B055F0AF3702E3E27663FAB2FD88A7CFFA9C1F6AF6868497112D38EFB3F08C91F665F76D83F09EB3B06DCD6E5C9D3A1FD79AB717122796DF732A249E02387024BEDFB01B13A6AD47A10AD1949A3490C44E9EF01EC5C270E3D623ACDE74044D822788693BE62CD985B94B77A14DE4742C58B917B317EF14529FBF7407CED99A5164BD81C973B7C2396B33546F3182643E86765D676031C9D8B8D324340B99226636E0C99DF8B1396DBACE42795A3F73F136CA52CAC2BDABEE5F44C3B5489810BF6A8BD1A2754644794A4D781CFBE6DD2751E8EBBEA2129B8AA277107D06DF3492B7620F14AED15F3C0A731E159ED97405384D9FCBC2F368CEAAADC6C2E8D61A990322D092D29452F58789289F392012C1BD1760DF918B58B9E9188AD71B260A56E66251E2395133E97358F84BF4B36349701E46D075783CC6CEDD8E7E13D6E39BDDA750B6D9184A6528AD21E14F9CBF8DD92B0FC2A7DA60EC3E7C11CD49FAC98BF762D094CDE2A921B1B3B7A360DD589CBC7847447A1BD9DF91F006BF187A258C9F90803A523B42165BC5710E6F2BBCD8473867AC8BC2E5233178F40A7CBB6A3B6E8E9880CB351AEB4EA8C457800B25ABE056B7BE38B3782DE6CDDD845A81C3F1113FDBD546ECE409EF4B51FDD6DDC7E83174093A769B85D51B8F80EF7E8ADF70180346AFA4A87D1143C6AD16C77918B00F9D7FE3F623D4A62B804B8E4004854D15C2470F5C84EF4E5C1537844C99BF1DC74E5E133319F00311BCCB7543DEB2D1620ECAE25409CD539ABE08929B85AF4E397AC526B194E2048B7D1CEDCF52C1DA7DE89CB9C586F8D0BB03BAF45B8825EB0EA108C9EEFB656F213F3FFE72DDF6E3622C8E1B1520BE59DCAF5A3F217CA6C2E1681A3603C5EB0CA14A6C7B348B9889E611B310DC67A1186C16317899B851A462F3D1626C7BFB5E71F8A2F10891EE340A9D813397EEA255CC7C8C99B31D1BF79CC688995B91A52489A3E4F03C7953FBBE4B50AAC918ECA7AB41EB9E8B442118326D0BA62DDD8FAE23D72032761536ED3D832CFCC81B3DE113ADB8BE51E1ABC3F80949EC80A408EF08B3E4D25D516959767A55663A30D23A4B5BA5C1404C9EB11EE7E237E1CE8058F31C94B9FD84E8E78B56C4CDCE5D7175FE72C42FD88436619391CDB71D4C2C7286BA663E63C139CADBA227B98A295343346A370E8B56EE4368EF7908E939173529627B940C4737123896727B8EF891FDE250A27A5F4C9EB70D41E1D349EEC54849154C27AA989624014751C534A2DF028C9BB519355A8E4638AD4FA22B40DBE85998476216ADDE8F84EE20E6A10CEDB300951AC5C2E01A2830666D61C14014A75466115D29068C5D230A00F7C0F218FA92B50761D8A4F5088A9C89AF02472166D87214A77DD154B9E567456529162E1E8159ADCD38AB561B862BB9B53A4C20C997A06DF7B9E8D86B010A561B202ABD1D7AC661E9FA2368D37D3ECA3519293AA902EA0CC5B8793B10366839A652D49E4F69CF024A87DCCA516AA25460E7AC3C8076BD16A240F5C124F83E0C98F40D4653E168D9230E2DE8B3BA0C5E21E4E7B135A9785C8C52A9B5546E19BD0240709BBDC081EC097435439557152BE1399D31A734D5742597795DE1192BE1D575497855DA2CF9DAA2419B51581CB719D7566DC49385CBC57C927757AEC7D6C59B10D5672EF2950E47CA2C0D1344577945E119AF921102AE9C7EE6D3D6DC3C49B9B96FF968741F42396CA321700BE80297EC81C85D3C029EA5A29082D6D561066972052157D170B816EC8CAC85BBE053CAB9991C54514DE3DE1A79E8FC946E41A259323D45710F8AEE9F51FA228B2EC315DABC94AFE7A5B447159E4943690D0F30E3B1F49CFEB8D1FAC7DE1D91DEAF33DCE9DC14EE6D9087DE97997E17ADF0CC075449E6AB003F52278D4FB04873DCCBF5A482124515E15EF4DE9EC844694F8E523148E5DD495454FB90FCF3E20F62083FB8ECF855F87C35C0227CCEB2F49E8A7D4425B66ADB49184669501EAAE866A2F77FE01F8E9C748C0795B9704B8B4676953721BC896467DE1FE139AACBA2DB119E3165AC078FA29D51A3D95074889A864EDD66A071BB31285C311A1FBB35B33AD78AD7105E45ED7052F930774B14AC18237A58CDCD92096369ECA2763C693AA02CB832B6915D862597B18CB3D160CA11244868AE247444D722EE81D5B4E2C8EDF48C91522096DAA7EA00546D33111502C7A252CB71F8C88F0A9322BC4C8662D108A81F2B1EA3639988551A51698FC48457531A191BE9F347090C0528AD21DE2FE155EC082FC3913C956B2338652299758E5BF116849779A3C267E157B3F0A66C24EF3F28BC2ABD3DE19994DE21489D3F14261E3C26496E8534A8ECFF95F07AA862F32C0662260349764695DB2E3CF3417A125F454FE844E142615F7A464F72197DC9651C0BAFBD0B4A9F04E1ED457AADF00E61F95978A5F735C948C2ABD2CBE20BE1D58E272D92E85C7915D8115EED7D957B611D0AAF915E4F78151BE17D15587CAB4AACA5D2FA66846754B95F5978597A5DA113E35F223CCBFE8684E728FFA684D74A9F54E155E9AD8610F354DB82FFC7C2335AD1557425977947C26BF9370B6F496DD494E60D08CFA8D2F390035DD9195576795D57788AF2FF84F0427AC2D25CA9086FF8F86B5DC965F4C4B6418CAB37AFBF92F0CC7FC22759788BEC6F41788685375094B7115D45165EDDF69010378033EFA1F0868F1DB7C55B899D446CA44F4B955307988527D155D2D785912436F2AB82BEE45AB85D5E813B9AA8222B93A8F0EAF0606988B035247D6612DB1EAAFC7C1714C105408B41A0489F88F8F6B0925EA9B89A587A09033F0059204BCE9555157DD92D08E929AD913092DC3206778AE212460FEE813563603C29A5D1C2D22B680B802ABAB9138A655784CF17614BFE4892DC8C219F19557C4BBBBC82E87862DE1FE125D91564D9932EBC35DC032B93A8F01969FFEB084F6823BE8DF09F2BE8089F54E9B5515E6DAD915B6DF485D711DB1E49129EA496307A74B6C2C0E848AF0A2F8FB0B4115EED69158227482D6389EC9A6DB55D3EA17D3E46F0CE8417CD923C6E472129C26BA5D7133A31FE35C26BE4D7935C2629C28B47DCFFC3C20BE9932BBC486908CB70837FA3F092EC49165E4973FE13DE96FF29E17D28A5F121E9659229BCA1403733AAF08EA4D7133A31922FBC596E2BD9A57DFFEF84570557D114003DC9ADD0565C35E28B94467442BD86F06EFFB0F0AAF46F44F8685BE1B5BC8EF05AB4826BB1082E2317004615F9755A7234C2CB24557843A626562426BCBC2DE07DB2EC7A2891DEAEFCDC3BAB915D8DFA42FA9C2C37ED13A8F23B165E6D8EB4DA9F4CE1655E49784B2B8D8C8EF092F432AAF05A0CBEFF66E15F071DD155DE2BE155EC49EF4078D14C492457781B5E21A591F99F125E9BD2E8492E9324E15FAB8D5E4147749524099F89C848D24BBC55E199E40ACFEB3CE3C16B086F89F6FF099F34922B3C93A8F0AAF424AE3103E5E35A9993828EE82AEF95F089E5F43AC20BD1497A0B3993297C4EED08CA764910BEA38DE82A42767BC2CBB31DC8C26BC51790F0DE2438F3BAC21B3FAC0AC347D56C91A4D7133A315E4578C6F4691D0B36C7D5FD4AABCDAB5564AD7B63597247C8D37830C64C247922298D1613C92BC385C048222715597C358A5B89AD6023BF83796D181389AD625444B7C0D3F2714F6BA2C2D37E7E55111D50AAF424B73D3C4204464F125C83296FA81546AF30225C605061F9AD64E7F67ADAAFED81157041882662DE3FE119BBC22BC738D2BF2BE16DE1289F3CE16D10D2EBCBAD8756787BD813DE9EF46F4678CDF083B72CBC457AADF09AA86F1BE5BB11EF50F8449B25EDECD7434D6FFEA78467D9932BBC722FAC3DE9DF17E1D57595B7237C0C1183FF03A2E872A2F781A8800000000049454E44AE426082)
INSERT [dbo].[taikhoan] ([id], [username], [name], [password], [address], [email], [role], [avatar]) VALUES (4, N'sss', N'Nguyen Van A', NULL, N'sss', N'thanhle41@vanlanguni.vn', 2, NULL)
SET IDENTITY_INSERT [dbo].[taikhoan] OFF
SET IDENTITY_INSERT [dbo].[thutuc] ON 

INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (2, N'ggggggff', 3, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (3, N'gggggg', 3, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (4, NULL, 3, NULL, 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (13, NULL, 3, NULL, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (15, NULL, 3, NULL, 7, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (16, NULL, 3, NULL, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (17, NULL, 3, NULL, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (20, NULL, 3, NULL, 7, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (23, N'Họ Và Tên', 3, 3, 8, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (24, NULL, 3, NULL, 8, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (25, NULL, 3, NULL, 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (27, N'dd', 3, NULL, 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (28, NULL, 3, 3, 9, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (29, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (31, NULL, 3, 3, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (39, NULL, 3, 2, 5, N'dsdsdsds', CAST(0x16410B00 AS Date), 1, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (40, NULL, 3, NULL, 5, NULL, NULL, NULL, NULL)
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (41, NULL, 2, NULL, 11, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (42, NULL, 2, NULL, 11, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (43, N'Xin Thôi Học', 2, NULL, 5, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (44, N'Xin Thôi Học', 2, NULL, 5, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (45, NULL, 2, NULL, 31, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (46, NULL, 2, NULL, 31, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (47, NULL, 2, NULL, 18, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (48, NULL, 4, NULL, 31, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (49, NULL, 4, NULL, 32, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (50, NULL, 4, NULL, 22, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
INSERT [dbo].[thutuc] ([id], [name], [nguoitao], [nguoiduyet], [loaithutuc], [ghichu], [ngaylay], [tinhtrang], [ngaytao]) VALUES (51, NULL, 4, NULL, 31, NULL, NULL, NULL, CAST(0x2B410B00 AS Date))
SET IDENTITY_INSERT [dbo].[thutuc] OFF
USE [master]
GO
ALTER DATABASE [DichVuMotCua] SET  READ_WRITE 
GO
USE [DichVuMotCua]
GO
create TRIGGER update_quyenhs
ON quyenhs
AFTER  INSERT,UPDATE,DELETE
AS
BEGIN
update quyenhs set name_user = (select email from taikhoan where id = (select id_user from inserted)),
thutuc_hienthi = (CASE 
WHEN id_thutuc = 1 THEN N'Cấp Giấy Chứng Nhận' 
WHEN id_thutuc = 2 THEN N'Cấp In Thẻ Sinh Viên' 
WHEN id_thutuc = 3 THEN N'Cập Nhật Thông Tin' 
WHEN id_thutuc = 4 THEN N'Tiếp Tục Học' 
WHEN id_thutuc = 5 THEN N'Xin Thôi Học' 
WHEN id_thutuc = 6 THEN N'Bảo Lưu Kết Quả Học Tập' 
WHEN id_thutuc = 7 THEN N'Bảo Lưu Kết Quả Tuyển Sinh' 
WHEN id_thutuc = 8 THEN N'Chuyển Điểm' 
WHEN id_thutuc = 9 THEN N'Đăng Ký Học Chậm' 
WHEN id_thutuc = 10 THEN N'Đăng Ký Học Vượt' 
WHEN id_thutuc = 12 THEN N'Cấp Bản Sao Văn Bằng' 
WHEN id_thutuc = 13 THEN N'Đăng Ký Chương Trình Học Nhanh' 
WHEN id_thutuc = 14 THEN N'Đăng Ký Học Lại' 
WHEN id_thutuc = 15 THEN N'Đăng Ký Học Phần Thay Thế' 
WHEN id_thutuc = 16 THEN N'Đăng Ký Học Theo Tiến Độ Chậm' 
WHEN id_thutuc = 17 THEN N'Đề Nghị Cấp Bảng Điểm' 
WHEN id_thutuc = 18 THEN N'Đề Nghị Chứng Thực Văn Bằng' 
WHEN id_thutuc = 19 THEN N'Điều Chỉnh Kết Quả Học Tập' 
WHEN id_thutuc = 20 THEN N'Đối Chiếu Chương Trình Đào Tạo' 
WHEN id_thutuc = 21 THEN N'Đơn Đăng Ký Cải Thiện Điểm' 
WHEN id_thutuc = 22 THEN N'Đơn Phúc Khảo' 
WHEN id_thutuc = 23 THEN N'Đơn Rút Bớt Học Phần' 
WHEN id_thutuc = 24 THEN N'Đơn Xét Tốt Nghiệp Sớm' 
WHEN id_thutuc = 25 THEN N'Đơn Xin Chuyển Lớp' 
WHEN id_thutuc = 26 THEN N'Đơn Xin Chuyển Ngành' 
WHEN id_thutuc = 27 THEN N'Đơn Xin Nhập Học' 
WHEN id_thutuc = 28 THEN N'Đơn Xin Tạm Ngừng Học' 
WHEN id_thutuc = 29 THEN N'Đơn Xin Thôi Học Rút Hồ Sơ' 
WHEN id_thutuc = 30 THEN N'Giấy Giới Thiệu Thực Tập ' 
WHEN id_thutuc = 31 THEN N'Giấy Xác Nhận Đề Cương' 
WHEN id_thutuc = 32 THEN N'Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo' 
WHEN id_thutuc = 33 THEN N'Học Hai Chương Trình' 
WHEN id_thutuc = 34 THEN N'Kế Hoạch Học Tập' 
WHEN id_thutuc = 35 THEN N'Mô Tả Chương Trình Đào Tạo' 
WHEN id_thutuc = 36 THEN N'Thi Cải Thiện Điểm' 
WHEN id_thutuc = 37 THEN N'Tốt Nghiệp Tạm Thời' 
WHEN id_thutuc = 38 THEN N'Xác Nhận Kết Quả Thực Tập' 
WHEN id_thutuc = 39 THEN N'Xác Nhận Thời Gian Thực Tập' ELSE NULL END),

name_thutuc = (CASE 
WHEN id_thutuc = 1  then N'CAPGIAYCHUNGNHAN'
WHEN id_thutuc = 2  then N'CAPINTHESINHVIEN'
WHEN id_thutuc = 3  then N'CAPNHATTHONGTIN'
WHEN id_thutuc = 4  then N'TIEPTUCHOC'
WHEN id_thutuc = 5  then N'XINTHOIHOC'
WHEN id_thutuc = 6  then N'BAOLUUKETQUAHOCTAP'
WHEN id_thutuc = 7  then N'BAOLUUKETQUATUYENSINH'
WHEN id_thutuc = 8  then N'CHUYENDIEM'
WHEN id_thutuc = 9  then N'HOCCHAM'
WHEN id_thutuc = 10 then N'HOCVUOT'
WHEN id_thutuc = 12 then N'CAPBANSAOBANG'
WHEN id_thutuc = 13 then N'DANGKYCHUONGTRINHHOCNHANH'
WHEN id_thutuc = 14 then N'DANGKYHOCLAI'
WHEN id_thutuc = 15 then N'DANGKYHOCPHANTHAYTHE'
WHEN id_thutuc = 16 then N'DANGKYHOCTHEOTIENDOCHAM'
WHEN id_thutuc = 17 then N'DENGHICAPBANGDIEM'
WHEN id_thutuc = 18 then N'DENGHICHUNGTHUCVANBANG'
WHEN id_thutuc = 19 then N'DIEUCHINHKEHOACHHOCTAP'
WHEN id_thutuc = 20 then N'DOICHIEUCHUONGTRINHDAOTAO'
WHEN id_thutuc = 21 then N'DONDANGKYCAITHIENDIEM'
WHEN id_thutuc = 22 then N'DONPHUCKHAO'
WHEN id_thutuc = 23 then N'DONRUTBOTHOCPHAN'
WHEN id_thutuc = 24 then N'DONXETTOTNGHIEPSOM'
WHEN id_thutuc = 25 then N'DONXINCHUYENLOP'
WHEN id_thutuc = 26 then N'DONXINCHUYENNGANH'
WHEN id_thutuc = 27 then N'DONXINNHAPHOC'
WHEN id_thutuc = 28 then N'DONXINTAMNGUNGHOC'
WHEN id_thutuc = 29 then N'DONXINTHOIHOCRUTHOSO'
WHEN id_thutuc = 30 then N'GIAYGIOITHIEUTHUCTAP'
WHEN id_thutuc = 31 then N'GIAYXACNHANDECUONG'
WHEN id_thutuc = 32 then N'GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO'
WHEN id_thutuc = 33 then N'HOCHAICHUONGTRINH'
WHEN id_thutuc = 34 then N'KEHOACHHOCTAP'
WHEN id_thutuc = 35 then N'MOTACHUONGTRINHDAOTAO'
WHEN id_thutuc = 36 then N'THICAITHIENDIEM'
WHEN id_thutuc = 37 then N'TOTNGHIEPTAMTHOI'
WHEN id_thutuc = 38 THEN N'XACNHANKETQUAHOCTAP' 
WHEN id_thutuc = 39 THEN N'XACNHANTHOIGIANTHUCTAP' ELSE NULL END)
where id = (select id from inserted);
GO
CREATE TRIGGER update_date_thutuc
ON thutuc
AFTER  INSERT
AS
BEGIN
update thutuc set ngaytao = GETDATE() where id = (select id from inserted)
END
GO
create TRIGGER [dbo].[convert_date] 
ON [dbo].[chitietthutuc]
AFTER  INSERT,UPDATE,DELETE
AS
begin
	update  chitietthutuc set value = CONVERT(varchar, CAST(value as date), 23) where ISDATE(value) = 1 and LEN(value) = 10
end

END

GO
Create TRIGGER [dbo].[delete_duplicate_quyen]
ON [dbo].[quyenhs]
AFTER  INSERT
AS
BEGIN
WITH quyenhs AS(
   SELECT id_user, id_thutuc,
       RN = ROW_NUMBER()OVER(PARTITION BY id_user,id_thutuc ORDER BY id_user,id_thutuc)
   FROM dbo.quyenhs
)
DELETE FROM quyenhs WHERE RN > 1

END
GO
create TRIGGER [dbo].[update_name]
ON [dbo].[quyenhs]
AFTER  INSERT,UPDATE,DELETE
AS
BEGIN
	update quyenhs set name_thutuc = CASE
    WHEN id_thutuc ='1' THEN 'CAPGIAYCHUNGNHAN'
    WHEN id_thutuc ='2' THEN 'CAPINTHESINHVIEN'
    WHEN id_thutuc ='3' THEN 'CAPNHATTHONGTIN'
    WHEN id_thutuc ='4' THEN 'TIEPTUCHOC'
    WHEN id_thutuc ='5' THEN 'XINTHOIHOC'
    WHEN id_thutuc ='6' THEN 'BAOLUUKETQUAHOCTAP'
    WHEN id_thutuc ='7' THEN 'BAOLUUKETQUATUYENSINH'
    WHEN id_thutuc ='8' THEN 'CHUYENDIEM'
    WHEN id_thutuc ='9' THEN 'HOCCHAM'
    WHEN id_thutuc ='10' THEN 'HOCVUOT'
    WHEN id_thutuc ='12' THEN 'CAPBANSAOBANG'
    WHEN id_thutuc ='13' THEN 'DANGKYCHUONGTRINHHOCNHANH'
    WHEN id_thutuc ='14' THEN 'DANGKYHOCLAI'
    WHEN id_thutuc ='15' THEN 'DANGKYHOCPHANTHAYTHE'
    WHEN id_thutuc ='16' THEN 'DANGKYHOCTHEOTIENDOCHAM'
    WHEN id_thutuc ='17' THEN 'DENGHICAPBANGDIEM'
    WHEN id_thutuc ='18' THEN 'DENGHICHUNGTHUCVANBANG'
    WHEN id_thutuc ='19' THEN 'DIEUCHINHKEHOACHHOCTAP'
    WHEN id_thutuc ='20' THEN 'DOICHIEUCHUONGTRINHDAOTAO'
    WHEN id_thutuc ='21' THEN 'DONDANGKYCAITHIENDIEM'
    WHEN id_thutuc ='22' THEN 'DONPHUCKHAO'
    WHEN id_thutuc ='23' THEN 'DONRUTBOTHOCPHAN'
    WHEN id_thutuc ='24' THEN 'DONXETTOTNGHIEPSOM'
    WHEN id_thutuc ='25' THEN 'DONXINCHUYENLOP'
    WHEN id_thutuc ='26' THEN 'DONXINCHUYENNGANH'
    WHEN id_thutuc ='27' THEN 'DONXINNHAPHOC'
    WHEN id_thutuc ='28' THEN 'DONXINTAMNGUNGHOC'
    WHEN id_thutuc ='29' THEN 'DONXINTHOIHOCRUTHOSO'
    WHEN id_thutuc ='30' THEN 'GIAYGIOITHIEUTHUCTAP'
    WHEN id_thutuc ='31' THEN 'GIAYXACNHANDECUONG'
    WHEN id_thutuc ='32' THEN 'GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO'
    WHEN id_thutuc ='33' THEN 'HOCHAICHUONGTRINH'
    WHEN id_thutuc ='34' THEN 'KEHOACHHOCTAP'
    WHEN id_thutuc ='35' THEN 'MOTACHUONGTRINHDAOTAO'
    WHEN id_thutuc ='36' THEN 'THICAITHIENDIEM'
    WHEN id_thutuc ='37' THEN 'TOTNGHIEPTAMTHOI'
    WHEN id_thutuc ='38' THEN 'XACNHANKETQUAHOCTAP'
    WHEN id_thutuc ='39' THEN 'XACNHANTHOIGIANTHUCTAP'
    ELSE ''
END,
name_user = (select email from taikhoan where id = id_user),
thutuc_hienthi = CASE
    WHEN id_thutuc ='1' THEN N'CAPGIAYCHUNGNHAN'
    WHEN id_thutuc ='2' THEN N'CAPINTHESINHVIEN'
    WHEN id_thutuc ='3' THEN N'CAPNHATTHONGTIN'
    WHEN id_thutuc ='4' THEN N'TIEPTUCHOC'
    WHEN id_thutuc ='5' THEN N'XINTHOIHOC'
    WHEN id_thutuc ='6' THEN N'BAOLUUKETQUAHOCTAP'
    WHEN id_thutuc ='7' THEN N'BAOLUUKETQUATUYENSINH'
    WHEN id_thutuc ='8' THEN N'CHUYENDIEM'
    WHEN id_thutuc ='9' THEN N'HOCCHAM'
    WHEN id_thutuc ='10' THEN N'HOCVUOT'
    WHEN id_thutuc ='12' THEN N'Cấp Bản Sao Văn Bằng'
    WHEN id_thutuc ='13' THEN N'Đăng Ký Chương Trình Học Nhanh'
    WHEN id_thutuc ='14' THEN N'DANGKYHOCLAI'
    WHEN id_thutuc ='15' THEN N'Đăng Ký Học Phần Thay Thế'
    WHEN id_thutuc ='16' THEN N'Đăng Ký Học Theo Tiến Độ Chậm'
    WHEN id_thutuc ='17' THEN N'Đề Nghị Cấp Bảng Điểm'
    WHEN id_thutuc ='18' THEN N'Đề Nghị Chứng Thực Văn Bằng'
    WHEN id_thutuc ='19' THEN N'DIEUCHINHKEHOACHHOCTAP'
    WHEN id_thutuc ='20' THEN N'Đối Chiếu Chương Trình Đào Tạo'
    WHEN id_thutuc ='21' THEN N'Đơn Đăng Ký Cải Thiện Điểm'
    WHEN id_thutuc ='22' THEN N'Đơn Phúc Khảo'
    WHEN id_thutuc ='23' THEN N'Đơn Rút Bớt Học Phần'
    WHEN id_thutuc ='24' THEN N'Đơn Xét Tốt Nghiệp Sớm'
    WHEN id_thutuc ='25' THEN N'DONXINCHUYENLOP'
    WHEN id_thutuc ='26' THEN N'Đơn Xin Chuyển Ngành'
    WHEN id_thutuc ='27' THEN N'Đơn Xin Nhập Học'
    WHEN id_thutuc ='28' THEN N'Đơn Xin Tạm Ngừng Học'
    WHEN id_thutuc ='29' THEN N'Đơn Xin Thôi Học Rút Hồ Sơ'
    WHEN id_thutuc ='30' THEN N'Giấy Giới Thiệu Thực Tập'
    WHEN id_thutuc ='31' THEN N'Giấy Xác Nhận Đề Cương'
    WHEN id_thutuc ='32' THEN N'Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo'
    WHEN id_thutuc ='33' THEN N'Học Hai Chương Trình'
    WHEN id_thutuc ='34' THEN N'Kế Hoạch Học Tập'
    WHEN id_thutuc ='35' THEN N'Mô Tả Chương Trình Đào Tạo'
    WHEN id_thutuc ='36' THEN N'Thi Cải Thiện Điểm'
    WHEN id_thutuc ='37' THEN N'Tốt Nghiệp Tạm Thời'
    WHEN id_thutuc ='38' THEN N'Xác Nhận Kết Quả Học Tập'
    WHEN id_thutuc ='39' THEN N'Xác Nhận Thời Gian Thực Tập'
    ELSE ''
END

END
GO
create TRIGGER [dbo].[update_date_thutuc]
ON [dbo].[thutuc]
AFTER  INSERT
AS
BEGIN
update thutuc set ngaytao = GETDATE() where id = (select id from inserted)
END
GO

alter proc list_thu_tuc @id int
as
begin
select  id,loaithutuc as id_thutuc, (CASE 
WHEN thutuc.loaithutuc ='1'  THEN N'CAPGIAYCHUNGNHAN'
WHEN thutuc.loaithutuc ='2'  THEN N'CAPINTHESINHVIEN'
WHEN thutuc.loaithutuc ='3'  THEN N'CAPNHATTHONGTIN'
WHEN thutuc.loaithutuc ='4'  THEN N'TIEPTUCHOC'
WHEN thutuc.loaithutuc ='5'  THEN N'XINTHOIHOC'
WHEN thutuc.loaithutuc ='6'  THEN N'BAOLUUKETQUAHOCTAP'
WHEN thutuc.loaithutuc ='7'  THEN N'BAOLUUKETQUATUYENSINH'
WHEN thutuc.loaithutuc ='8'  THEN N'CHUYENDIEM'
WHEN thutuc.loaithutuc ='9'  THEN N'HOCCHAM'
WHEN thutuc.loaithutuc ='10' THEN N'HOCVUOT'
WHEN thutuc.loaithutuc ='12' THEN N'CAPBANSAOBANG'
WHEN thutuc.loaithutuc ='13' THEN N'DANGKYCHUONGTRINHHOCNHANH'
WHEN thutuc.loaithutuc ='14' THEN N'DANGKYHOCLAI'
WHEN thutuc.loaithutuc ='15' THEN N'DANGKYHOCPHANTHAYTHE'
WHEN thutuc.loaithutuc ='16' THEN N'DANGKYHOCTHEOTIENDOCHAM'
WHEN thutuc.loaithutuc ='17' THEN N'DENGHICAPBANGDIEM'
WHEN thutuc.loaithutuc ='18' THEN N'DENGHICHUNGTHUCVANBANG'
WHEN thutuc.loaithutuc ='19' THEN N'DIEUCHINHKEHOACHHOCTAP'
WHEN thutuc.loaithutuc ='20' THEN N'DOICHIEUCHUONGTRINHDAOTAO'
WHEN thutuc.loaithutuc ='21' THEN N'DONDANGKYCAITHIENDIEM'
WHEN thutuc.loaithutuc ='22' THEN N'DONPHUCKHAO'
WHEN thutuc.loaithutuc ='23' THEN N'DONRUTBOTHOCPHAN'
WHEN thutuc.loaithutuc ='24' THEN N'DONXETTOTNGHIEPSOM'
WHEN thutuc.loaithutuc ='25' THEN N'DONXINCHUYENLOP'
WHEN thutuc.loaithutuc ='26' THEN N'DONXINCHUYENNGANH'
WHEN thutuc.loaithutuc ='27' THEN N'DONXINNHAPHOC'
WHEN thutuc.loaithutuc ='28' THEN N'DONXINTAMNGUNGHOC'
WHEN thutuc.loaithutuc ='29' THEN N'DONXINTHOIHOCRUTHOSO'
WHEN thutuc.loaithutuc ='30' THEN N'GIAYGIOITHIEUTHUCTAP'
WHEN thutuc.loaithutuc ='31' THEN N'GIAYXACNHANDECUONG'
WHEN thutuc.loaithutuc ='32' THEN N'GIAYXACNHANHOANTHANHCHUONGTRINHDAOTAO'
WHEN thutuc.loaithutuc ='33' THEN N'HOCHAICHUONGTRINH'
WHEN thutuc.loaithutuc ='34' THEN N'KEHOACHHOCTAP'
WHEN thutuc.loaithutuc ='35' THEN N'MOTACHUONGTRINHDAOTAO'
WHEN thutuc.loaithutuc ='36' THEN N'THICAITHIENDIEM'
WHEN thutuc.loaithutuc ='37' THEN N'TOTNGHIEPTAMTHOI'
WHEN thutuc.loaithutuc ='38' THEN N'XACNHANKETQUAHOCTAP'
WHEN thutuc.loaithutuc ='39' THEN N'XACNHANTHOIGIANTHUCTAP'
ELSE ''
END) as thutucname,
  (CASE
    WHEN thutuc.loaithutuc ='1' THEN N'CAPGIAYCHUNGNHAN'
    WHEN thutuc.loaithutuc ='2' THEN N'CAPINTHESINHVIEN'
    WHEN thutuc.loaithutuc ='3' THEN N'CAPNHATTHONGTIN'
    WHEN thutuc.loaithutuc ='4' THEN N'TIEPTUCHOC'
    WHEN thutuc.loaithutuc ='5' THEN N'Xin Thôi Học'
    WHEN thutuc.loaithutuc ='6' THEN N'BAOLUUKETQUAHOCTAP'
    WHEN thutuc.loaithutuc ='7' THEN N'BAOLUUKETQUATUYENSINH'
    WHEN thutuc.loaithutuc ='8' THEN N'CHUYENDIEM'
    WHEN thutuc.loaithutuc ='9' THEN N'HOCCHAM'
    WHEN thutuc.loaithutuc ='10' THEN N'HOCVUOT'
    WHEN thutuc.loaithutuc ='12' THEN N'Cấp Bản Sao Văn Bằng'
    WHEN thutuc.loaithutuc ='13' THEN N'Đăng Ký Chương Trình Học Nhanh'
    WHEN thutuc.loaithutuc ='14' THEN N'Đăng Ký Học Lại'
    WHEN thutuc.loaithutuc ='15' THEN N'Đăng Ký Học Phần Thay Thế'
    WHEN thutuc.loaithutuc ='16' THEN N'Đăng Ký Học Theo Tiến Độ Chậm'
    WHEN thutuc.loaithutuc ='17' THEN N'Đề Nghị Cấp Bảng Điểm'
    WHEN thutuc.loaithutuc ='18' THEN N'Đề Nghị Chứng Thực Văn Bằng'
    WHEN thutuc.loaithutuc ='19' THEN N'Điều Chỉnh Kế Hoạch Học Tập'
    WHEN thutuc.loaithutuc ='20' THEN N'Đối Chiếu Chương Trình Đào Tạo'
    WHEN thutuc.loaithutuc ='21' THEN N'Đơn Đăng Ký Cải Thiện Điểm'
    WHEN thutuc.loaithutuc ='22' THEN N'Đơn Phúc Khảo'
    WHEN thutuc.loaithutuc ='23' THEN N'Đơn Rút Bớt Học Phần'
    WHEN thutuc.loaithutuc ='24' THEN N'Đơn Xét Tốt Nghiệp Sớm'
    WHEN thutuc.loaithutuc ='25' THEN N'Đơn Xin Chuyển Lớp'
    WHEN thutuc.loaithutuc ='26' THEN N'Đơn Xin Chuyển Ngành'
    WHEN thutuc.loaithutuc ='27' THEN N'Đơn Xin Nhập Học'
    WHEN thutuc.loaithutuc ='28' THEN N'Đơn Xin Tạm Ngừng Học'
    WHEN thutuc.loaithutuc ='29' THEN N'Đơn Xin Thôi Học Rút Hồ Sơ'
    WHEN thutuc.loaithutuc ='30' THEN N'Giấy Giới Thiệu Thực Tập'
    WHEN thutuc.loaithutuc ='31' THEN N'Giấy Xác Nhận Đề Cương'
    WHEN thutuc.loaithutuc ='32' THEN N'Giấy Xác Nhận Hoàn Thành Chương Trình Đào Tạo'
    WHEN thutuc.loaithutuc ='33' THEN N'Học Hai Chương Trình'
    WHEN thutuc.loaithutuc ='34' THEN N'Kế Hoạch Học Tập'
    WHEN thutuc.loaithutuc ='35' THEN N'Mô Tả Chương Trình Đào Tạo'
    WHEN thutuc.loaithutuc ='36' THEN N'Thi Cải Thiện Điểm'
    WHEN thutuc.loaithutuc ='37' THEN N'Tốt Nghiệp Tạm Thời'
    WHEN thutuc.loaithutuc ='38' THEN N'Xác Nhận Kết Quả Học Tập'
    WHEN thutuc.loaithutuc ='39' THEN N'Xác Nhận Thời Gian Thực Tập'
    ELSE ''
END) as loaithutuc, (select taikhoan.email from taikhoan where taikhoan.id = thutuc.nguoitao) as nguoitao,(select taikhoan.email from taikhoan where taikhoan.id = thutuc.nguoiduyet) as nguoiduyet,ngaytao, ngaylay, tinhtrang, ghichu from thutuc where tinhtrang is null
and thutuc.id in (select id_thutuc from quyenhs where id_user = @id)
order by id desc
end
